% Manifold learning test script
% Here we use TIME SERIES of all voxels in the WHOLE BRAIN
%
% To see whether dimension reduction approaches could extract time series
% from activated voxels
%
% Ground truth: activated voxels are considered to be localizer defined
%

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/Manifold_learning/');

if ismac
    drtoolbox_dir = '~/Documents/Toolboxes/drtoolbox';
    addpath(genpath(drtoolbox_dir));
elseif isunix
    drtoolbox_dir = '~/Documents/Toolboxes/drtoolbox_linux';
    addpath(genpath(drtoolbox_dir));
end

nsub_dir = toolrotationBaseDir;
fig_dir = fullfile(nsub_dir, 'Figures_dim_reduction');
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

% -------------------------------------------------------------------
fileID = fopen(fullfile(nsub_dir, 'dim_red_loc_roi_lin.txt'), 'w');
% -------------------------------------------------------------------

bold_dir_listing = dir('ToolRotationRun*')';
data_type = 'unsmoothed';
% Batch process each functional run
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    wholebrain_ts_name = [data_type '_' bold_run_name '_timeseries_nofixation.mat'];
    load(fullfile(bold_dir, wholebrain_ts_name));
    
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    roi_ts_name = [data_type '_' bold_run_name '_locroi_ts.mat'];
    load(fullfile(roi_ts_dir, roi_ts_name));
    
    [wholebrain_labels, wholebrain_colors] = gen_ts_labels(ijk_coords, roi_ijk_coords);
    fprintf(fileID, '%s: %d time series in total \n', bold_run_name, size(ijk_coords, 1));
    fprintf(fileID, '%s: %d time series for loc roi \n', bold_run_name, size(roi_ijk_coords, 1));
    
    % N-by-volumes dimensional
    wholebrain_data = zero_meaned_ts';
    
    % Feed in data matrix
    % PCA
    [mapped_data_PCA, ~] = compute_mapping(wholebrain_data, 'PCA');
    figure;
    scatter(mapped_data_PCA(:, 1), mapped_data_PCA(:, 2), 50, wholebrain_colors);
    xlabel('1st principal component');
    ylabel('2nd principal component');
    fig_name = [bold_run_name ' PCA: first two components'];
    title(fig_name);
    
    fig_filename = ['ts_locroi_' bold_run_name '_PCA.png'];
    saveas(gcf, fullfile(fig_dir, fig_filename));
    
    %     % MDS
    %     [mapped_data_MDS, ~] = compute_mapping(wholebrain_data, 'MDS');
    %     figure;
    %     scatter(mapped_data_MDS(:, 1), mapped_data_MDS(:, 2), 50, wholebrain_colors);
    %     title('MDS');
end