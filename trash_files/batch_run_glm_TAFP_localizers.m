% First level glm analysis in SPM for caos_toolrotation localizer runs
%
% Make sure generate_multiple_condition_mat.m has been run before!
% 
% different glm modesl?
clear;
clc;
close all;

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/');

spm_jobman('initcfg');
clc;

rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);
cd(datadir);

nsub_dir = uigetdir;
cd(nsub_dir);
fprintf('Analyzing %s \n', nsub_dir);

localizer_dir_listing = dir('LocalizerRun*')';

% Parameters
TR = 2.2; % in secs
dis_acqs = 2; % Discard first two volumes
num_vols = 84; % Totol number of localizer scans
num_vols = num_vols - dis_acqs;

% load('run_glm_TAFP_localizers.mat');

ESTIMATE_GLM = 1;

for runIdx = 1:length(localizer_dir_listing)
    loc_run_name = localizer_dir_listing(runIdx).name;
    cd(fullfile(nsub_dir, loc_run_name));
    
    timing_file = dir('TAFP*.csv');
    if isempty(timing_file)
        disp('This is not the TAFP localizer run!');
        break;
    end
    
    loc_file = dir('wraToolRotation*bold*.nii');
    loc_file = loc_file.name;
    fprintf('Analyzing %s \n', loc_file);
    
    % Modify spm output directory
    spm_output_dir = fullfile(nsub_dir, loc_run_name, 'glm_results');
    if ~exist(spm_output_dir, 'dir')
        mkdir(spm_output_dir);
    end
    % Create jobs structrue
    jobs{1,1}.spm.stats{1}.fmri_spec.dir = cellstr(spm_output_dir);
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.units = 'secs';
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.RT = TR;
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.fmri_t = 16;
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.fmri_t0 = 1;
    
    % Select scan files and remove first two volumes!!
    loc_sel_files = spm_select('ExtFPList', pwd, '^wra\w*.ep2d_bold', 3:84);
    
    % Load timing file
    load(fullfile(pwd, 'timings', 'timings_onsets.mat'));
    fprintf('Loading timing file: %s \n', fullfile(loc_run_name, 'timings', 'timings_onsets'));
    
    [nameList, uni_idx, ori_idx] = unique(RegNames, 'stable');
    onsets = cell(1, size(nameList, 1));
    durations = cell(1, size(nameList, 1));
    for nameIdx = 1:size(nameList, 1)
        onsets{nameIdx} = double(Onsets(ori_idx == nameIdx, :))'; % row vector
        durations{nameIdx} = double(Durations(uni_idx(nameIdx))); % row vector
    end
    names = nameList';
    save('multiple_sessions.mat', 'names', 'onsets', 'durations');
    
    % Create jobs structrue
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).scans = cellstr(loc_sel_files);
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {});
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).multi = cellstr('multiple_sessions.mat');
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).multi_reg = {''};
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).hpf = 128;
    
    % movefile
    movefile('multiple_sessions.mat', spm_output_dir);
    
    % Fill in the rest jobs fields
    jobs{1,1}.spm.stats{1}.fmri_spec.fact = struct('name', {}, 'levels', {});
    jobs{1,1}.spm.stats{1}.fmri_spec.bases.hrf = struct('derivs', [0 0]);
    jobs{1,1}.spm.stats{1}.fmri_spec.volt = 1;
    jobs{1,1}.spm.stats{1}.fmri_spec.global = 'None';
    jobs{1,1}.spm.stats{1}.fmri_spec.mask = {''};
    jobs{1,1}.spm.stats{1}.fmri_spec.cvi = 'AR(1)';
    
    cd(spm_output_dir);
    
    spm_jobman('run', jobs);
    if ESTIMATE_GLM == 1
        load SPM
        spm_spm(SPM);
    end
    
end

% ---------------------------------
%     % Create timing structure
%     timing_struct = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {});
%     [nameList, uni_idx, ori_idx] = unique(RegNames, 'stable');
%     for nameIdx = 1:size(nameList)
%         timing_struct(nameIdx).name = RegNames{uni_idx(nameIdx)};
%         timing_struct(nameIdx).onset = Onsets(ori_idx == nameIdx, :);
%         timing_struct(nameIdx).duration = Durations(uni_idx(nameIdx));
%         timing_struct(nameIdx).tmod = 0;
%     end
% ---------------------------------