function ind_ts = compare_timeseries(bold_data_dir, type, ind_mni_coords)
% compare_timeseries(bold_data_dir, type, ind_mni_coords):
% compare the detrened and high pass filtered time sereis with original
% time series
% 
% Input:
%   bold_data_dir: original bold data directory (string)
% 
%   type: 'smoothed' or 'unsmoothed'
% 
%   ind_mni_coords: mni coordniates that you want to plot
% 
% Output:
%   ind_ts: zero-meaned and high-pass filtered individual time series
% 
% Update Dec 23, 2015:
% - Change input parameters so that this function can be used to check the
%   based time series

cd(bold_data_dir);

if strcmp(type, 'smoothed')
    ts_file = dir('smoothed*_timeseries.mat');
elseif strcmp(type, 'unsmoothed')
    ts_file = dir('unsmoothed*_timeseries.mat');
end

if isempty(ts_file)
    disp('Cannot find processed time series file!');
    quit();
else
    load(ts_file.name);
end

% Read original time series
idx = ismember(mni_coords, ind_mni_coords, 'rows');
% ind_xyz_coords = xyz_coords(idx, :);

% Plot time series
figure(1);
ind_ts = zero_meaned_ts(:, idx);
plot(ind_ts);
title('Zero meaned and high pass filtered time series');
% grid on;
axh = gca;
set(axh, 'YGrid', 'on');

end