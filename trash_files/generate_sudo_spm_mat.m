function generate_sudo_spm_mat(bold_data_dir, TR, type)
% generate sudo spm.mat file for 

cd(bold_data_dir);
if strcmp(type, 'smoothed')
    bold_file = dir(fullfile(bold_data_dir, 'swraToolRotation*bold*.nii'));
    bold_file = bold_file.name;
    spm_output_dir = fullfile(bold_data_dir, 'glm_func', 'smoothed');
elseif strcmp(type, 'unsmoothed')
    bold_file = dir(fullfile(bold_data_dir, 'wraToolRotation*bold*.nii'));
    bold_file = bold_file.name;
    spm_output_dir = fullfile(bold_data_dir, 'glm_func', 'unsmoothed');

end
fprintf('Analyzing %s \n', bold_file);

if exist(spm_output_dir, 'dir')
    display('SPM.mat file already exists!');
    quit();
else
    mkdir(spm_output_dir);
end

% Read bold dataset and get parameters
hdr = spm_vol(fullfile(bold_data_dir, bold_file));
vol = spm_read_vols(hdr);
num_all_vols = size(vol, 4);
dis_acqs = 2;

if strcmp(type, 'smoothed')
    [bold_sel_files, ~]=spm_select('ExtFPList', bold_data_dir, '^swra\w*.ep2d_bold', (1 + dis_acqs):num_all_vols);
elseif strcmp(type, 'unsmoothed')
    [bold_sel_files, ~]=spm_select('ExtFPList', bold_data_dir, '^wra\w*.ep2d_bold', (1 + dis_acqs):num_all_vols);
end

% SPM.xY.VY
SPM.xY.VY = cellstr(bold_sel_files);

% SPM.xGX.gSF

% SPM.xX.K
% Generate DCT hpf
nvol = num_all_vols - dis_acqs;
K.RT = TR;
K.row = 1:nvol;
K.HParam = 180;
K = spm_filter(K);
DCT_Matrix = K.X0;

SPM.xX.K = DCT_Matrix;

save(fullfile(spm_output_dir, 'SPM.mat'), 'SPM');
end