% Test script to generate a rotating Utah teapot
clear;
close all;

%% Freeze aspect radio when rotating object
% Read patch info
[verts, faces, cindex] = teapotGeometry;

figure;
p = patch('Faces',faces,'Vertices',verts,'FaceVertexCData',cindex,'FaceColor','interp');

% Extract frames
% F(360) = struct('cdata',[],'colormap',[]);
% for rot_ang = 1:360
%     view(rot_ang,0);
%     axis off;
%     
%     p.FaceAlpha = 1;           % remove the transparency
%     p.FaceColor = 'interp';    % set the face colors to be interpolated
%     p.LineStyle = 'none';      % remove the lines
%     colormap autumn;           % change the colormap
%     
%     l = light('Position',[-0.4 0.2 0.9],'Style','infinite');
%     lighting gouraud;
%     material shiny;
%     l.Position = [-0.1 0.6 0.8];
%     
%     frame_ind = getframe(gca);
%     F(rot_ang) = frame_ind;
%     axis vis3d; % For axis vis3d: freeze the aspect ratios at their current values
% end

% fig = figure;
% movie(fig,F,1);

%% 
clear;
close all;

[verts, faces, cindex] = teapotGeometry;

figure;
p = patch('Faces',faces,'Vertices',verts,'FaceVertexCData',cindex,'FaceColor','interp');

F(360) = struct('cdata',[],'colormap',[]);
for rot_ang = 1:360
    view(rot_ang,0);
%     axis off;
    daspect([1 1 1]);
    axis equal;
	ax = gca;
    axis([-5 5 -5 5 -5 5]);

    p.FaceAlpha = 1;           % remove the transparency
    p.FaceColor = 'interp';    % set the face colors to be interpolated
    p.LineStyle = 'none';      % remove the lines
    colormap autumn;           % change the colormap
    
    l = light('Position',[-0.4 0.2 0.9],'Style','infinite');
    lighting gouraud;
    material shiny;
    l.Position = [-0.1 0.6 0.8];
    
    frame_ind = getframe(gca);
    F(rot_ang) = frame_ind;
end