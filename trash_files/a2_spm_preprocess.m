% Script to batch preprocess all data sets
clear;
clc;
close all;


addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/utilities/');
rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);

% addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/spm8/');
% rootdir = '~/Desktop/caos_toolrotation/';
% datadir = fullfile(rootdir, 'Data_ToolRotation');
% addpath(datadir);

datainfofile = 'caos_toolrotation_info.xlsx';
T = readtable(fullfile(rootdir, datainfofile));

ToolRotationID = T.ToolRotationID;
SubjectID = T.SubjectID;
Date = T.Date;
sub_num = length(ToolRotationID);

% Set up timing parameters
SliceOrder = [1:2:33 2:2:33];
refslice = 1;
TR = 2.2;
tot_slice_num = 33;
TA = TR - (TR/tot_slice_num);
time1 = TA/(tot_slice_num - 1);
time2 = TR - TA;
timing = [time1 time2];

for nsub = 1:sub_num
    ind_rotationID = ToolRotationID{nsub, 1};
    fprintf('Analyzing %s \n', ind_rotationID);
    nsub_dir = fullfile(datadir, ind_rotationID);
    cd(nsub_dir);
    
    % Slice timing correction
    temp_listing = dir('a*bold*.nii');
    if isempty(temp_listing)
        bold_files = spm_select('list', pwd, '^T\w*.ep2d_bold');
        spm_slice_timing(bold_files, SliceOrder, refslice, timing);
        fprintf('Slice timing correction for %s is done \n', ind_rotationID);
    else
        disp('Slice timing correction is done!');
    end
    
    % Realignment
    temp_listing = dir('ra*bold*.nii');
    if isempty(temp_listing)
        abold_files = spm_select('list', pwd, '^aT\w*.ep2d_bold');
        % Realignment for each bold  file
        for ctr = 1:size(abold_files,1)
            ind_abold_files = spm_select('Extlist', pwd, abold_files(ctr,:), 1:178);
            spm_realign(ind_abold_files);
            spm_reslice(ind_abold_files);
            fprintf('Realignment for %s is done \n', abold_files(ctr,:));            
        end
    else
        disp('Realignment is done!');
    end
    
    % Reset anterior commissure manually in SPM
    
    % Coregistration & normalization: wra*.nii
    
    fprintf('\n');
    fprintf('\n');

end