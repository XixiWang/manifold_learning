% Manifold learning test script
% Here we use BRAIN PATTERNS of LOC ROI
% 
% This script uses the original LLE toolbox
% 
clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/Manifold_learning/');
addpath('~/Documents/Toolboxes/LLE/');

nsub_dir = toolrotationBaseDir;
fig_dir = fullfile(nsub_dir, 'Figures_dim_reduction');
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

ColorMode = 0;
bold_dir_listing = dir('ToolRotationRun*')';
% Batch process each functional run
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    fig_filename = ['brainpattern_' bold_run_name '_twopks_lle_new.png'];
    if exist(fullfile(fig_dir, fig_filename), 'file')
        disp('LLE results already generated!')
        continue;
    end
    
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    % -------------------------------------------------------------
    roi_ts_name = dir(fullfile(roi_ts_dir, '*twopeaks*'));
    roi_ts_name = roi_ts_name.name;
    % -------------------------------------------------------------
    load(fullfile(roi_ts_dir, roi_ts_name));
    
    % Before rotation fixation has already been removed
    pattern_data = roi_zero_meaned_ts;
    % Remove after rotation fixation period
    [rot_ang_labels, label_color] = gen_rot_ang_labels(16, 5, 2.2, 6, 60, ColorMode);
    pattern_data = pattern_data(1:size(rot_ang_labels, 2), :);

    % Feed in data matrix    
    % LLE
    nnei = 12;
    kdim = 2;
    Y = lle(pattern_data', nnei, kdim);
    
    figure;
    scatter(Y(1,:), Y(2, :), 50, label_color);
    fig_name = [bold_run_name ' LLE'];
    title(fig_name);
    
    figure;
    % Generate new color label
    scatter(Y(1,:), Y(2, :), 100, rot_ang_labels(3, :), 'filled');
    labels_text = num2str(rot_ang_labels(3, :)', '%4.1f');
    text(Y(1,:), Y(2, :), labels_text, 'horizontal','left', 'vertical','bottom');
    colormap(hot);
    % saveas(gcf, fullfile(fig_dir, fig_filename));
end