% Generate FFT of time series
time_series = temp_ts;
TR = 2.2; % secs
Fs = 1/TR; % Hz

L = length(time_series);
% Generate time vector
time_vec = (0:L-1) * TR;

switch model
    case 'Single_sided'
        NFFT = 2^nextpow2(L); % Next power of 2 from length of y
        Y = fft(time_series, NFFT)/L;
        f = Fs/2 * linspace(0, 1, NFFT/2 + 1);
        
        % Plot single-sided amplitude spectrum.
        figure;
        plot(f, 2 * abs(Y(1: NFFT/2 + 1)))
        title('Single-Sided Amplitude Spectrum of y(t)')
        xlabel('f (Hz)')
        ylabel('|Y(f)|')
        
    case 'FFT'
        Y = fft(time_series);
        % Compute two-sided power spectrum
        P2 = abs(Y/L);
        P1 = P2(1:L/2 + 1);
        P1(2:end - 1) = 2 * P1(2:end - 1);
        
        % Define the frequency domain
        f = Fs * (0:(L/2))/L;
        figure;
        plot(f, P1)
        title('Single-Sided Amplitude Spectrum of X(t)')
        xlabel('f (Hz)')
        ylabel('|P1(f)|')
end
