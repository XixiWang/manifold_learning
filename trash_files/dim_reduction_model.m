function dim_reduction_model(model_name)
% Wrapper function to call a specific dimension reduction model name 
% 
% Input: 
%   model_name:
%   'patterns_pca'
%   'patterns_isomap'
%   'patterns_lle'
%   

addpath('~/Documents/Code/manifold_learning/analyses/dim_reduction/');
switch model_name
    case 'patterns_pca'
        patterns_pca;
    case 'patterns_isomap'
        patterns_isomap;
    case 'patterns_lle'
        patterns_lle;
end
end




