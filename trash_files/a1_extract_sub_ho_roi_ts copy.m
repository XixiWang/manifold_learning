% Script to extract time series for ho-rois
% (Can also combine two rois and generate a sub roi)
% 
% Update 01/18/2016: add ROI_info.csv file for batch ROI time series
% extraction

% Output files are: .../ROI_harv_oxf/combined_ho_roi.nii, 
% .../roi_mtrx/sub_ho_roi_ts.mat

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses');
nsub_dir = toolrotationBaseDir;

% ----------------------------------------------
Lingidx = 3;
Occidx = 3;

mask_dir = fullfile(nsub_dir, 'ROI_harv_oxf'); 
bold_type = 'unsmoothed';      
% ----------------------------------------------

% Load LingualGyrus_fft
load(fullfile(nsub_dir, ['ToolRotationRun0' num2str(Lingidx)], 'roi_fft', ...
    [bold_type '_ToolRotationRun0' num2str(Lingidx) '_FFT_LingualGyrus' ]), 'sel_T');
Lingcoords = sel_T.roi_ijk_coords;
clear sel_T;

load(fullfile(nsub_dir, ['ToolRotationRun0' num2str(Occidx)], 'roi_fft', ...
    [bold_type '_ToolRotationRun0' num2str(Occidx) '_FFT_OccipitalPole' ]), 'sel_T');
Occcoords = sel_T.roi_ijk_coords;
clear sel_T

% Load OccipitalPole_fft
combined_roi_coords = [Lingcoords; Occcoords];

% Output nii file
mask_name = ['Occ' num2str(Occidx) '_Lin' num2str(Lingidx) '.nii'];      
write_harv_oxf_nii(combined_roi_coords, mask_name, mask_dir);

% Batch process each functional run
bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % Output dir
    % ------------------------------------------------
    output_dir = fullfile(bold_dir, 'roi_mtrx');
    output_name = [bold_type '_' bold_run_name '_' mask_name(1:end-4)];
    % ------------------------------------------------

    % Extract and save masked time series
    extract_masked_timeseries(bold_dir, bold_type, mask_dir, mask_name, output_dir, output_name);
end

% Compare time series
% compare_timeseries(ts_file, ind_mni_coords);