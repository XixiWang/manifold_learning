function rm_spm_dir()
% rm_spm_dir: Check MATLAB version and remove corresponding spm path

% Code to run in MATLAB 2013b and earlier
if verLessThan('matlab', '8.3');
    spm_dir = '~/Documents/Toolboxes/spm8/';
    rmpath(spm_dir);
    fprintf('MATLAB version %s: remove spm8 \n', version);
else
    spm_dir = '~/Documents/Toolboxes/spm12';
    rmpath(spm_dir);
    fprintf('MATLAB version %s: remove spm12 \n', version);
end

end