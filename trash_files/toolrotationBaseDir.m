function [nsub_dir, subID] = toolrotationBaseDir()
% Return directory for a specific subject 
% 
% TODO: can't find directory?

[~, datadir] = add_mani_learn_dir;

if ismac
    add_spm_dir;
    % addpath('~/Documents/utilities/');
elseif isunix
    addpath('/usr/local/spm8/');
end

spm('Defaults', 'fmri');

nsub_dir = uigetdir(datadir);
cd(nsub_dir);
fprintf('Analyzing %s \n\n', nsub_dir);

[~, subID] = fileparts(nsub_dir);
end