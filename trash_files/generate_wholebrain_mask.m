% Script to generate a whole brain mask based on thresholding

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses/')
addpath('~/Documents/Code/manifold_learning/Analyses/Func_Fourier/');
addpath('~/Documents/utilities');

nsub_dir = toolrotationBaseDir;

% Batch process each localizer run
Func_dir_listing = dir('ToolRotationRun*')';

% Move mean file to top directory
if isempty(dir('*mean*bold*.nii'));
    mean_files = dir(fullfile(nsub_dir, 'ToolRotationRun01', '*meanaToolRotation*bold*.nii'));
    for ctr = 1:length(mean_files)
        movefile(fullfile(nsub_dir, 'ToolRotationRun01', ...
            mean_files(ctr).name), nsub_dir)
    end
else
    disp('Mean files already existed');
end

for runIdx = 1:length(Func_dir_listing)
    % for runIdx = 1
    Func_run_name = Func_dir_listing(runIdx).name;
    Func_dir = fullfile(nsub_dir, Func_run_name);
    cd(Func_dir);
    
    if exist('mask_wholebrain.nii', 'file') && exist('wratmean_func.nii', 'file')
        disp('Whole brain mask already exists!')
        continue;
    else
        % Use unsmoothed functional data!!!
        Func_file = dir('wraToolRotation*bold*.nii');
        Func_file = Func_file.name;
        fprintf('Analyzing %s \n', Func_file);
        
        % Read file & generate mask file
        Func_hdr = spm_vol(Func_file);
        Func_vol = spm_read_vols(Func_hdr);
        
        % Create tmean file
        % Vol
        tmean_func = mean(Func_vol, 4);
        % hdr
        mean_file = dir(fullfile(nsub_dir, 'wmeanaToolRotation*bold*.nii'));
        mean_file = mean_file.name;
        tmean_hdr = spm_vol(fullfile(nsub_dir, mean_file));
        tmean_hdr.fname = fullfile(Func_dir, 'wratmean_func.nii');
        
        spm_write_vol(tmean_hdr, tmean_func);
        
        % Set the threshold as 2
        thresh = 20;
        mask = tmean_func;
        mask(mask < thresh) = 0;
        mask(mask >= thresh) = 1;
        
        mask_hdr = tmean_hdr;
        mask_hdr.fname = fullfile(Func_dir, 'mask_wholebrain.nii');
        spm_write_vol(mask_hdr, mask);
    end
    
    
end