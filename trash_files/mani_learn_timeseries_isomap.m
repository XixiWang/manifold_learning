% Manifold learning test script
% Here we use TIME SERIES of voxels in LOC ROI
% 
% This script uses the original isomap toolbox from Tenenbaum
% 
% TODO: whole brain analysis? 

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/Manifold_learning/');
addpath('~/Documents/Toolboxes/IsomapR1/');

nsub_dir = toolrotationBaseDir;
fig_dir = fullfile(nsub_dir, 'Figures_dim_reduction');
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

% -------------------------------------------------------------------
% fileID = fopen(fullfile(nsub_dir, 'dim_red_loc_roi_nonlin.txt'), 'w');
% -------------------------------------------------------------------

bold_dir_listing = dir('ToolRotationRun*')';
data_type = 'unsmoothed';
% Batch process each functional run
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    wholebrain_ts_name = [data_type '_' bold_run_name '_timeseries_nofixation.mat'];
    load(fullfile(bold_dir, wholebrain_ts_name));
    
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    roi_ts_name = [data_type '_' bold_run_name '_locroi_ts.mat'];
    load(fullfile(roi_ts_dir, roi_ts_name));
    
    [wholebrain_labels, wholebrain_colors] = gen_ts_labels(ijk_coords, roi_ijk_coords);
    
    % N-by-volumes dimensional
    wholebrain_data = zero_meaned_ts;
    locroi_data = roi_zero_meaned_ts;
    % Feed in data matrix
    % Isomap
    % 1) Calculate distance matrix
    tic;
    D = L2_distance(locroi_data, locroi_data);
    
    % 2) Generate embedding
    options.dims = 1:10;
    [Y, R, E] = Isomap(D, 'k', 10, options);
    tEla = toc;
    fprintf('%s: %d time series for loc ROI \n', bold_run_name, size(roi_ijk_coords, 1));
    fprintf('Total calc time is %f secs \n', tEla);
    
    figure; 
    coords_2d = Y.coords{2, 1};
    scatter(coords_2d(1, :), coords_2d(2, :));
    xlabel('1st isomap component');
    ylabel('2nd isomap component');
    title('2D isomap');
    
    figure;
    coords_3d = Y.coords{3, 1};

    
    % fig_filename = ['ts_locroi_' bold_run_name '_isomap.png'];
    % saveas(gcf, fullfile(fig_dir, fig_filename));
end