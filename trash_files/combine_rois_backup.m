function combine_rois(roi1_dir, roi1_name, roi2_dir, roi2_name, output_roi_name, output_dir)
% function to combine two rois and write out nii file 

if nargin < 5
    output_roi_name = 'default_combined_roi.nii';
end

if nargin < 6
    output_dir = roi1_dir;
end

add_spm_dir;
roi_hdr1 = spm_vol(fullfile(roi1_dir, roi1_name));
roi_vol1 = spm_read_vols(roi_hdr1);

roi_hdr2 = spm_vol(fullfile(roi2_dir, roi2_name));
roi_vol2 = spm_read_vols(roi_hdr2);

if ~isequal(size(roi_vol1), size(roi_vol2))
    error('ROI dimensions do not match!');
end

output_roi_vol = roi_vol1 + roi_vol2;
output_roi_hdr = roi_hdr1;
output_roi_hdr.fname = fullfile(output_dir, output_roi_name);

spm_write_vol(output_roi_hdr, output_roi_vol);
end