% Manifold learning test script
% Here we use TIME SERIES of all voxels in the WHOLE BRAIN
% 
% To see whether dimension reduction approaches could extract time series
% from activated voxels
% 
% Ground truth: activated voxels are considered to be localizer defined
% 
% Todo: original Isomap toolbox

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/Manifold_learning/');
addpath('~/Documents/utilities/');
sendgmail_init;

if ismac
    drtoolbox_dir = '~/Documents/Toolboxes/drtoolbox';
    addpath(genpath(drtoolbox_dir));
elseif isunix
    drtoolbox_dir = '~/Documents/Toolboxes/drtoolbox_linux';
    addpath(genpath(drtoolbox_dir));
end

nsub_dir = toolrotationBaseDir;
fig_dir = fullfile(nsub_dir, 'Figures_dim_reduction');
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

% -------------------------------------------------------------------
% fileID = fopen(fullfile(nsub_dir, 'dim_red_loc_roi_nonlin.txt'), 'w');
% -------------------------------------------------------------------

bold_dir_listing = dir('ToolRotationRun*')';
data_type = 'unsmoothed';
% Batch process each functional run
% for runIdx = 1:length(bold_dir_listing)
for runIdx = 1
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    wholebrain_ts_name = [data_type '_' bold_run_name '_timeseries_nofixation.mat'];
    load(fullfile(bold_dir, wholebrain_ts_name));
    
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    roi_ts_name = [data_type '_' bold_run_name '_locroi_ts.mat'];
    load(fullfile(roi_ts_dir, roi_ts_name));
    
    [wholebrain_labels, wholebrain_colors] = gen_ts_labels(ijk_coords, roi_ijk_coords);
    
    % N-by-volumes dimensional
    wholebrain_data = zero_meaned_ts';
    fprintf('Total number of time series is %d \n', size(wholebrain_data, 1));
    
    % Calculate intrinsic dimensionality
    % Call another function to estimate intrinsic dimensionality due to
    % high-computational cost

    % Feed in data matrix
    % Isomap
    tic;
    [mapped_data_iso, ~] = compute_mapping(wholebrain_data, 'Isomap');
    tEla = toc;
    sendmail('xixi.wang577@gmail.com', ...
        'isomap test is done', ...
        ['tEla = ' num2str(tEla)]);
    
    figure; 
    scatter(mapped_data_iso(:, 1), mapped_data_iso(:, 2), 50, wholebrain_colors);
    xlabel('1st isomap component');
    ylabel('2nd isomap component');
    fig_name = [bold_run_name ' isomap: first two components'];
    title(fig_name);
    
    fig_filename = ['ts_locroi_' bold_run_name '_isomap.png'];
    % saveas(gcf, fullfile(fig_dir, fig_filename));
end