function [nsub_dir, subID] = toolrotationBaseDir()
% Return directory for a specific subject 
% 
% TODO: can't find directory?

if ismac
    add_spm_dir;
    spm('Defaults', 'fmri');
    addpath('~/Documents/utilities/');
    rootdir = '~/Documents/Data/caos_toolrotation/';
    datadir = fullfile(rootdir, 'Data_ToolRotation');
elseif isunix
    addpath('/usr/local/spm8/');
    rootdir = '/raizadaData/Tool_Rotation/';
    datadir = fullfile(rootdir, 'Data_ToolRotation');
    spm('Defaults', 'fmri');
end;

nsub_dir = uigetdir(datadir);
cd(nsub_dir);
fprintf('Analyzing %s \n\n', nsub_dir);

[~, subID] = fileparts(nsub_dir);
end