
F = spm_select(Inf,'image');
F = spm_vol(F);
g = spm_global(F);
gSF = 100 / mean(g); % see spm_fmri_spm_ui.m
Y = spm_summarise(F,'all') * gSF;
TH = g.*gSF*0.8;
Y  = Y(:,all(bsxfun(@gt,Y,TH),1)); % masking
% TR=3s and high-pass filter cutoff period=128
K = spm_filter(struct('RT',2.2,'row',1:size(Y,1),'HParam',180));
Y = spm_filter(K,Y);