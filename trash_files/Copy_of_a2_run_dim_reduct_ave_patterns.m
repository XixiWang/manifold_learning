% Dimension reduction wrapper script
% 
% Calculate the averaged brain patterns
% 
% TODO: test different free parameters and output corresponding figs

clear all;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses/');
addpath('~/Documents/Code/manifold_learning/analyses/dim_reduction/');
nsub_dir = toolrotationBaseDir;

% -------------------------------------------------------------
% ToolRotation09
bold_type = 'unsmoothed';
ts_roi_name = 'fork_twopeaks_loc3_ts'; %'Occ3_Lin3', 'locroi_ts'
mode = 'pattern';

nnei = 5; % nnei = 10; 
max_dim = 2; % max_dim = 10;
nTR_bins = 33;
% -------------------------------------------------------------
subset_TR_labels = num2str([1: nTR_bins]');
scrsz = get(groot, 'ScreenSize');

% Generate rotation angles
rot_ang_labels = gen_rot_ang_labels(16, 5, 2.2, 6, 60);
deg_per_TR = rot_ang_labels(3, :);
ang_bin_labels = gen_bin_labels(nTR_bins, deg_per_TR);

% Batch process each functional run
bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    ts_mtrx_dir = fullfile(bold_dir, 'roi_mtrx');
    ts_mtrx_name = [bold_type '_' bold_run_name '_' ts_roi_name '.mat'];

    % Load time series matrix
    [ts_mtrx, ts_ijk_coords, ts_mni_coords] = load_ts_mtrx(ts_mtrx_dir, ts_mtrx_name);
    % Average brain patterns for each angle bin
    [ave_pattern_mtrx, bin_TR_idx] = gen_ave_patterns(ts_mtrx, ang_bin_labels);
    
    h_fig = figure;
    % Customize figure size
    set(h_fig, 'Position', [1 scrsz(4)/2 scrsz(3)*0.5 scrsz(4)*0.5]);
    set(gcf, 'Color', [1 1 1]);
    
    % Dimension reduction
    % PCA
    pca_embed_mtrx = pca(ave_pattern_mtrx', 'NumComponents', max_dim);
    subplot(1, 2, 1); 
    gen_scatter_text(pca_embed_mtrx(:, 1), pca_embed_mtrx(:,2), subset_TR_labels);
    title(['PCA: ' bold_run_name], 'FontSize', 20);
    
    % lle
    lle_embed_mtrx = run_lle_roweis(ave_pattern_mtrx, nnei, max_dim, mode);
    % Generate figures
    subplot(1, 2, 2);
    gen_scatter_text(lle_embed_mtrx(:, 1), lle_embed_mtrx(:, 2), subset_TR_labels);
    title(['LLE: ' bold_run_name], 'FontSize', 20);

end

    % Generate color labels
    % ang_bin_labels = gen_bin_labels(num_bins, rot_ang_labels(3, :));