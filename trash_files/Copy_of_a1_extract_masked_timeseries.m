% Read in masks and extract corresponding time series
% 
% Update 1/8/2015: use overlap ROIs

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses');
nsub_dir = toolrotationBaseDir;

% ROI director
% -------------------------------
roi_dir = fullfile(nsub_dir, 'ROI_Overlap'); 
% -------------------------------
[roi_vol, roi_name, ~] = read_roi_vol(roi_dir);
roi_ijk_coords = get_roi_coords(roi_vol);

% Batch process each functional run
bold_dir_listing = dir('ToolRotationRun*')';
type = 'unsmoothed';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    if ~exist(roi_ts_dir, 'dir')
        mkdir(roi_ts_dir);
    end
    
    % -------------------------------
    % roi_ts_name = [type '_' bold_run_name '_locroi_ts.mat'];
    roi_ts_name = [type '_' bold_run_name '_' roi_name(6: end - 4) '_ts.mat'];
    % -------------------------------
    if exist(fullfile(roi_ts_dir, roi_ts_name), 'file')
        fprintf('%s already exist! \n', roi_ts_name);
        continue;
    end
    
    % Use time series without fixation 
    if strcmp(type, 'smoothed')
        bold_file = dir(fullfile(bold_dir, 'smoothed*_timeseries_nofixation.mat'));
        bold_file = bold_file.name;
    elseif strcmp(type, 'unsmoothed')
        bold_file = dir(fullfile(bold_dir, 'unsmoothed*_timeseries_nofixation.mat'));
        bold_file = bold_file.name;
    end
    load(fullfile(bold_dir, bold_file));
    fprintf('Analyzing %s \n', bold_file);

    [sela, selb] = ismember(roi_ijk_coords, ijk_coords, 'rows');
    no_zeros = length(find(sela == 0));
    fprintf('Remove %d voxels \n', no_zeros);
    
    selb(selb == 0) = [];
    roi_zero_meaned_ts = zero_meaned_ts(:, selb);
    roi_ijk_coords = ijk_coords(selb, :);
    roi_mni_coords = mni_coords(selb, :);
    
    save(fullfile(roi_ts_dir, roi_ts_name), 'roi_zero_meaned_ts', ...
        'roi_ijk_coords', 'roi_mni_coords');
    
end

% Compare time series
% compare_timeseries(ts_file, ind_mni_coords);