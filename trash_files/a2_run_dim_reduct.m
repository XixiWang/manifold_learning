% Dimension reduction wrapper script
clear all;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses/');
addpath('~/Documents/Code/manifold_learning/analyses/dim_reduction/');
nsub_dir = toolrotationBaseDir;

% -------------------------------------------------------------
% ToolRotation09
bold_type = 'unsmoothed';
ts_roi_name = 'fork_twopeaks_loc3_ts'; %'Occ3_Lin3', 'locroi_ts'
nnei = 12; % nnei = 10; 
max_dim = 2; % max_dim = 10;
mode = 'pattern';

subset_TR_num = 33;
subset_txt_offset = 0.05;
% -------------------------------------------------------------
subset_TR_labels = num2str([1: subset_TR_num]');
scrsz = get(groot, 'ScreenSize');

% Batch process each functional run
bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    ts_mtrx_dir = fullfile(bold_dir, 'roi_mtrx');
    ts_mtrx_name = [bold_type '_' bold_run_name '_' ts_roi_name '.mat'];

    % Load time series matrix
    [ts_mtrx, ts_ijk_coords, ts_mni_coords] = load_ts_mtrx(ts_mtrx_dir, ts_mtrx_name);
    
    h_fig = figure;
    % Customize figure size
    set(h_fig, 'Position', [1 scrsz(4)/2 scrsz(3)*0.75 scrsz(4)*0.75]);
    set(gcf, 'Color', [1 1 1]);
    
    % test lle
    lle_embed_mtrx = run_lle_roweis(ts_mtrx, nnei, max_dim, mode);
    % Generate figures
    subplot(2, 2, 1);
    scatter(lle_embed_mtrx(:,1), lle_embed_mtrx(:, 2), 100);
    title('LLE with fixation')

    % -------------------------------------------------------------
    % Plot subset
    subplot(2, 2, 2)
    scatter(lle_embed_mtrx(1:subset_TR_num, 1), lle_embed_mtrx(1:subset_TR_num, 2), 100);
    hold on;
    plot(lle_embed_mtrx(1:subset_TR_num, 1), lle_embed_mtrx(1:subset_TR_num, 2), '-x');
    text(lle_embed_mtrx(1:subset_TR_num, 1) + subset_txt_offset, ...
         lle_embed_mtrx(1:subset_TR_num, 2) + subset_txt_offset, ...
         subset_TR_labels, 'horizontal','left', 'vertical','bottom');
    % -------------------------------------------------------------
    
    % Remove fixation after rotation and test lle
    ts_mtrx_nofix = ts_mtrx(1:168, :);
    embed_mtrx_nofix = run_lle_roweis(ts_mtrx_nofix, nnei, max_dim, mode);
    subplot(2, 2, 3);
    scatter(embed_mtrx_nofix(:,1), embed_mtrx_nofix(:, 2), 100);
    title('LLE without fixation')

    % PCA
    pca_embed_mtrx = pca(ts_mtrx', 'NumComponents', max_dim);
    subplot(2, 2, 4); 
    color_lab = 1:174;
    scatter(pca_embed_mtrx(:, 1), pca_embed_mtrx(:,2), 50, color_lab, 'fill');
    colormap hot;
    title('PCA with fixation')
end

    % Generate color labels
    % ang_bin_labels = gen_bin_labels(num_bins, rot_ang_labels(3, :));