% Script to create contrast files
clear;
clc;
close all;

rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);
cd(datadir);

spm_jobman('initcfg');
clc;

nsub_dir = uigetdir;
cd(nsub_dir);
fprintf('Converting multiple condition file for %s \n', nsub_dir);

localizer_dir_listing = dir('LocalizerRun*')';



for runIdx = 2
    cd(fullfile(nsub_dir, localizer_dir_listing(runIdx).name));
    fprintf('Analyzing %s \n', localizer_dir_listing(runIdx).name);
    cd glm_results
    
    jobs{1}.spm.stats.con.spmmat = cellstr(fullfile(pwd, 'SPM.mat'));
    
    % Define contrasts
    % Animal Face Places Tools ScaAnimals ScaFace ScaPlaces ScaTools
    jobs{1}.spm.stats.con.consess{1}.tcon.name = 'Animal > Others';
    jobs{1}.spm.stats.con.consess{1}.tcon.convec = [1 -0.25 -0.25 -0.25 -0.25 0 0 0];
    jobs{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';

    % Animal Face Places Tools ScaAnimals ScaFace ScaPlaces ScaTools
    jobs{1}.spm.stats.con.consess{2}.tcon.name = 'Face > Others';
    jobs{1}.spm.stats.con.consess{2}.tcon.convec = [-0.25 1 -0.25 -0.25 0 -0.25 0 0];
    jobs{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    
    % Animal Face Places Tools ScaAnimals ScaFace ScaPlaces ScaTools
    jobs{1}.spm.stats.con.consess{3}.tcon.name = 'Places > Others';
    jobs{1}.spm.stats.con.consess{3}.tcon.convec = [-0.25 -0.25 1 -0.25 0 0 -0.25 0];
    jobs{1}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
    
    % Animal Face Places Tools ScaAnimals ScaFace ScaPlaces ScaTools
    jobs{1}.spm.stats.con.consess{4}.tcon.name = 'Tools > Others';
    jobs{1}.spm.stats.con.consess{4}.tcon.convec = [-0.25 -0.25 -0.25 1 0 0 0 -0.25];
    jobs{1}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
    
    jobs{1}.spm.stats.con.delete = 1;
    
    spm_jobman('run',jobs);
    
end

