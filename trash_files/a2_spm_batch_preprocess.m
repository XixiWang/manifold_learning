% Wrapper function to batch preprocess ToolRotation dataset
clear;
clc;

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/utilities/');
rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);

load('a2_spm_batch_preprocess_ind_sessions.mat');

spm_jobman('initcfg');
cd(datadir);
% Select data director
nsub_dir = uigetdir;
cd(nsub_dir);

bold_dir_listing = dir('ToolRotationRun*');

for ctr = 1:length(bold_dir_listing);
% for ctr = 1
    ind_matlabbatch = matlabbatch;
    cur_dir = bold_dir_listing(ctr).name;
    cur_dir = fullfile(nsub_dir, cur_dir);
    cd(cur_dir);
    fprintf('Preprocessing %s \n', cur_dir);
    
    % Call spm batch script function
    bold_files = spm_select('Extlist', pwd, '^T\w*.ep2d_bold', 1:178);
    
    % Slice timing correction
    files_cell = cellstr(bold_files);
    scans_cell = cell(178, 1);
    for ctri = 1:length(files_cell)
        scans_cell(ctri, 1) = fullfile(cur_dir, files_cell(ctri, 1));
    end
    ind_matlabbatch{1,1}.spm.temporal.st.scans{1,1} = scans_cell;
   
    % Coregistration
    anat_files = spm_select('Extlist', pwd, '^ToolRotation\w*.anat.nii');
    ind_matlabbatch{1,3}.spm.spatial.coreg.estwrite.source{1,1} = fullfile(cur_dir, anat_files);
    fprintf('Anatomical file: %s \n', fullfile(cur_dir, anat_files));
    
    % Normalization
    ind_matlabbatch{1,4}.spm.spatial.normalise.estwrite.subj.source{1,1} = fullfile(cur_dir, anat_files);

    spm_jobman('run', ind_matlabbatch);
    
    % Check alignment and coregistration
end
