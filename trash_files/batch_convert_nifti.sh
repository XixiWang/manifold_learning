#! /bin/bash

# Script to batch convert dicom files to nifti files

export raw_data_dir=~/Desktop
cd $raw_data_dir 

if [ -d "dicom_conversion" ];then
    echo "dicom_conversion folder already existed"
else
    echo "Start converting"
    mcverter --output dicom_conversion/ --split_dir --nii --format nifti */*.dcm
    echo "Conversion is done"
else

