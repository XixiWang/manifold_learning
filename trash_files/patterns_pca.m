function patterns_pca()
% Manifold learning test script
% The idea here is to compare BRAIN PATTERNS at different rotating angles
%
% Todo:
% 1) MATLAB - PCA (Done, generate same results as the toolbox)
% 1-1) Linear: PCA, MDS (Done)
% 2) Nonlinear: free paramters - number of neighbors, plot in three
% dimensions
% 3) Different brain patterns categorization
% Other non-linear dim reduction approaches
%
% Update 01/06/2015: 1) check OS version and select proper drtoolbox directory
% 2) PCA: first two components = MDS first two components

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/Manifold_learning/');

if ismac
    drtoolbox_dir = '~/Documents/Toolboxes/drtoolbox';
    addpath(genpath(drtoolbox_dir));
elseif isunix
    drtoolbox_dir = '~/Documents/Toolboxes/drtoolbox_linux';
    addpath(genpath(drtoolbox_dir));
end

% load('~/Documents/Data/caos_toolrotation/Data_ToolRotation/TimingInfo_WithFixation.mat');
nsub_dir = toolrotationBaseDir;
fig_dir = fullfile(nsub_dir, 'Figures_dim_reduction');
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

% -------------------------------------------------------------------
fileID = fopen(fullfile(nsub_dir, 'dim_red_brain_pattern_overlapROI.txt'), 'w');
% -------------------------------------------------------------------
% Figure mode
figMode = 1;
% Color mode
ColorMode = 0;

bold_dir_listing = dir('ToolRotationRun*')';
data_type = 'unsmoothed';
% Batch process each functional run
for runIdx = 1:length(bold_dir_listing)
    % for runIdx = 1
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % -------------------------------------------------------------
    fig_filename = ['brainpattern_' bold_run_name '_twopks_PCA.png'];
    % -------------------------------------------------------------
    if exist(fullfile(fig_dir, fig_filename), 'file')
        disp('PCA results already generated!')
        continue;
    end
    
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    % -------------------------------------------------------------
    roi_ts_name = dir(fullfile(roi_ts_dir, '*twopeaks*'));
    roi_ts_name = roi_ts_name.name;
    % -------------------------------------------------------------
    load(fullfile(roi_ts_dir, roi_ts_name));
    
    % Before rotation fixation has already been removed
    pattern_data = roi_zero_meaned_ts;
    % Remove after rotation fixation period
    if ColorMode == 0
        [rot_ang_labels, label_color] = gen_rot_ang_labels(16, 5, 2.2, 6, 60);
    elseif ColorMode ~= 0
        [rot_ang_labels, label_color] = gen_rot_ang_labels(16, 5, 2.2, 6, 60, 90);
    end
    pattern_data = pattern_data(1:size(rot_ang_labels, 2), :);
    
    fprintf(fileID, '%s: %d volumes, %d voxels \n', bold_run_name, size(pattern_data, 1), size(pattern_data, 2));
    
    % If analyzing brain patterns, matrix doesn't need to be transposed
    tic;
    dim_mle = intrinsic_dim(pattern_data, 'MLE'); % Default
    dim_corr = intrinsic_dim(pattern_data, 'CorrDim');
    dim_Eig = intrinsic_dim(pattern_data, 'EigValue');
    dim_NearNN = intrinsic_dim(pattern_data, 'NearNbDim');
    dim_gmst = intrinsic_dim(pattern_data, 'GMST');
    dim_packnum = intrinsic_dim(pattern_data, 'PackingNumbers');
    tEla = toc;
    
    fprintf(fileID, 'MLE dim: %f \n', dim_mle);
    fprintf(fileID, 'Corr dim: %f \n', dim_corr);
    fprintf(fileID, 'PCA eig dim: %f \n', dim_Eig);
    fprintf(fileID, 'NearNb dim: %f \n', dim_NearNN);
    fprintf(fileID, 'GMST dim: %f \n', dim_gmst);
    fprintf(fileID, 'PackingNums dim: %f \n', dim_packnum);
    fprintf(fileID, 'Total estimation time: %f \n', tEla);
    fprintf(fileID, '\n');
    
    if figMode == 1
        % Feed in data matrix
        % PCA
        [mapped_data_PCA, ~] = compute_mapping(pattern_data, 'PCA');
        figure;
        scatter(mapped_data_PCA(:, 1), mapped_data_PCA(:, 2), 50, label_color);
        xlabel('1st principal component');
        ylabel('2nd principal component');
        fig_name = [bold_run_name ' PCA: first two components'];
        title(fig_name);
        saveas(gcf, fullfile(fig_dir, fig_filename));
    elseif figMode == 0
        continue;
    end
    
end
fclose(fileID);

% Combine different labels
% -------------------------------------------------
% Combine label colors: compare first 270~90 & 90~270
%     % 3 to 2
%     [lia, ~] = ismember(label_color, [ 0 0 1], 'rows');
%     label_color(lia, :) = repmat([0 1 0], length(find(lia == 1)),1);
%     % 4 to 1
%     [lia2, ~] = ismember(label_color, [ 1 1 0], 'rows');
%     label_color(lia2, :) = repmat([1 0 0], length(find(lia2 == 1)),1);
% -------------------------------------------------
%
%
% drtoolbox application
%         % MATLAB embed function
%         [coeff, score] = pca(pattern_data);
%         figure;
%         plot(coeff(:, 1), coeff(:, 2), '+');
%
%         [mapped_data_PCA2, ~] = compute_mapping(pattern_data, 'PCA', dim_Eig);
%         subplot(1, 4, 2);
%         scatter(mapped_data_PCA2(:, 1), mapped_data_PCA2(:, 2), 50, label_color);
%         fig_name = ['PCA dim Eig: ' num2str(dim_Eig)];
%         title(fig_name);
%
%         [mapped_data_PCA3, ~] = compute_mapping(pattern_data, 'PCA', dim_mle);
%         subplot(1, 4, 3);
%         scatter(mapped_data_PCA3(:, 1), mapped_data_PCA3(:, 2), 50, label_color);
%         fig_name = ['PCA dim mle: ' num2str(dim_mle)];
%         title(fig_name);
%
%         % MDS
%         [mapped_data_MDS, ~] = compute_mapping(pattern_data, 'MDS', 2);
%         subplot(1, 4, 4);
%         scatter(mapped_data_MDS(:, 1), mapped_data_MDS(:, 2), 50, label_color);
%         fig_name = 'MDS default';
%         title(fig_name);
%
% Isomap
% [mapped_data_iso, mapping3] = compute_mapping(pattern_data, 'Isomap');
% figure; scatter(mapped_data_iso(:, 1), mapped_data_iso(:, 2));
% title('Isomap');
end