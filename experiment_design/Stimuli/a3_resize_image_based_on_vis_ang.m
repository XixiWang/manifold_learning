% Resize images based on visual angle (5 degs)
%% Calculate the size of object based on the visual angle
clc;
clear;
close all;

subID = '866';
Obj_dir = fullfile('~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/Manifold_learning_objects',subID);
% Obj_dir = fullfile('~/Documents/Data/Manifold_learning_objects',subID);
cd(Obj_dir);
mkdir('Resized_images_new');
cd(fullfile(Obj_dir,'Resized_images_new'))
mkdir('Cropped_images');

load(fullfile(Obj_dir,'Modified_images',[subID '_modified_img.mat']));
vis_ang_deg = 5; % degree
AD = 14;
AB = (101 + 109.3)/2; % cm

obj_size = tan(degtorad(vis_ang_deg)/2) * (AD+AB) * 2;
vis_ang_calc = radtodeg(2*atan(obj_size/(2*(AD+AB))));
fprintf('Visual angle is %1.1f degrees \nObject size is %6.4f cm\n',...
    vis_ang_calc,obj_size)

%% Convert object size to pixels
ppcm = 32; % monitor property: pixels per cm
obj_px = ppcm * obj_size;
obj_px_round = round(obj_px);
fprintf('Object size in px: %d pixels\n',obj_px_round);
fprintf('\n');

%% Modify images
se = strel('disk',5);
wid = zeros(1,72);
hei = zeros(1,72);
rmin = wid; rmax = wid;
cmin = wid; cmax = wid;

for img_num = 1:72
    temp_rgb = img_mod{img_num};
    temp = im2double(rgb2gray(temp_rgb));
    temp(temp~=0) = 1;
    temp = im2bw(temp);
    % Delete small holes
    temp = imopen(temp,se);
    
    [r_obj,c_obj] = find(temp == 1);
    rmin(1,img_num) = min(r_obj(:));
    rmax(1,img_num) = max(r_obj(:));
    cmin(1,img_num) = min(c_obj(:));
    cmax(1,img_num) = max(c_obj(:));
    wid(1,img_num) = cmax(1,img_num) - cmin(1,img_num);
    hei(1,img_num) = rmax(1,img_num) - rmin(1,img_num);
end

fprintf('Min width is %d, max width is %d \n',min(wid(:)),max(wid(:)));
fprintf('Min height is %d, max height is %d \n',min(hei(:)),max(hei(:)));
fprintf('Min rmin is %d \n',min(rmin(:)));
fprintf('Min cmin is %d \n',min(cmin(:)));
fprintf('\n');

px_ver_diff = max(hei(:)) - min(hei(:));
dist_ver_diff = 1/32 * px_ver_diff; % cm
vis_ang_ver_diff = radtodeg(2*atan(dist_ver_diff/(2*(AD+AB))));

px_hor_diff = max(wid(:)) - min(wid(:));
dist_hor_diff = 1/32 * px_hor_diff; % cm
vis_ang_hor_diff = radtodeg(2*atan(dist_hor_diff/(2*(AD+AB))));

fprintf('Vertical visual angle offset is %6.4f degrees \n', vis_ang_ver_diff);
fprintf('Horizontal visual angle offset is %6.4f degrees \n', vis_ang_hor_diff);

%% Modify images based on smallest image
if strcmp(subID,'979')    
    sel_wid = max(wid(:));
    sel_hei = max(hei(:));
    
    scale = obj_px / sel_hei;
else
    sel_wid = min(wid(:));
    sel_hei = min(hei(:));
    
    scale = obj_px / sel_hei;
end

for img_num = 1:72
    temp_rgb = img_mod{img_num};
    temp_rgb_resized = imresize(temp_rgb,scale);
    
    resize_img_name = sprintf('%s%03d%s',[subID '_r'],(img_num-1)*5,'.png');
%     imshow(temp_rgb_resized);
%     imwrite(temp_rgb_resized,fullfile(Obj_dir,'Resized_images_new',resize_img_name));
    clear temp_rgb_resized;
end

%% Create a global bounding box
rmin_mtrx = zeros(1,72);
rmax_mtrx = rmin_mtrx;
cmin_mtrx = rmin_mtrx;
cmax_mtrx = rmin_mtrx;
wid_mtrx = rmin_mtrx;
hei_mtrx = rmin_mtrx;

for img_num = 1:72
    resize_img_name = sprintf('%s%03d%s',[subID '_r'],(img_num-1)*5,'.png');
    
    temp_rgb = imread(fullfile(Obj_dir,'Resized_images_new',resize_img_name));
    temp = im2double(rgb2gray(temp_rgb));
    temp(temp~=0) = 1;
    temp = im2bw(temp);
    % Delete small holes
    temp = imopen(temp,se);
    
    [r_obj,c_obj] = find(temp == 1);
    rmin = min(r_obj(:));rmax = max(r_obj(:));
    cmin = min(c_obj(:));cmax = max(c_obj(:));
    wid = cmax - cmin;
    hei = rmax - rmin;
    
    imshow(temp);hold on;
    rectangle('Position',[cmin rmin wid hei],'EdgeColor','g');
    
    rmin_mtrx(1,img_num) = rmin;
    rmax_mtrx(1,img_num) = rmax;
    cmin_mtrx(1,img_num) = cmin;
    cmax_mtrx(1,img_num) = cmax;
    wid_mtrx(1,img_num) = wid;
    hei_mtrx(1,img_num) = hei;
end

rmin_global = min(rmin_mtrx(:));
cmin_global = min(cmin_mtrx(:));
rmax_global = max(rmax_mtrx(:));
cmax_gloabl = max(cmax_mtrx(:));
wid_max_glob = cmax_gloabl - cmin_global;
hei_max_glob = rmax_global - rmin_global;
rectangle('Position',[cmin_global rmin_global wid_max_glob hei_max_glob],'EdgeColor','b');

%% Extract images based on global bounding box
% Configure offset to
if strcmp(subID,'62')
    l_off = 25;
    r_off = 22;
    t_off = 10;
    b_off = 10;
elseif strcmp(subID,'125')
    l_off = 30;
    r_off = 30;
    t_off = 10;
    b_off = 7;
elseif strcmp(subID,'164')
    l_off = 5;
    r_off = 20;
    t_off = 10;
    b_off = 15;
elseif strcmp(subID,'249')
    l_off = 5;
    r_off = -15;
    t_off = 25;
    b_off = 0;
elseif strcmp(subID,'701')
    l_off = 20;
    r_off = 20;
    t_off = 20;
    b_off = 20;  
elseif strcmp(subID,'794')
    l_off = 20;
    r_off = 5;
    t_off = 20;
    b_off = 20;
elseif strcmp(subID,'809')
    l_off = 0;
    r_off = 0;
    t_off = 10;
    b_off = 5;
elseif strcmp(subID,'866')
    l_off = 10;
    r_off = 0;
    t_off = 20;
    b_off = 20;
elseif strcmp(subID,'979')
    l_off = -5;
    r_off = 0;
    t_off = 0;
    b_off = 0;
end

cropped_img_all = cell(1,72);
for img_num = 1:72
    resize_img_name = sprintf('%s%03d%s',[subID '_r'],(img_num-1)*5,'.png');
    temp_rgb = imread(fullfile(Obj_dir,'Resized_images_new',resize_img_name));
    
    % cropped_img = temp_rgb(rmin_global:rmin_global+hei_max_glob,cmin_global:cmin_global+wid_max_glob,:);
    cropped_img = temp_rgb(rmin_global-t_off:rmin_global+hei_max_glob+b_off,cmin_global-l_off:cmin_global+wid_max_glob+r_off,:);
    
    cropped_img_name = sprintf('%s%03d%s',[subID '_r'],(img_num-1)*5,'.png');
%     figure;imshow(temp_rgb);
%     figure;imshow(cropped_img);
    
    imwrite(cropped_img,fullfile(Obj_dir,'Resized_images_new','Cropped_images',cropped_img_name));
    cropped_img_all{1,img_num} = cropped_img;
end
save(fullfile(Obj_dir,'Resized_images_new','Cropped_images',[subID '_cropped_imgs.mat']),'cropped_img_all');
