% Script to modify image background pixels
close all;
clear;

subID = '866';
datadir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/Manifold_learning_objects/';
cd(fullfile(datadir,subID));

switch subID
    case '62' % Duck
        quantile_val = 0.75;
        RGB_sel = 1;
    case '138' % Car
        quantile_val = 0.75;
        RGB_sel = 3;
    case '164' % Basketball
        quantile_val = 0.75;
        RGB_sel = 1;
    case '249' % Lego
        quantile_val = 0.75;
        RGB_sel = 1;  
    case '701' % Clock
        quantile_val = 0.75;
        RGB_sel = 1;  
    case '794' % Yellow mug
        quantile_val = 0.85;
        RGB_sel = 1; 
    case '979' % Hat
        quantile_val = 0.7;
        RGB_sel = 1;
    case '809' % Watering can
        quantile_val = 0.65;
        RGB_sel = 1;
    case '866' % Watering can
        quantile_val = 0.8;
        RGB_sel = 2;
end

% Use the first image to select a threshold
testimg = imread([subID '_r000.png']);
testimg_b = testimg(:,:,RGB_sel);
figure;imshow(testimg_b);
thresh_b = quantile(testimg_b(:),quantile_val);
if strcmp(subID,'809') || strcmp(subID,'164') || strcmp(subID,'866') 
    thresh_b = thresh_b + 5;
elseif strcmp(subID,'701')  
    thresh_b = thresh_b + 8;
elseif strcmp(subID,'794')
    thresh_b = thresh_b + 13;
end
testimg_b(testimg_b <= thresh_b) = 0;
figure;imshow(testimg_b)

% Modify images
imageNames = dir(fullfile(pwd,'*_r*.png'));
imageNum = length(imageNames);
img_mod = cell(1,imageNum);

for ctr = 1:imageNum
    f = imageNames(ctr).name;
    img = imread(f);
    img_single = img(:,:,RGB_sel);
    
    [r,c] = find(img_single <= thresh_b);
    
    for ctrj = 1:length(r)
        img(r(ctrj),c(ctrj),1:3) = 0;
    end
    img_mod{1,ctr} = img;
end

% Save images
mkdir('Modified_images');
cd('Modified_images');
for ctr = 1:imageNum
    tempimgname = sprintf('%s%03d%s',[subID '_r'],(ctr-1)*5,'.png');
    imwrite(img_mod{1,ctr},tempimgname);
end

FileName = [subID '_modified_img'];
save(fullfile(datadir,subID,'Modified_images',FileName),'img_mod');

