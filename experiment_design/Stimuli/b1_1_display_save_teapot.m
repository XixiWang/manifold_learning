% Test script to generate a rotating mug using OpenGL Psychtoolbox         
demodir = '/Applications/Psychtoolbox/PsychDemos/OpenGL4MatlabDemos';
datadir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/Manifold_learning_objects/teapot_new_viewpoint';

AssertOpenGL;
screenid=max(Screen('Screens'));
Screen('Preference','SkipSyncTests',1);

InitializeMatlabOpenGL(1);
[win,winRect] = Screen('OpenWindow', screenid);

% %%% Let's show stuff in a little tiny window, for debugging
% left_edge = 600;
% top_edge = 200;
% win_width = 600;
% win_height = 400;
% my_rect = [ left_edge top_edge (left_edge+win_width) (top_edge+win_height)];
% [win, winRect] = Screen('OpenWindow',screenid,0,my_rect);


Screen('BeginOpenGL', win);
ar=winRect(4)/winRect(3);

% Lightning
% % Turn on OpenGL local lighting model: The lighting model supported by
% % OpenGL is a local Phong model with Gouraud shading.
glEnable(GL_LIGHTING);
% % Enable the first local light source GL_LIGHT_0. Each OpenGL
% % implementation is guaranteed to support at least 8 light sources.
glEnable(GL_LIGHT0);
% % Enable two-sided lighting - Back sides of polygons are lit as well.
glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);

% % Enable proper occlusion handling via depth tests:
glEnable(GL_DEPTH_TEST);
 
% Define the cubes light reflection properties by setting up reflection
% c oefficients for ambient, diffuse and specular reflection:
blue = [0 1 1 1]; 

glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT, [ 0.5 0.5 0.5 0.5  ]);
glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,blue);
glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,blue);
glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,0);

% To disable this
% glDisable(GL_LIGHTING);  
 
%
% Set projection matrix: This defines a perspective projection,
% corresponding to the model of a pin-hole camera - which is a good
% approximation of the human eye and of standard real world cameras --
% well, the best aproximation one can do with 3 lines of code ;-)
glMatrixMode(GL_PROJECTION);
glLoadIdentity;

% Field of view is +/- 25 degrees from line of sight. Objects close than
% 0.1 distance units or farther away than 100 distance units get clipped
% away, aspect ratio is adapted to the monitors aspect ratio:
gluPerspective(25,1/ar,0.1,100);

% Setup modelview matrix: This defines the position, orientation and
% looking direction of the virtual camera:
glMatrixMode(GL_MODELVIEW);
glLoadIdentity;

% Cam is located at 3D position (0,0,10), points upright (0,1,0) and fixates
% at the origin (0,0,0) of the worlds coordinate system:
gluLookAt(0,3,5, 0,0,0, 0,1,0);

% Setup position and emission properties of the light source:

% Set background color to 'black':
glClearColor(0,0,0,0);

% Point lightsource at (1,2,3)...
glLightfv(GL_LIGHT0,GL_POSITION,[ 0 -5 0 0 ]);

% Emits white (1,1,1,1) diffuse light:
glLightfv(GL_LIGHT0,GL_DIFFUSE, [ 1 1 1 1 ]);

% Emits reddish (1,1,1,1) specular light:
glLightfv(GL_LIGHT0,GL_SPECULAR, [ 1 0 0 1 ]);

% There's also some blue, but weak (R,G,B) = (0.1, 0.1, 0.1)
% ambient light present:
glLightfv(GL_LIGHT0,GL_AMBIENT, [ .1 .1 .6 1 ]);

% Initialize amount and direction of rotation
theta=0;
% Rotates the teapot horizontally
% rotatev=[ 0 1 0 ]; % Counter-clockwise
rotatev=[ 0 -1 0]; % Clockwise 

t0 = GetSecs;
clockwise = 1;
% Animation loop: Run until key press...
 for ctr = 1:1440
    % Calculate rotation angle for next frame:
    theta=mod(theta+0.25,360);
   
    rotatev=rotatev+[ 0 -1 0];
% 
%     % Setup cubes rotation around axis:
    glPushMatrix;
    glRotated(theta,rotatev(1),rotatev(2),rotatev(3));

    % Clear out the backbuffer: This also cleans the depth-buffer for
    % proper occlusion handling:
    glClear;
    
    glutSolidTeapot(1.0);
    
    glPopMatrix;
    
    % Finish OpenGL rendering into PTB window and check for OpenGL errors.
    Screen('EndOpenGL', win);
    
    % Show rendered image at next vertical retrace:
    Screen('Flip', win);
    imgArray = Screen('GetImage',win);
        
    name = sprintf('teapot_%i.jpg',ctr);
    imwrite(imgArray,fullfile(datadir,name));  
    
    % Switch to OpenGL rendering again for drawing of next frame:
    Screen('BeginOpenGL', win);
    
    % Check for keyboard press and exit, if so:
    if KbCheck
        break; 
    end;
end
tEla = GetSecs - t0;

% Shut down OpenGL rendering:
Screen('EndOpenGL', win);

% Close onscreen window and release all other ressources:
Screen('CloseAll');

% Reenable Synctests after this simple demo:
Screen('Preference','SkipSyncTests',1);