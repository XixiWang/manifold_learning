% Script to read datasets and combine all images into a single .mat file
clear;
clc;

[~,hostname] = system('hostname');

if length(hostname) == 18 % Lab-Mac
    datadir = '~/Documents/Data/Manifold_learning_objects/';
elseif length(hostname) == 40
    datadir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/Manifold_learning_objects/';
end

subID = '866';

if strcmp(subID,'125')
    thresh = 0.15;
elseif strcmp(subID,'62')
    thresh = 0.35;
elseif strcmp(subID,'138')
    thresh = 0.35;
end
cd(fullfile(datadir,subID));

%% Rename files
imageNames = dir(fullfile(pwd,'*_r*.png'));
for id = 1:length(imageNames)
    f = imageNames(id).name;
    
    if length(f) == (length(subID)+7)
        tempimgnum = str2double(f(length(subID)+3));
        movefile(imageNames(id).name, sprintf('%s%03d%s',[subID '_r'],tempimgnum,'.png'));
    elseif length(f) == (length(subID)+8)
        tempimgnum = str2double(f(length(subID)+3:length(subID)+4));
        movefile(imageNames(id).name, sprintf('%s%03d%s',[subID '_r'],tempimgnum,'.png'));
    end
end

imageNames = dir(fullfile(pwd,'*_r*.png'));
imageNum = length(imageNames);
img_mod = cell(1,imageNum);

%% Modify images
for ctr = 1:imageNum
    f = imageNames(ctr).name;
    img = imread(f);
    
    % Modify each image
    img_bin = im2bw(img,thresh);
    imshow(img_bin);
    [r,c] = find(img_bin == 0);
    
    for ctrj = 1:length(r)
        img(r(ctrj),c(ctrj),1:3) = 0;
    end 
    img_mod{1,ctr} = img;
end

%% Save images
% mkdir('Modified_images');
% cd('Modified_images');
% for ctr = 1:imageNum
%     tempimgname = sprintf('%s%03d%s',[subID '_r'],(ctr-1)*5,'.png');
%     imwrite(img_mod{1,ctr},tempimgname);
% end

FileName = [subID '_modified_img'];
save(fullfile(datadir,subID,'Modified_images',FileName),'img_mod');
