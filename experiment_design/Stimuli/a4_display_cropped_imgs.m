% Display cropped image examples
clear;
close all;
cd('/Volumes/Macintosh HDD/Dropbox/Manifold_learning_pilot');

subID = [62 125 164 249 701 809 866 979];
for ctr = 1:length(subID)
    load([num2str(subID(ctr)) '_rotating_cropped']);
    
    img = rotatingImgs{1,1};
    figure;
    imshow(img);
    title(['subID-' num2str(subID(ctr))]);
    
    clear rotatingImgs img
end