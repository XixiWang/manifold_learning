function UtahTeapotDemo_modified(num_rot, rot_dir, first_frame_num, teapot_size, angle_incre, rot_period, degree_per_TR)
%
% UtahTeapotDemo_modified(num_rot[, rot_dir, first_frame_num, teapot_size, angle_incre, rot_period, degree_per_TR])
% function to display the Utah teapot, adapted from PsychToolbox's
% UtahTeapotDemo function. Modified by Xixi Wang.
%
%   Required input variables:
%
%       num_rot: number of complete rotations
%
%   Optional input variables:
%
%       rot_dir: rotation direction. 1 for counter-clockwise and -1 for
%           clockwise, default is 1
%
%       first_frame_num: first frame number, default is 1 (or 0?!)
%
%       teapot_size: relative teapot size - (0,1], default value is 1
%
%       angle_incre: angle increment in degrees, default is 0.25 degrees
%
%       rot_period: rotation period in seconds, default is
%       (72 - 1) * 2 = 142 s
%
%       degree_per_TR: rotation degree per each TR, default is 5 degrees
%       (to match with the discrete stimuli)
%
%       
%   How to define the first slice number? 


% Set up default values
if nargin == 0
    error('Not enough input variables');
end
if nargin < 2
    rot_dir = 1;
end
if nargin < 3
    first_frame_num = 1;
end
if nargin < 4
    teapot_size = 1;
end
if nargin < 5
    angle_incre = 0.25;
end
if nargin < 6
    rot_period = 142;
end
if nargin < 7
    degree_per_TR = 5;
end

% Is the script running in OpenGL Psychtoolbox?
AssertOpenGL;

% Find the screen to use for display:
screenid=max(Screen('Screens'));

% Disable Synctests for this simple demo:
Screen('Preference','SkipSyncTests',1);

% Setup Psychtoolbox for OpenGL 3D rendering support and initialize the
% mogl OpenGL for Matlab wrapper:
InitializeMatlabOpenGL(1);

% Open a double-buffered full-screen window on the main displays screen.
% [win , winRect] = Screen('OpenWindow', screenid);

%%% Let's show stuff in a little tiny window, for debugging
left_edge = 600;
top_edge = 200;
win_width = 600;
win_height = 400;
my_rect = [ left_edge top_edge (left_edge+win_width) (top_edge+win_height)];
[win, winRect] = Screen('OpenWindow',screenid,0,my_rect);

% Setup the OpenGL rendering context of the onscreen window for use by
% OpenGL wrapper. After this command, all following OpenGL commands will
% draw into the onscreen window 'win':
Screen('BeginOpenGL', win);

% Get the aspect ratio of the screen:
ar=winRect(4)/winRect(3);

% Turn on OpenGL local lighting model: The lighting model supported by
% OpenGL is a local Phong model with Gouraud shading.
glEnable(GL_LIGHTING);

% Enable the first local light source GL_LIGHT_0. Each OpenGL
% implementation is guaranteed to support at least 8 light sources.
glEnable(GL_LIGHT0);

% Enable two-sided lighting - Back sides of polygons are lit as well.
glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);

% Enable proper occlusion handling via depth tests:
glEnable(GL_DEPTH_TEST);

% Define the cubes light reflection properties by setting up reflection
% coefficients for ambient, diffuse and specular reflection:
% blue = [0 0.8 0.8 1];
blue = [0 1 1 1];

glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT, [ 0.5 0.5 0.5 0.5]);
glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,blue);
glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,blue);
glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,0);

% Set projection matrix: This defines a perspective projection,
% corresponding to the model of a pin-hole camera - which is a good
% approximation of the human eye and of standard real world cameras --
% well, the best aproximation one can do with 3 lines of code ;-)
glMatrixMode(GL_PROJECTION);
glLoadIdentity;

% Field of view is +/- 25 degrees from line of sight. Objects close than
% 0.1 distance units or farther away than 100 distance units get clipped
% away, aspect ratio is adapted to the monitors aspect ratio:
gluPerspective(25,1/ar,0.1,100);

% Setup modelview matrix: This defines the position, orientation and
% looking direction of the virtual camera:
glMatrixMode(GL_MODELVIEW);
glLoadIdentity;

% Cam is located at 3D position (0,0,10), points upright (0,1,0) and fixates
% at the origin (0,0,0) of the worlds coordinate system:
% gluLookAt(0,0,10,0,0,0,0,1,0);

% gluLookAt(eyex,eyey,eyez,ctrx,ctry,ctrz,upx,upy,upz)
% Modify cam location
% gluLookAt(5,5,-5, 0,0,0, 0,1,0); %---------Modified view
gluLookAt(-5,3,5, 0,0,0, 0,1,0);

% Setup position and emission properties of the light source:

% Set background color to 'black':
glClearColor(0,0,0,0);

% Point lightsource at (1,2,3)...
% glLightfv(GL_LIGHT0,GL_POSITION,[ 0 0 -100 0 ]);
glLightfv(GL_LIGHT0,GL_POSITION,[ 0 -5 0 0 ]);

% Emits white (1,1,1,1) diffuse light:
glLightfv(GL_LIGHT0,GL_DIFFUSE, [ 1 1 1 1 ]);

% Emits reddish (1,1,1,1) specular light:
glLightfv(GL_LIGHT0,GL_SPECULAR, [ 1 0 0 1 ]);

% There's also some blue, but weak (R,G,B) = (0.1, 0.1, 0.1)
% ambient light present:
glLightfv(GL_LIGHT0,GL_AMBIENT, [ .1 .1 .6 1 ]);

% Initialize amount and direction of rotation
theta=0;
rotatev = [ 0 rot_dir 0 ];

% Modify the timing parameters
frame_num_per_TR = degree_per_TR / angle_incre;
frame_num_per_rot = 360 / degree_per_TR * frame_num_per_TR;
time_delay_per_frame = rot_period / frame_num_per_rot;
tot_frame_num = frame_num_per_rot * num_rot;

dataFile = fopen('test_teapot.txt', 'wt');

runStartTime = GetSecs(); %%%%%%%%%%%%%%%%%%%%%%%%%% Check the start time!!!!!
for nframe = 1: tot_frame_num
    
    % Calculate rotation angle for next frame
    theta = mod(theta + angle_incre,360);
    rotatev = rotatev + [0 rot_dir 0];
    
    % Calculate rotation angle for next frame:
    % Setup cubes rotation around axis:
    glPushMatrix;
    glRotated(theta,rotatev(1),rotatev(2),rotatev(3));
    
    % time_string = num2str(GetSecs-t0, 3);
    % current_angle = (nframe - 1) * angle_incre;
    % Display timing for debugging
    % disp(['Time is ' time_string 's. Angle is ' num2str(current_angle) ' degrees']);
    
    % Clear out the backbuffer: This also cleans the depth-buffer for
    % proper occlusion handling:
    glClear;
    glutSolidTeapot(teapot_size); % Modify the relative size of teapot
    glPopMatrix;
    % Finish OpenGL rendering into PTB window and check for OpenGL errors.
    Screen('EndOpenGL', win);
    % Show rendered image at next vertical retrace:
    % Screen('Flip', win);
    [~, StimulusOnsetTime, ~] = Screen('Flip', win, runStartTime + time_delay_per_frame * nframe);
    
    % --------------------------------------
    % Print data to output file
    % fprintf(dataFile, '%f\t\n', StimulusOnsetTime - runStartTime);
    % --------------------------------------
    
    % Switch to OpenGL rendering again for drawing of next frame:
    Screen('BeginOpenGL', win);
    
    %     time_string = num2str(GetSecs-runStartTime, 3);
    %     if rem(theta,5)==0,
    %         disp(['Time is ' time_string 's. Angle is ' num2str(theta) ]);
    %     end;
end
fprintf(dataFile, 'Total time: %4.2f',GetSecs - runStartTime);

fclose(dataFile);
% Shut down OpenGL rendering:
Screen('EndOpenGL', win);

% Close onscreen window and release all other ressources:
Screen('CloseAll');

% Reenable Synctests after this simple demo:
Screen('Preference','SkipSyncTests',1);

% Well done!
return

%     %% print data to output file
%     fprintf(dataFile, '%i\t', trial); % 1-numSets*numImgsinSet
%     fprintf(dataFile, '%i\t', set); % which set within the run; 1-numSets
%     fprintf(dataFile, '%i\t', setOrder(run,set)); % which obj; 1-numSets
%     fprintf(dataFile, '%i\t', imgOrder(imgIndex)); % which img within a set; 1-numImgsinaSet
%     fprintf(dataFile, '%f\t\n', StimulusOnsetTime-runStartTime); % stimulus onset relative to run start
