function [current_TR, current_angle, tot_rot_duration] = RotateDiscreteStimuli(num_rot,varargin) 
% 
% RotateDiscreteStimuli(num_rot, [first_frame_num,rot_dir, TR, angle_incre])
% function to display discrete rotating stimuli
% Written by Xixi. 11/04/2015
% 
%   Required input variables:
% 
%       num_rot: number of complete rotations
%
%   Optional input variables:
% 
%       first_frame_num: first frame number. Default first frame number = 0
% 
%       rot_dir: rotation direction. 1 for clockwise and -1 for
%       counter-clockwise. Default is 1. 
% 
%       TR: default TR = 2s
% 
%       angle_incre: angle increment in degrees. Default angle_incre = 5
%       degrees

% Set up default values
numvarargs = length(varargin);

if nargin == 0
    error('Not enough input variables');
elseif numvarargs >  4
    error('Too many optional inputs');
end
   
% Fill in optional values
optargs = {0 1 2 5};
optargs(1:numvarargs) = varargin;

[first_frame_num, rot_dir, TR, angle_incre] = optargs(:);

% 

return