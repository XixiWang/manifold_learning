% Script to resize teapot
clc;
clear;
close all;

datadir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/Manifold_learning_objects';
cd(fullfile(datadir,'teapot_new_viewpoint'));

%% Visual angle calculation
vis_ang_deg = 5; % degree
AD = 14;
AB = (101 + 109.3)/2; % cm

obj_size = tan(degtorad(vis_ang_deg)/2) * (AD+AB) * 2;
vis_ang_calc = radtodeg(2*atan(obj_size/(2*(AD+AB))));
fprintf('Visual angle is %1.1f degrees \nObject size is %6.4f cm\n',...
    vis_ang_calc,obj_size)

% Convert object size to pixels
ppcm = 32; % monitor property: pixels per cm
obj_px = ppcm * obj_size;
obj_px_round = round(obj_px);
fprintf('Object size in px: %d pixels\n',obj_px_round);
fprintf('\n');

%% Select image subset to calculate scale ratio
subset_num = 10;
wid = zeros(1,subset_num);
hei = wid;
rmin = wid; rmax = wid;
cmin = wid; cmax = wid;

for img_num = 1:subset_num
    img_name = ['teapot_' num2str(img_num) '.jpg'];
    temp_rgb = imread(img_name);
    temp = im2double(rgb2gray(temp_rgb));
    temp(temp~=0) = 1;
    temp = im2bw(temp);
    
    [r_obj,c_obj] = find(temp == 1);
    rmin(1,img_num) = min(r_obj(:));
    rmax(1,img_num) = max(r_obj(:));
    cmin(1,img_num) = min(c_obj(:));
    cmax(1,img_num) = max(c_obj(:));
    wid(1,img_num) = cmax(1,img_num) - cmin(1,img_num);
    hei(1,img_num) = rmax(1,img_num) - rmin(1,img_num);
end

sel_wid = min(wid(:));
sel_hei = min(hei(:));

scale = obj_px / sel_hei;

%% Create a global bounding box using rescaled images
rmin_mtrx = zeros(1,subset_num);
rmax_mtrx = rmin_mtrx;
cmin_mtrx = rmin_mtrx;
cmax_mtrx = rmin_mtrx;
wid_mtrx = rmin_mtrx;
hei_mtrx = rmin_mtrx;

for img_num = 1:subset_num
    img_name = ['teapot_' num2str(img_num) '.jpg'];
    temp_rgb = imread(img_name);
    % Resize image
    temp_rgb_resized = imresize(temp_rgb,scale);
    % Convert to binary image
    temp_rgb_resized_bin = im2double(rgb2gray(temp_rgb_resized));
    temp_rgb_resized_bin(temp_rgb_resized_bin~=0) = 1;
    temp_rgb_resized_bin = im2bw(temp_rgb_resized_bin);
    
    [r_obj,c_obj] = find(temp_rgb_resized_bin == 1);
    rmin = min(r_obj(:));rmax = max(r_obj(:));
    cmin = min(c_obj(:));cmax = max(c_obj(:));
    wid = cmax - cmin;
    hei = rmax - rmin;
    
    % Display the resized image and the corresponding bounding boxes
    % imshow(temp_rgb_resized_bin);hold on;
    % rectangle('Position',[cmin rmin wid hei],'EdgeColor','g');
    
    rmin_mtrx(1,img_num) = rmin;
    rmax_mtrx(1,img_num) = rmax;
    cmin_mtrx(1,img_num) = cmin;
    cmax_mtrx(1,img_num) = cmax;
    wid_mtrx(1,img_num) = wid;
    hei_mtrx(1,img_num) = hei;
end

rmin_global = min(rmin_mtrx(:));
cmin_global = min(cmin_mtrx(:));
rmax_global = max(rmax_mtrx(:));
cmax_gloabl = max(cmax_mtrx(:));
wid_max_glob = cmax_gloabl - cmin_global;
hei_max_glob = rmax_global - rmin_global;

rectangle('Position',[cmin_global rmin_global wid_max_glob hei_max_glob],'EdgeColor','b');

%% Crop and save images
l_off = 65;
r_off = 35;
t_off = 50;
b_off = 30;

cropped_img_all = cell(1,1440);
for img_num = 1:1440
% for img_num = 635 %730 - to testify several specific images
    img_name = ['teapot_' num2str(img_num) '.jpg'];
    temp_rgb = imread(img_name);
    temp_rgb = imresize(temp_rgb,scale);
    
    cropped_img = temp_rgb(rmin_global-t_off:rmin_global+hei_max_glob+b_off,cmin_global-l_off:cmin_global+wid_max_glob+r_off,:);    
    % figure; imshow(cropped_img);
    resize_img_name = ['teapot_' num2str(img_num) '_cropped.png']; 

    % imwrite(cropped_img,fullfile(datadir,'teapot_new_viewpoint','cropped',resize_img_name));
    cropped_img_all{1,img_num} = cropped_img;
end
% save(fullfile(pwd,'cropped','teapot_cropped_all.mat'),'cropped_img_all');

%% Save new images
rotatingImgs = cropped_img_all;
save(fullfile(pwd,'cropped','teapot_cropped_all.mat'),'rotatingImgs');

%% Modified block
load(fullfile(pwd,'cropped','teapot_cropped_all.mat'),'rotatingImgs');

rotatingImgs = rotatingImgs + 1;
for ctr = 1:length(rotatingImgs)
    img_name = ['teapot_' num2str(ctr) '.jpg'];
    temp_rgb = imread()
end