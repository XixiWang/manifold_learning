function extract_masked_timeseries(bold_dir, bold_type, mask_dir, mask_name, output_dir, output_name)
% extract_masked_timeseries(bold_dir, bold_type, mask_dir, mask_name, output_dir, output_name):
% extract and save time series from ROI.nii/mask.nii files
%
% Input:
%   bold_dir: path to the bold dataset. i.e.
%   ***/Data_ToolRotation/ToolRotation02/ToolRotationRun01
%
%   bold_type: 'smoothed' or 'unsmoothed'
%
%   mask_dir: path to the mask/roi directory. i.e.
%   ***/Data_ToolRotation/ToolRotation02/ROI_harv_oxf
%
%   mask_name: mask/roi name. mask should be .nii file
%
%   output_dir: path to the output time series matrix, default is
%   bold_dir/default_masked_ts
%
%   output_name: output time series matrix name, can be specified without 
%   extension, default is [bold_type_masked_ts.mat]
%
% Update 1/19/2016:
% - add check_var_ext function
% Update 1/21/2016:
% - call GetFullPath function

bold_dir = GetFullPath(bold_dir);
if nargin < 5
    output_dir = fullfile(bold_dir, 'default_masked_ts');
end

if nargin < 6
    output_name = [bold_type '_masked_ts.mat'];
end

output_name = check_var_ext(output_name, '.mat');
if exist(fullfile(output_dir, output_name), 'file')
    [~, temp_output_dir, ~] = fileparts(output_dir);
    fprintf('%s/%s already exists! \n\n', temp_output_dir, output_name);
    return;
end

if ~exist(output_dir, 'dir')
    mkdir(output_dir);
    fprintf('mkdir %s \n\n', output_dir);
end

[~, ~, mask_dbtype] = fileparts(mask_name);

mask_dir = GetFullPath(mask_dir);
if strcmp(mask_dbtype, '.nii')
    addpath('~/Documents/Code/manifold_learning/analyses');
    roi_hdr = spm_vol(fullfile(mask_dir, mask_name));
    roi_vol = spm_read_vols(roi_hdr);
    roi_ijk_coords = get_roi_coords(roi_vol);
else
    error('mask should be .nii file!');
end

% Use time series without fixation
if strcmp(bold_type, 'smoothed')
    bold_file = dir(fullfile(bold_dir, 'smoothed*_timeseries_nofixation.mat'));
    bold_file = bold_file.name;
elseif strcmp(bold_type, 'unsmoothed')
    bold_file = dir(fullfile(bold_dir, 'unsmoothed*_timeseries_nofixation.mat'));
    bold_file = bold_file.name;
end

load(fullfile(bold_dir, bold_file));
fprintf('Loading %s \n\n', bold_file);

[sela, selb] = ismember(roi_ijk_coords, ijk_coords, 'rows');
no_zeros = length(find(sela == 0));
fprintf('Remove %d voxels \n\n', no_zeros);

selb(selb == 0) = [];
roi_zero_meaned_ts = zero_meaned_ts(:, selb);
roi_ijk_coords = ijk_coords(selb, :);
roi_mni_coords = mni_coords(selb, :);

[~, temp_output_dir, ~] = fileparts(output_dir);
fprintf('Saving %s/%s \n\n', temp_output_dir, output_name);
save(fullfile(output_dir, output_name), 'roi_zero_meaned_ts', ...
    'roi_ijk_coords', 'roi_mni_coords');
end