function [bold_type,roi_name] = read_roi_txt_info(roi_info_text)
% Quick function to read ri.txt file
% Input:
%   roi_info_text: .txt file that end with *_dim_red
% 
%   newchar: new character that is used to replace * in roi names with 
%   run index
% 
% Update 1/19/2016:
% - modify todo_rois_dim_red.txt file

if ~exist(roi_info_text, 'file')
    error('Can not find roi_info text file!!');
end

[~, temp, ~] = fileparts(roi_info_text);

% roi_dim_red
if strcmp(temp(end - 7: end), '_dim_red')
    fid = fopen(roi_info_text);
    all_info = textscan(fid, '%s %s', 'Delimiter','\t', 'CommentStyle', {'/*', '*/'});
    bold_type = all_info{1, 1};
    roi_name = all_info{1, 2};
    % Optional output
else
    error('Check text file name!');
end
end

% % roi_info
% if strcmp(temp(end - 4: end), '_info')
%     fid = fopen(roi_info_text);
%     all_info = textscan(fid,'%s %s %s %d', 'Delimiter','\t', 'CommentStyle', {'/*', '*/'});
%     
%     bold_type = all_info{1, 1};
%     roi_dir = all_info{1, 2};
%     roi_name = all_info{1, 3};
%     run_idx = all_info{1,4};
%     
%     fclose(fid);
