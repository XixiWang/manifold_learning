load('StimuliOrder.mat');

StimuliInfo = {'fork', 'knife', 'scissors', 'spoon'};
StimuliNames = StimuliInfo(StimuliOrder);

save(fullfile('~/Documents/Data/caos_toolrotation/Data_ToolRotation', 'StimuliOrder.mat'), ...
    'StimuliOrder', 'StimuliNames');