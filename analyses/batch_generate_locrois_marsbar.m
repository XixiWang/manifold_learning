% Batch run marsbar to generate functional ROIs using marsbar
% Output *_roi.mat and *_roi.nii files
%
% Modified based on run_tutorial.m from marsbar
%
% Updated Dec 21, 2015: Change p values and cluster size manually
% Analyzing ToolRotation03-Run03 now

clear;
close all;
clc;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/Toolboxes/spm8/toolbox/marsbar/');

% Start marsbar to make sure spm_get works
spm('Defaults', 'fmri');
marsbar('on')

nsub_dir = toolrotationBaseDir;

% Load saved contrast paramters
fileID = fopen('contrast_info.txt', 'r');
Intro = textscan(fileID, '%s %s %s %s', 1);
contrast_data = textscan(fileID, '%f %f %f %s', 7);
RunNum = contrast_data{1, 1};
p_T_val = contrast_data{1, 2};
ClusSize = contrast_data{1, 3};
Quality = contrast_data{1, 4};
fclose(fileID);

% Batch process each localizer run
type = 'smoothed';
localizer_dir_listing = dir('LocalizerRun*')';
for runIdx = 1:(length(localizer_dir_listing) - 1)
% for runIdx = 5;
    loc_run_name = localizer_dir_listing(runIdx).name;
    loc_dir = fullfile(nsub_dir, loc_run_name);
    
    if strcmp(Quality{runIdx, 1}, 'S')
        fprintf('Skip %s \n', loc_run_name);
        continue;
    end
    
    if strcmp(type, 'smoothed')
        spm_output_dir = fullfile(loc_dir, 'glm_results_ind_loc', 'smoothed');
        loc_file = dir(fullfile(loc_dir, 'swraToolRotation*bold*.nii'));
    elseif strcmp(type, 'unsmoothed')
        spm_output_dir = fullfile(loc_dir, 'glm_results_2ind_loc', 'unsmoothed');
        loc_file = dir(fullfile(loc_dir, 'wraToolRotation*bold*.nii'));
    end
    
    % Get roi space
    loc_V = spm_vol(fullfile(loc_dir, loc_file.name));
    roi_space = mars_space(loc_V);
    
    cd(spm_output_dir);
    roi_dir = fullfile(spm_output_dir, 'rois');
    % Directory to store (and load) ROIs
    if ~exist(roi_dir, 'dir');
        mkdir(roi_dir);
    end
    
    if exist(fullfile(roi_dir, 'sel_overall_roi.mat'), 'file')
        fprintf('ROI files already exist for %s \n', loc_run_name);
        continue;
    end
    
    % MarsBaR version check
    v = str2num(marsbar('ver'));
    if v < 0.35
        error('Batch script only works for MarsBaR >= 0.35');
    end
    
    % SPM version check. We need this to guess which model directory to use and
    % to get the SPM configured design name.
    spm_ver = spm('ver');
    
    % Get SPM model
    glm_model = mardo('SPM.mat');
    
    % Get activation cluster by loading T image
    con_name = 'Tools > Others';
    t_con = get_contrast_by_name(glm_model, con_name);
    if isempty(t_con)
        error(['Cannot find the contrast ' con_name ...
            ' in the design; has it been estimated?']);
    end
    
    t_con_fname = t_con.Vspm.fname;
    t_pth = fileparts(t_con_fname);
    if isempty(t_pth)
        t_con_fname = fullfile(spm_output_dir, t_con_fname);
    end
    if ~exist(t_con_fname, 'file')
        error(['Cannot find t image ' t_con_fname ...
            '; has it been estimated?']);
    end
    
    % Get t threshold of uncorrected p < 0.001
    thresh_val = p_T_val(runIdx, 1);
    if thresh_val < 1
        p_thresh = thresh_val;
        erdf = error_df(glm_model);
        t_thresh = spm_invTcdf(1-p_thresh, erdf);
    else
        t_thresh = thresh_val;
    end
    
    
    % get all voxels from t image above threshold
    V = spm_vol(t_con_fname);
    img_vol = spm_read_vols(V);
    thre_ind = find(img_vol(:) > t_thresh);
    XYZ = mars_utils('e2xyz', thre_ind, V.dim(1:3));
    % Save all clusters
    all_rois = maroi_pointlist(struct('XYZ', XYZ, 'mat', V.mat), 'vox');
    saveroi(all_rois, fullfile(roi_dir, 'overall_roi.mat'));
    mars_rois2img(fullfile(roi_dir, 'overall_roi.mat'), ...
        fullfile(roi_dir, 'overall_roi.nii'), roi_space);
    
    % Save individual clusters
    sel_all_XYZ = [];
    
    cluster_idx = spm_clusters(XYZ);
    tot_cluster_num = max(cluster_idx);
    cluster_size = ClusSize(runIdx, 1);
    
    for ctr = 1:tot_cluster_num
        ind_cluster = ctr;
        ind_cluster_voxel_num = length(find(cluster_idx == ind_cluster));
        if ind_cluster_voxel_num < cluster_size
            continue;
        end
        
        ind_cluster_XYZ = XYZ(:, cluster_idx == ind_cluster);
        
        ind_roi = maroi_pointlist(struct('XYZ', ind_cluster_XYZ, 'mat', V.mat), 'vox');
        ind_roi_name = ['ind_' num2str(ctr) '_roi.mat'];
        
        % Save ROIs
        saveroi(ind_roi, fullfile(roi_dir, ind_roi_name));
        % save_as_image(ind_roi, fullfile(roi_dir, 'test.img'));
        
        sel_all_XYZ = [sel_all_XYZ ind_cluster_XYZ];
    end
    sel_all_rois= maroi_pointlist(struct('XYZ', sel_all_XYZ, 'mat', V.mat), 'vox');
    saveroi(sel_all_rois, fullfile(roi_dir, 'sel_overall_roi.mat'));
    mars_rois2img(fullfile(roi_dir, 'sel_overall_roi.mat'), ...
        fullfile(roi_dir, 'sel_overall_roi.nii'), roi_space);
    
end

% % % Check ROIs
% cd(roi_dir);
% marsbar;
