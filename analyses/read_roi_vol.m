function [roi_vol, roi_name, roi_hdr] = read_roi_vol(roi_dir)

roi_name = uigetfile('*.nii', 'Select ROI file', roi_dir);
roi_hdr = spm_vol(fullfile(roi_dir, roi_name));
roi_vol = spm_read_vols(roi_hdr);
end