function [zero_meaned_ts, ijk_coords, mni_coords] = extract_filter_ts(bold_data_dir, spm_output_dir, type, fixation)
% [zero_meaned_ts, xyz_coords, mni_coords] = extract_filter_ts(bold_data_dir, spm_output_dir, type[, fixation]):
% extract time series for the whole brain and then high pass filter the
% time series. All the settings are based on the spm-glm output files
% (mask.img and SPM.mat).
%
% Modified based on Raj's code: get_time_courses_of_mask_voxels.m and
% vox2mni_matrix.m
%
% Input:
%   bold_data_dir: bold data directory
%
%   spm_output_dir: spm-glm output directory
%
%   type: dataset type, 'smoothed' or 'unsmoothed'
%
%   fixation: optional, fixation period (before rotation) in number of TRs
%
% Output:
%   zero_meaned_ts: zero-meaned and high-pass filtered time-series matrix.
%   Each row corresponds to the voxels in a slice and each column
%   corresponds to a time series
%
%   ijk_coords: ijk coordinates for all voxels (index from 1)
%
%   mni_coords: mni coordinates for all voxels (start from 0)
%
% Update 12/16/2015: 
% - change xyz_coords to ijk_coords
% - add fixation period

if nargin < 3
    error('Not enough input parameters');
end

if nargin == 3
    % Default: no fixation period
    fixation = 0;
end

%%% Select spm files
cd(spm_output_dir);

Vmask = spm_vol(fullfile(spm_output_dir, 'mask.img'));
mask_matrix = spm_read_vols(Vmask);
mask_inds = find(mask_matrix);     %%% The indices of where mask=1
num_mask_voxels = length(mask_inds);

spm_get_defaults;
load SPM.mat;
num_runs = 1; % Right now I'm analyzing each run separately
num_time_points_per_run = size( SPM.xY.VY, 1 )/num_runs;

[i_in_mask,j_in_mask,k_in_mask]=ind2sub(size(mask_matrix),mask_inds);
%%% XYZ has three rows, and one col for every voxel in the mask
ijk_coords = [i_in_mask,j_in_mask,k_in_mask]';

%%% Select bold dataset
cd(bold_data_dir);
if strcmp(type, 'smoothed')
    bold_file = dir(fullfile(bold_data_dir, 'swraToolRotation*bold*.nii'));
    bold_file = bold_file.name;
elseif strcmp(type, 'unsmoothed')
    bold_file = dir(fullfile(bold_data_dir, 'wraToolRotation*bold*.nii'));
    bold_file = bold_file.name;
end
fprintf('Analyzing %s \n', bold_file);

% Read bold dataset and get parameters
hdr = spm_vol(fullfile(bold_data_dir, bold_file));
vol = spm_read_vols(hdr);
num_all_vols = size(vol, 4);
% Updated on Dec 16, 2015: add fixation period
dis_acqs = 2;

if strcmp(type, 'smoothed')
    [bold_sel_files, dirs]=spm_select('ExtFPList', bold_data_dir, '^swra\w*.ep2d_bold', (1 + dis_acqs):num_all_vols);
elseif strcmp(type, 'unsmoothed')
    [bold_sel_files, dirs]=spm_select('ExtFPList', bold_data_dir, '^wra\w*.ep2d_bold', (1 + dis_acqs):num_all_vols);
end

bold_files_cell_array = cellstr(bold_sel_files);
raw_ts_for_this_run = spm_get_data(bold_files_cell_array, ijk_coords);

%%% To turn these raw images into SPM-filtered data,
%%% we multiply by gSF scale-factor, filter, then zero-mean
%%% We'll do this for each voxel separately
scaling_dot_times = SPM.xGX.gSF;
scaled_raw_time_courses_for_all_runs = zeros(size(raw_ts_for_this_run));

num_voxels_in_IJK = size(ijk_coords,2);

% disp('Now applying the scaling factor (voxel-by-voxel to save memory)');
for voxel_num = 1:num_voxels_in_IJK,
    
    scaled_raw_time_courses_for_all_runs(:,voxel_num) = ...
        scaling_dot_times .* raw_ts_for_this_run(:,voxel_num);
end;

clear raw_time_courses_for_all_runs;

%%% Now filter the time-courses
%%% High-pass filter, to remove slow-drifts, is SPM.xX.K
filtered_time_courses_for_all_runs = spm_filter(SPM.xX.K,scaled_raw_time_courses_for_all_runs);
clear scaled_raw_time_courses_for_all_runs;

%%% Finally, zero-mean the time-courses
%%% The mean for each run is different
%%% Let's start off with this at full size, but full of zeros
num_new_vols = num_all_vols - dis_acqs - fixation;
zero_meaned_ts = zeros(num_new_vols*num_runs, num_voxels_in_IJK);

% time_points_for_this_run = 1:num_time_points_per_run;
time_points_for_this_run = 1 + fixation : num_time_points_per_run;
filtered_time_courses_for_this_run = ...
    filtered_time_courses_for_all_runs(time_points_for_this_run,:);
%%% filtered_time_courses_for_this_run has 925 rows and num_voxels columns
vec_of_means_for_each_voxel = mean(filtered_time_courses_for_this_run);

%%% We have one mean for each voxel
%%% The quickest way of subtracting all these means at once
%%% is to make a matrix which is the same size as
%%% filtered_time_courses_for_this_run
matrix_of_means_to_subtract = ...
    ones(length(time_points_for_this_run) ,1)*vec_of_means_for_each_voxel;

zero_meaned_ts = ...
    filtered_time_courses_for_this_run  - matrix_of_means_to_subtract;

clear matrix_of_means_to_subtract filtered_time_courses_for_this_run  vec_of_means_for_each_voxel

%%%% Let's fill in that part of the matrix: all of the voxels, and this
%%%% run's time-points

ijk_coords = ijk_coords';

% Convert xyz_coords to mni_coords (so that we can visualize them in MRIcro!!)
mni_coords = vox2mni_matrix(ijk_coords, Vmask);

end