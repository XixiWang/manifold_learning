function [ave_brain_pattern, sel_ts_mtrx_all, pattern_ijk, pattern_mni] = gen_ave_pattern_across_runs(nsub_dir, roi_name, bold_type, ts_mtrx_folder)
% [ave_brain_pattern, sel_ts_mtrx_all, pattern_ijk, pattern_mni] = gen_ave_pattern_across_runs(nsub_dir, roi_name[, bold_type, ts_mtrx_folder])
% function to generate averaged brain pattern across multiple runs
%
% Input:
%   nsub_dir: subject's directory, i.e.
%   .../Data_ToolRotation/ToolRotation09
%
%   roi_name: roi name, i.e. 'FrontalPole'
% 
%   bold_type: bold data type, default is 'unsmoothed';
% 
%   ts_mtrx_dir: directory to the time series matrix, default is 'roi_mtrx'
%
% Output:
%   ave_brain_pattern: averaged brain pattern across multiple runs
% 
%   sel_ts_mtrx_all: selected time series matrix for each run
% 
%   pattern_ijk
% 
%   pattern_mni
% 
% TODO:
%   Exclude some runs?!

if nargin < 3
    bold_type = 'unsmoothed';
end

if nargin < 4
    ts_mtrx_folder = 'roi_mtrx';
end

bold_dir_listing = dir(fullfile(nsub_dir, 'ToolRotationRun*'));
ts_mtrx_all = cell(1, length(bold_dir_listing));
ts_ijk_all = ts_mtrx_all;
ts_mni_all = ts_mtrx_all;
nvox_per_run = zeros(1, length(bold_dir_listing));
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;

    % Load time series matrix for roi
    ts_mtrx_dir = fullfile(nsub_dir, bold_run_name, ts_mtrx_folder);
    ts_mtrx_name = [bold_type '_' bold_run_name '_' roi_name '.mat'];    
    [ts_mtrx, ts_ijk_coords, ts_mni_coords] = load_ts_mtrx(ts_mtrx_dir, ts_mtrx_name);
    
    % save data for each run
    ts_mtrx_all{1, runIdx} = ts_mtrx;
    ts_ijk_all{1, runIdx} = ts_ijk_coords;
    ts_mni_all{1, runIdx} = ts_mni_coords;
    
    nvox_per_run(1, runIdx) = size(ts_ijk_coords, 1);
end

[final_vox_num, tempidx] = min(nvox_per_run(:));
clear ts_mtrx ts_ijk_coords ts_mni_coords
fprintf('%d voxels in total for %s \n\n', final_vox_num, roi_name);

% Remove extra voxels
sel_ts_mtrx_all = cell(1, length(bold_dir_listing));
sel_ijk_coords_all = sel_ts_mtrx_all;
pattern_ijk = ts_ijk_all{1, tempidx};
pattern_mni = ts_mni_all{1, tempidx};

for runIdx = 1:length(bold_dir_listing)
    ts_ijk_coords = ts_ijk_all{1, runIdx};
    ts_mtrx = ts_mtrx_all{1, runIdx};
    if size(ts_ijk_coords, 1) > final_vox_num
        [~, Locb] = ismember(ts_ijk_coords, pattern_ijk, 'rows');        
        sel_ts_mtrx_all{1, runIdx} = ts_mtrx(:, Locb~= 0);
        sel_ijk_coords_all{1, runIdx} = ts_ijk_coords(Locb~=0, :);
        warning('Remove %d voxels for run%d', length(find(Locb==0)), runIdx);
    elseif size(ts_ijk_coords, 1) == final_vox_num
        sel_ts_mtrx_all{1, runIdx} = ts_mtrx;
        sel_ijk_coords_all{1, runIdx} =ts_ijk_coords;
    end
end

tempcell = cat(3, sel_ts_mtrx_all{:});
ave_brain_pattern = mean(tempcell, 3);
end
