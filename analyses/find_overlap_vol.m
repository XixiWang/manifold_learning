function overlap_vol = find_overlap_vol(var1, var2, niftifile, writemode, filename)
% overlap_vol = find_overlap_vol(var1, var2[, niftifile, writemode, filename]):
% Find the overlap volume between two volumes
% 
% Input:
%   var1, var2:
%   Input volumes could be nifti (.nii) files or MATLAB variables, var1 and
%   var2 can be specified without extension
%
%   niftifile: whether the input file is .nii file, default is 0
% 
%   writemode: whether to save output volume, default is 'off'
% 
%   filename: output nifti file name, no extension is needed
% 

if nargin < 3
    niftifile = 0;
end

if nargin < 4
    writemode = 'off';
end 

% If reading into nifti files directly
if niftifile == 1
    disp('Analyzing nifti files'); 
    var1 = check_var_ext(var1, '.nii');
    var2 = check_var_ext(var2, '.nii');
    vol1_hdr = spm_vol(var1);
    vol1 = spm_read_vols(vol1_hdr);
    vol2_hdr = spm_vol(var2);
    vol2 = spm_read_vols(vol2_hdr);
elseif niftifile == 0
    vol1 = var1;
    vol2 = var2;
else
    error('Wrong niftifile value!');
end

if size(vol1) ~= size(vol2)
    error('Volume dimensions do not match!');
end
    
overlap_vol = vol1 & vol2;

if strcmp(writemode, 'on') && niftifile == 1
    overlap_vol_hdr = vol1_hdr;
    if nargin == 4
        overlap_vol_hdr.fname = 'overlap_vol.nii';
    else
        overlap_vol_hdr.fname = [filename '.nii'];
    end
    spm_write_vol(overlap_vol_hdr, overlap_vol);
end

if strcmp(writemode, 'on') && niftifile == 0
   if nargin == 4
       save('overlap_vol.mat', 'overlap_vol');
   else
       save([filename '.mat'], 'overlap_vol');
   end
end

end