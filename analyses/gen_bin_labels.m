function [ang_bin_labels, ang_bin_counts] = gen_bin_labels(num_bins, rot_angs)
% Generate labels for each TR according to different number of bins
% 
% Input:
%   num_bins: total number of bins
% 
%   rot_angs: rotation angles for each TR

% Generate bin edges
ang_min = 0;
ang_max = 360;

bin_edges = ang_min: ang_max/(num_bins - 1):ang_max;
[ang_bin_counts, ang_bin_labels] = histc(rot_angs, bin_edges); 

end