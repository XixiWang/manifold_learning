function drtoolbox_dir = add_drtoolbox_dir()
% Check platform version and add corresponding dimension reduction toolbox
% path

if ismac
    drtoolbox_dir = '~/Documents/Code/manifold_learning/toolboxes/drtoolbox';
    drtoolbox_dir = GetFullPath(drtoolbox_dir);
    addpath(genpath(drtoolbox_dir));
    fprintf('Add drtoolbox-mac path:%s\n', drtoolbox_dir);
elseif isunix
    drtoolbox_dir = '/raizadaData/Xixi_manifold_learning/toolboxes/drtoolbox';
    drtoolbox_dir = GetFullPath(drtoolbox_dir);
    addpath(genpath(drtoolbox_dir));
    fprintf('Add drtoolbox-linux path:%s\n', drtoolbox_dir);
end
end