% Batch read ROIs for all localizer runs and select overlapped voxels to
% generate the averaged ROI
%
% Output averaged ROI.nii file
% Problems: output voxel number is zero?!
% Solution1: check the number of voxels for each run separately
% 
% Update Dec 21, 2015: numver of voxels varies for each run

clear;
close all;
clc;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/Toolboxes/spm8/toolbox/marsbar/');
addpath('~/Documents/utilities/');
spm('Defaults', 'fmri');

% -----------------------------------------
type = 'smoothed';
roi_file_name = 'sel_overall_roi.nii';
% -----------------------------------------

nsub_dir = toolrotationBaseDir;

%% Batch process each localizer run
localizer_dir_listing = dir('LocalizerRun*')';
num_loc_runs = length(localizer_dir_listing) - 1;
all_rois = cell(1, num_loc_runs);
roi_vox_numers = zeros(1, num_loc_runs);

for runIdx = 1:num_loc_runs
    loc_run_name = localizer_dir_listing(runIdx).name;
    loc_dir = fullfile(nsub_dir, loc_run_name);
    
    if strcmp(type, 'smoothed')
        roi_dir = fullfile(loc_dir, 'glm_results_ind_loc', 'smoothed', 'rois');
    elseif strcmp(type, 'unsmoothed')
        roi_dir = fullfile(loc_dir, 'glm_results_ind_loc', 'unsmoothed', 'rois');
    end
    
    if ~exist(fullfile(roi_dir, roi_file_name), 'file')
        fprintf('%s does not exist for %s \n', roi_file_name, loc_run_name);
        all_rois{1, runIdx} = [];
        roi_vox_numers(1, runIdx) = 0;
        continue;
    end
    
    roi_hdr = spm_vol(fullfile(roi_dir, roi_file_name));
    roi_vol = spm_read_vols(roi_hdr);
    
    roi_vox_numers(1, runIdx) = length(find(roi_vol == 1));
    all_rois{1, runIdx} = roi_vol;
end

if ~exist(fullfile(nsub_dir, 'loc_func_rois.mat'), 'file')
    save(fullfile(nsub_dir, 'loc_func_rois.mat'), 'roi_vox_numers', 'all_rois');
end
disp(roi_vox_numers);

%% Find averaged functional ROIs across all localizer runs
[~, SubID] = fileparts(nsub_dir);
if strcmp(SubID, 'ToolRotation02')
    ave_roi = all_rois{1,1} & all_rois{1,3} & all_rois{1,4} ...
        & all_rois{1,5} & all_rois{1,6};
    roi_vox_numers(1, 7) = length(find(ave_roi == 1));
else
    ave_roi = all_rois{1,1} & all_rois{1,2} & all_rois{1,3} ...
        & all_rois{1,4} & all_rois{1,5} & all_rois{1,6};
    roi_vox_numers(1, 7) = length(find(ave_roi == 1));
end

