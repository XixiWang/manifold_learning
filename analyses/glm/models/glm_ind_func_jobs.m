function [jobs, spm_output_dir] = glm_ind_func_jobs(nsub_dir, func_dir, func_file)
% [jobs, spm_output_dir] = glm_ind_func_jobs(nsub_dir, func_dir, func_file):
% generates SUDO jobs structure for glm analysis in SPM. This is the glm job
% structure for each individual FUNCTIONAL run. PAY ATTENTION that those
% beta values should not be used for future analysis. The SPM.mat file is
% generated just for extract_filter_ts.m: only SPM.xY.VY (number of volumes),
% SPM.xGX.gSF (global scaling factor), SPM.xX.K (DCT filter matrix) and
% mask.hdr/img.
%
% Notice that for caos_toolrotation dataset, TR = 2.2 secs is hard coded!
%
% Input:
%   nsub_dir: subject directory (string)
%   
%   func_dir: functional scan file directory (string)
%
%   func_file: functional scan file name (string)
%
% Output:
%   jobs: job structure for spm-glm analysis
% 
%   spm_output_dir: spm-glm analysis output directory where the SPM.mat is
%   going to be saved

% Check glm_results folder
TR = 2.2; % in secs
dis_acqs = 2; % Discard first two volumes
V = spm_vol(func_file);
Y = spm_read_vols(V);
num_vols = size(Y, 4); % Totol number of localizer scans

% Modify spm output directory
if strcmp(func_file(1), 's')
    spm_output_dir = fullfile(func_dir, 'glm_func', 'smoothed');
elseif strcmp(func_file(1), 'w')
    spm_output_dir = fullfile(func_dir, 'glm_func', 'unsmoothed');
end

if exist(spm_output_dir, 'dir')
    display('glm results for individual functional run already exist!')
    jobs = [];
else
    mkdir(spm_output_dir);
    % Create jobs structrue for glm analysis
    jobs{1,1}.spm.stats{1}.fmri_spec.dir = cellstr(spm_output_dir);
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.units = 'secs';
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.RT = TR;
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.fmri_t = 16;
    jobs{1,1}.spm.stats{1}.fmri_spec.timing.fmri_t0 = 1;
    
    % Select scan files and remove first two volumes!!
    if strcmp(func_file(1), 's')
        func_sel_files = spm_select('ExtFPList', pwd, '^swra\w*.ep2d_bold', (1 + dis_acqs):num_vols);
    elseif strcmp(func_file(1), 'w')
        func_sel_files = spm_select('ExtFPList', pwd, '^wra\w*.ep2d_bold', (1 + dis_acqs):num_vols);
    end
    
    % Load timing file !!!!!!!!!!!!!
    load(fullfile(nsub_dir, 'timings_func_onsets.mat'));
    fprintf('Loading timing file: %s \n', fullfile(nsub_dir, 'timings_func_onsets.mat'));
    
    [nameList, uni_idx, ori_idx] = unique(RegNames, 'stable');
    onsets = cell(1, size(nameList, 1));
    durations = cell(1, size(nameList, 1));
    for nameIdx = 1:size(nameList, 1)
        onsets{nameIdx} = double(Onsets(ori_idx == nameIdx, :))'; % row vector
        durations{nameIdx} = double(Durations(uni_idx(nameIdx))); % row vector
    end
    names = nameList';
    save('multiple_sessions.mat', 'names', 'onsets', 'durations');
    
    % Create jobs structrue
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).scans = cellstr(func_sel_files);
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {});
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).multi = cellstr('multiple_sessions.mat');
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).multi_reg = {''};
    jobs{1,1}.spm.stats{1}.fmri_spec.sess(1).hpf = 180; % Change cut-off frequency (default is 128 s)
    
    % movefile
    movefile('multiple_sessions.mat', spm_output_dir);
    
    % Fill in the rest jobs fields
    jobs{1,1}.spm.stats{1}.fmri_spec.fact = struct('name', {}, 'levels', {});
    jobs{1,1}.spm.stats{1}.fmri_spec.bases.hrf = struct('derivs', [0 0]);
    jobs{1,1}.spm.stats{1}.fmri_spec.volt = 1;
    jobs{1,1}.spm.stats{1}.fmri_spec.global = 'None';
    jobs{1,1}.spm.stats{1}.fmri_spec.mask = {''};
    jobs{1,1}.spm.stats{1}.fmri_spec.cvi = 'none';  % jobs{1,1}.spm.stats{1}.fmri_spec.cvi = 'AR(1)';

end
end

