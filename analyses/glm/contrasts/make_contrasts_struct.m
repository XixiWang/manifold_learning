function contrasts = make_contrasts_struct(glm_model_dir, contrast_type)
% contrasts = make_contrasts_struct(glm_model_dir, contrast_type):
% Generate contrast structure for different conditions
% 
% Eight conditions:
% Animal Face Places Tools ScaAnimals ScaFace ScaPlaces ScaTools
% 
% contrast_type: 'Tools' for now!!!!! 


cd(glm_model_dir);
load('SPM.mat');

switch contrast_type
    case 'Tools'
        contrasts = struct('name', ...
            {'Tools > Animals', ...
             'Tools > Others', ...
             'Tools > Everything else'}, ...
             'vec', ...
            {[-1 0 0 1 0 0 0 0], ...
             [-0.25 -0.25 -0.25 1 0 0 0 -0.25], ...
             1/7 * [-1 -1 -1 7 -1 -1 -1 -1]});
end

end