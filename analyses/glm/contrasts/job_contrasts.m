function matlabbatch = job_contrasts(config, glm_model_dir)
% matlabbatch = job_contrasts(config, glm_model_dir): 
% Construct an SPM batch job structure for specifying contrasts. 
% 
%   Config: a structure that includes
%   .subject: subject ID
%   .contrasts: structure returned by make_contrasts_struct

spm_mat_loc = fullfile(glm_model_dir, 'SPM.mat');
matlabbatch{1}.spm.stats.con.spmmat = cellstr(spm_mat_loc);

consess = cell(size(config.contrasts));
for ctr = 1:length(consess)
    this_contrast = config.contrasts(ctr);
    matlabbatch{1}.spm.stats.con.consess{ctr}.tcon.name = this_contrast.name;
    matlabbatch{1}.spm.stats.con.consess{ctr}.tcon.convec = this_contrast.vec;
    matlabbatch{1}.spm.stats.con.consess{ctr}.tcon.sessrep = 'none';
end

matlabbatch{1}.spm.stats.con.delete = 1;
end