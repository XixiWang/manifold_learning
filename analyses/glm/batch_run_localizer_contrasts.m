% batch contrast
clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/contrasts/');

nsub_dir = toolrotationBaseDir;

% Select glm model to make contrast
% ------------------------------------------------------------------
glm_model = 'glm_results_ind_loc/smoothed';
% ------------------------------------------------------------------

% Batch process each localizer run
spm_jobman('initcfg');
clc;

localizer_dir_listing = dir('LocalizerRun*')';
for runIdx = 1:length(localizer_dir_listing)
% for runIdx = 1
    loc_run_name = localizer_dir_listing(runIdx).name;
    loc_dir = fullfile(nsub_dir, loc_run_name);
    cd(loc_dir);

    glm_model_dir = fullfile(loc_dir, glm_model);    
    if ~exist(glm_model_dir, 'dir');
        disp('This is not the TAFP localizer run!');
        break;
    end
    % Create contrast structure
    contrasts = make_contrasts_struct(glm_model_dir, 'Tools');
    
    % Create contrast job structure
    config.subject = loc_run_name; 
    config.contrasts = contrasts;
    
    batch_contrast = job_contrasts(config, glm_model_dir);
    save(fullfile(glm_model_dir, 'batch_contrasts.mat'), 'batch_contrast');
    
    % Back up original SPM.mat
    if ~exist(fullfile(glm_model_dir, 'SPM_backup.mat'), 'file')
        copyfile SPM.mat SPM_backup.mat
    end
    
    spm_jobman('serial', batch_contrast)
    
    % Load contrast and check the results
end