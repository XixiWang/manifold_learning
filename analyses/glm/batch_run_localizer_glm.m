% First level glm analysis for caos_toolrotation localizer runs
clear;
clc;
close all;

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/models/');

spm_jobman('initcfg');
clc;

nsub_dir = toolrotationBaseDir;

% Batch process each localizer run
localizer_dir_listing = dir('LocalizerRun*')';
for runIdx = 1:length(localizer_dir_listing)
% for runIdx = 1
    loc_run_name = localizer_dir_listing(runIdx).name;
    loc_dir = fullfile(nsub_dir, loc_run_name);
    cd(loc_dir);
    
    timing_file = dir('TAFP*.csv');
    if isempty(timing_file)
        disp('This is not the TAFP localizer run!');
        continue;
    end
    
    % For localizer scans: use smoothed dataset!
    % ------------------------------------------------------------------
    loc_file = dir('swraToolRotation*bold*.nii');
    % ------------------------------------------------------------------

    loc_file = loc_file.name;
    fprintf('Analyzing %s \n', loc_file);
    
    % Select glm model and generate glm job structure
    [jobs, spm_output_dir] = glm_ind_loc_jobs(loc_dir, loc_file);
    
    % Run glm analysis
    if ~isempty(jobs)
        cd(spm_output_dir);
        spm_jobman('run', jobs);
        load SPM
        spm_spm(SPM);
    else
        fprintf('GLM analysis not performed - check %s \n', spm_output_dir)
        continue;
    end
    
end
