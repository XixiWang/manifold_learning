% First level glm analysis for caos_toolrotation functional runs
clear;
clc;
close all;

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/Code/manifold_learning/Analyses');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/');
addpath('~/Documents/Code/manifold_learning/Analyses/glm/models/');

spm_jobman('initcfg');
clc;

nsub_dir = toolrotationBaseDir;

% Batch process each localizer run
Func_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(Func_dir_listing)
% for runIdx = 1
    func_run_name = Func_dir_listing(runIdx).name;
    func_dir = fullfile(nsub_dir, func_run_name);
    cd(func_dir);
    
    % For functctional scans: use unsmoothed dataset!
    % ------------------------------------------------------------------
    func_file = dir('wraToolRotation*bold*.nii');
    % ------------------------------------------------------------------

    func_file = func_file.name;
    fprintf('Analyzing %s \n', func_file);
    
    % Select glm model and generate glm job structure
    [jobs, spm_output_dir] = glm_ind_func_jobs(nsub_dir, func_dir, func_file);
    
    % Run glm analysis
    if ~isempty(jobs)
        cd(spm_output_dir);
        spm_jobman('run', jobs);
        load SPM
        spm_spm(SPM);
    else
        fprintf('GLM analysis not performed - check %s \n', spm_output_dir)
        continue;
    end
    
end
