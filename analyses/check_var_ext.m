function fullvar = check_var_ext(var1, default_ext)
% Check whether variable has extension (and add extension if not)

if nargin < 2
    error('Specify extension format first!');
end

[~, ~, ext] = fileparts(var1);
if isempty(ext)
    fullvar = strcat(var1, default_ext);
else
    fullvar = var1;
end

end