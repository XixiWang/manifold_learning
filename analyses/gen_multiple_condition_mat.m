% Script to generate multiple condition .mat file for each run
clear;
clc;
close all;

rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);
cd(datadir);

nsub_dir = uigetdir;
cd(nsub_dir);
fprintf('Converting multiple condition file for %s \n', nsub_dir);

localizer_dir_listing = dir('LocalizerRun*')';

TR = 2.2; % in secs

for runIdx = 1:length(localizer_dir_listing)
    loc_dir = fullfile(nsub_dir, localizer_dir_listing(runIdx).name);
    cd(loc_dir);
    fprintf('Analyzing %s \n', localizer_dir_listing(runIdx).name);
    
    % Read in .prt file and convert it to .mat file
    timing_file = dir('TAFP*.csv');
    if isempty(timing_file)
        disp('This is not the TAFP localizer run!');
        continue;
    end
    timing_file = importdata(timing_file.name);
    
    % Create timing file for glm anaylsis
    sti_tot_num = size(timing_file.data, 1);
    % Column 1: Run
    Runs = ones(sti_tot_num, 1) * runIdx;
    % Column 2: Regressor Name
    RegNames = timing_file.textdata;
    % Column 3: Onset Time (in secs, relative to start of each run)
    Onsets = (timing_file.data(:, 1)/1000);
    % Column 4: Duration (in secs)
    Durations = (timing_file.data(:, 2)/1000 - Onsets);
    
    timing_matfile = 'timings_onsets.mat';
    if ~exist(fullfile(loc_dir, 'timings'), 'dir');
        mkdir('timings');
    end
    save(fullfile(loc_dir, 'timings', timing_matfile), 'Runs', 'RegNames', 'Onsets', 'Durations');
    
end

%     % Create timing file for spm-glm analysis
%     names = RegNames';
%     names = names(1, [1:2:7, 9:end]);
%
%     onsets = cell(1, 8);
%     onsets{1} = [Onsets(1) Onsets(2)];
%     onsets{2} = [Onsets(3) Onsets(4)];
%     onsets{3} = [Onsets(5) Onsets(6)];
%     onsets{4} = [Onsets(7) Onsets(8)];
%     onsets{5} = Onsets(9);
%     onsets{6} = Onsets(10);
%     onsets{7} = Onsets(11);
%     onsets{8} = Onsets(12);
%
%     durations = num2cell(Durations');
%     durations = durations(1, [1:2:7, 9:end]);
%
%     % save(fullfile(pwd, 'timings', 'spm_timings_onsets.mat'), 'names', 'onsets', 'durations');
