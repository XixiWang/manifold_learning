% Extract time series and perform detrending
% Update on Dec 16, 2015: Remove fixation period

clear;
clc;
close all;

nsub_dir = toolrotationBaseDir;
spm_jobman('initcfg');
clc;

% Batch process each functional run
bold_dir_listing = dir('ToolRotationRun*')';
type = 'unsmoothed';
fixation = 2; % in number of TRs

for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % ---------------------------------------------------------------
    filename = [type '_' bold_run_name '_timeseries_nofixation.mat'];
    % ---------------------------------------------------------------
    if exist(fullfile(bold_dir, filename), 'file')
        fprintf('Processed time series already exists for %s \n', bold_run_name);
        continue;
    else
        % Specify spm output directory
        % spm_output_dir = fullfile(bold_dir, 'glm_results_ind_loc', 'unsmoothed');
        spm_output_dir = fullfile(bold_dir, 'glm_func', type);        
        % Get detrended time series for the whole brain and extract
        % corresponding coordinates
        [zero_meaned_ts, ijk_coords, mni_coords] = extract_filter_ts(bold_dir, spm_output_dir, type, fixation);
        % [zero_meaned_ts_withfix, ijk_coords1, mni_coords1] = extract_filter_ts(bold_dir, spm_output_dir, type);

        % Pay attention to the offset: MNI coords start from 0 & MATLAB index start
        % from 1!
        mni_coords = mni_coords - 1;
        
        save(fullfile(bold_dir, filename), 'zero_meaned_ts', 'ijk_coords', 'mni_coords');
    end
end
cd(nsub_dir);

% To check processed time series
% compare_timeseries(ts_file, ind_mni_coords)
