% Script to compute (and to display) fourier spectrum for time series of
% the whole brain
% 
% Updated on Dec 16, 2015: Use fixation period removed time series

clear;
clc;
close all;

nsub_dir = toolrotationBaseDir;
bold_dir_listing = dir('ToolRotationRun*');
type = 'unsmoothed';

TR = 2.2;
for runIdx = 1:length(bold_dir_listing)
% for runIdx = 4
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    FFT_filename = [type '_' bold_run_name '_FFT_nofixation.mat'];
    if exist(fullfile(bold_dir, FFT_filename), 'file')
        fprintf('FFT file for %s already exists! \n', bold_run_name);
        continue;
    else
        fprintf('Compute FFT for %s \n', bold_run_name);
        % Load time series mat file
        filename = [type '_' bold_run_name '_timeseries_nofixation.mat'];
        load(fullfile(bold_dir, filename));
        
        tot_voxel = size(zero_meaned_ts, 2);
        tot_volume = size(zero_meaned_ts, 1);
        all_voxel_fft = cell(1, tot_voxel);
        all_voxel_pk_freqs = zeros(tot_voxel, 5);
        all_voxel_pk_vals = zeros(tot_voxel, 5);
        
        % Generate Fourier power spectrum
        % Loop through every voxel
        for ind_vox_idx = 1:tot_voxel
            temp_ts = zero_meaned_ts(:, ind_vox_idx);
            % Compute FFT
            % Turn figure window off
            % [Y_ts, pk_vals, pk_freqs] = compute_ts_fft(temp_ts, TR, 'off');
            [Y_ts, pk_vals, pk_freqs] = compute_ts_fft(temp_ts, TR, 'on');

            all_voxel_fft{1, ind_vox_idx} = Y_ts;
            all_voxel_pk_freqs(ind_vox_idx, :) = pk_freqs;
            all_voxel_pk_vals(ind_vox_idx, :) = pk_vals;
        end
        
        % Save FFT results and peak frequency values for each voxel
        voxel_idx = (1:tot_voxel)';
        T = table(voxel_idx, all_voxel_pk_freqs, mni_coords, ijk_coords);
        
        save(fullfile(bold_dir, FFT_filename), 'all_voxel_fft', 'all_voxel_pk_vals', 'T');
    end
    
end
cd(nsub_dir);