function [sel_T, idx_cell, idx_all] = select_fourier_ROI(all_voxel_tables, flow_cutoff, fhigh_cutoff, num_peaks)
% sel_T = select_fourier_ROI(all_voxel_tables[, flow_cutoff, fhigh_cutoff]):
% Select Fourier spectrum based functional ROI
% 
% Input:
%   flow_cutoff: low-cut off frequency, default is 0.0151 Hz
% 
%   fhigh_cutoff: high cut-off freqency, default is 0.0186 Hz
% 
%   num_peaks: number of peaks that function checks
% 
% Update 1/7/2016:
%   - Modify the number of selected top frequency peaks
%   

% Default low cut-off frequency
if nargin < 2
    flow_cutoff = 0.0151;
end

% Default high cut-off frequency
if nargin < 3
    fhigh_cutoff = 0.0186;
end

% Default number of peaks
if nargin < 4
    num_peaks = 1;
end

top_pk_freq = all_voxel_tables.all_voxel_pk_freqs(:, 1: num_peaks);

idx_cell = cell(1, num_peaks);
idx_all = []; 
for ctr = 1: num_peaks
    idx_cell{1, ctr} = find(top_pk_freq(:, ctr) < fhigh_cutoff & top_pk_freq(:, ctr) > flow_cutoff);
    idx_all = [idx_all; idx_cell{1, ctr}];
end

idx_all = sort(idx_all, 'ascend');
sel_T = all_voxel_tables(idx_all, :);

end

% cut-off frequency calculation:
% % Rotation period in secs
% T_rot = 60; 
% f_rot = 1/T_rot;
% 
% % Generate frequency vector
% TR = 2.2;
% Fs = 1/TR;
% NFFT = 512;
% f_vec = Fs/2 * linspace(0, 1, NFFT/2 + 1);
% 
% % Select frequency range based on f_rot and f_vec
% % --------------------------------------------
% flow_cutoff = f_vec(18);
% fhigh_cutoff = f_vec(22);
% % --------------------------------------------