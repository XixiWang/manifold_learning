function [roi_ijk_coords, roi_idx] = get_roi_coords(roi_vol)
% Return masked roi coordinates [i, j, k] and global index

% Convert variable to logical type
roi_vol = logical(roi_vol);
roi_idx = find(roi_vol == 1);
[roii, roij, roik] = ind2sub(size(roi_vol), roi_idx);
roi_ijk_coords = [roii roij roik];

end