
% Script to batch convert caos_toolrotation raw data to nifti files
clear;
close all;
clc;

setenv('FSLDIR','/Applications/fsl');
setenv('FSLOUTPUTTYPE', 'NIFTI_GZ');

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/utilities/');
% addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/spm8/');

datadir = '~/Documents/Data/caos_toolrotation';
rawdatadir = fullfile(datadir, 'Data_Raw');
niftidatadir = fullfile(datadir, 'Data_ToolRotation');
addpath(datadir);

datainfofile = 'caos_toolrotation_info.xlsx';
T = readtable(datainfofile);

ToolRotationID = T.ToolRotationID;
SubjectID = T.SubjectID;
Date = T.Date;
sub_num = length(ToolRotationID);

%% Converts functional images
for nsub = 1:sub_num
    ind_rotationID = ToolRotationID{nsub, 1};
    ind_date = Date(nsub, 1);
    
    nsub_dir = fullfile(rawdatadir, ind_rotationID, num2str(ind_date));
    cd(nsub_dir);
    
    folders = dir('*ep2d_bold_2200TR*');
    for ctr = 1:length(folders)
        cd(fullfile(nsub_dir, folders(ctr).name));
        NIFTI_FILE_NAME = [ind_rotationID '_' num2str(ind_date) '_' folders(ctr).name '.nii'];
        if exist(fullfile(niftidatadir, ind_rotationID, NIFTI_FILE_NAME),'file')
            disp('NIFTI file already exists!');
        else
            disp(['Converting raw data to NIFTI for ' ind_rotationID '_' num2str(ind_date)]);
            disp(['Converting to NIFTI files: ' folders(ctr).name]);
            
            dicom_files = spm_select('list', pwd, 'ToolRotation*');
            dicom_hdr = spm_dicom_headers(dicom_files);
            spm_dicom_convert(dicom_hdr);
            
            setenv('NIFTI_NAME', [ind_rotationID '_' num2str(ind_date) '_' folders(ctr).name]);
            !/Applications/fsl/bin/fslmerge -t $NIFTI_NAME f*.hdr
            zipfile = dir('*.nii.gz');
            setenv('NIFTI_NAME', zipfile.name);
            !gunzip $NIFTI_NAME
            
            niifile = dir('*.nii');
            movefile(niifile.name, fullfile(niftidatadir, ind_rotationID))
        end
    end
    disp([ind_rotationID ' is done!']);
    fprintf('\n');
end

%% Converts T1 images
clc;
rawT1dir = fullfile(rawdatadir, 'Anatomical');
cd(rawT1dir);
folders = dir('*ToolRotation*');

for nsub = 1:sub_num
    ind_T1_ID = folders(nsub).name;
    cd(fullfile(rawT1dir, ind_T1_ID));
    
    ind_rotationID = ToolRotationID{nsub, 1};
    ind_date = Date(nsub, 1);
    T1_NIFTI_NAME = [ind_rotationID '_' num2str(ind_date) '_T1.nii'];
    if exist(fullfile(niftidatadir, ind_rotationID, T1_NIFTI_NAME),'file')
        disp('T1 file already exists');
    else
        disp(['Converting T1 images to NIFTI for ' ind_rotationID '_' num2str(ind_date)]);
        
        dicom_files = spm_select('list', pwd, 'ToolSparse*');
        fprintf('%d DICOM images in total\n', length(dicom_files));
        dicom_hdr = spm_dicom_headers(dicom_files);
        spm_dicom_convert(dicom_hdr);
        
        setenv('NIFTI_NAME', [ind_rotationID '_' num2str(ind_date) '_T1']);
        !/Applications/fsl/bin/fslmerge -t $NIFTI_NAME s*.hdr
        zipfile = dir('*.nii.gz');
        setenv('NIFTI_NAME', zipfile.name);
        !gunzip $NIFTI_NAME
        
        niifile = dir('*.nii');
        movefile(niifile.name, fullfile(niftidatadir, ind_rotationID))
    end
    
end