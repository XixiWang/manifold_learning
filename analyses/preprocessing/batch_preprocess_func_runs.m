% Wrapper function to batch preprocess ToolRotation dataset
clear;
clc;

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/utilities/');
rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);

load('a2_spm_batch_preprocess_all_sessions.mat');
ind_matlabbatch = matlabbatch;

spm_jobman('initcfg');
cd(datadir);

% Select data director
nsub_dir = uigetdir;
cd(nsub_dir);
fprintf('Preprocessing %s \n', nsub_dir);

bold_dir_listing = dir('ToolRotationRun*');

% Call spm batch script function
run1_files = spm_select('ExtFPListRec', bold_dir_listing(1).name, '^T\w*.ep2d_bold', 1:178);
run2_files = spm_select('ExtFPListRec', bold_dir_listing(2).name, '^T\w*.ep2d_bold', 1:178);
run3_files = spm_select('ExtFPListRec', bold_dir_listing(3).name, '^T\w*.ep2d_bold', 1:178);
run4_files = spm_select('ExtFPListRec', bold_dir_listing(4).name, '^T\w*.ep2d_bold', 1:178);

% Slice timing correction
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,1} = run1_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,2} = run2_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,3} = run3_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,4} = run4_files;

% Coregistration
anat_files = spm_select('ExtFPlist', nsub_dir, '^ToolRotation\w*.anat.nii');
ind_matlabbatch{1,3}.spm.spatial.coreg.estwrite.source{1,1} = anat_files;
fprintf('Anatomical file: %s \n', anat_files);

save(fullfile(nsub_dir, 'ind_spm_preprocess_all_sessions.mat'), 'ind_matlabbatch')

% Batch preprocessing
spm_jobman('run', ind_matlabbatch);

% Check alignment and corregistration
% Func and anat
% Func and T1 template



