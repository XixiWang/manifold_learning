% Wrapper function to batch preprocess ToolRotation dataset
clear;
clc;

addpath('~/Documents/Toolboxes/spm8/');
addpath('~/Documents/utilities/');
rootdir = '~/Documents/Data/caos_toolrotation/';
datadir = fullfile(rootdir, 'Data_ToolRotation');
addpath(datadir);

load('batch_preprocess_all_runs.mat');
ind_matlabbatch = matlabbatch;

spm_jobman('initcfg');
cd(datadir);

% Select data director
nsub_dir = uigetdir;
cd(nsub_dir);
fprintf('Preprocessing %s \n', nsub_dir);

% Make sure the anatomical image is correct
fprintf('Please check *anat.nii file! \n');

bold_dir_listing = dir('ToolRotationRun*');
localizer_dir_listing = dir('LocalizerRun*')';

% Call spm batch script function
% Get functional runs
run1_files = spm_select('ExtFPListRec', bold_dir_listing(1).name, '^T\w*.ep2d_bold', 1:178);
run2_files = spm_select('ExtFPListRec', bold_dir_listing(2).name, '^T\w*.ep2d_bold', 1:178);
run3_files = spm_select('ExtFPListRec', bold_dir_listing(3).name, '^T\w*.ep2d_bold', 1:178);
run4_files = spm_select('ExtFPListRec', bold_dir_listing(4).name, '^T\w*.ep2d_bold', 1:178);

% Get localizer runs
loc1_files = spm_select('ExtFPListRec', localizer_dir_listing(1).name, '^T\w*.ep2d_bold', 1:84);
loc2_files = spm_select('ExtFPListRec', localizer_dir_listing(2).name, '^T\w*.ep2d_bold', 1:84);
loc3_files = spm_select('ExtFPListRec', localizer_dir_listing(3).name, '^T\w*.ep2d_bold', 1:84);
loc4_files = spm_select('ExtFPListRec', localizer_dir_listing(4).name, '^T\w*.ep2d_bold', 1:84);
loc5_files = spm_select('ExtFPListRec', localizer_dir_listing(5).name, '^T\w*.ep2d_bold', 1:84);
loc6_files = spm_select('ExtFPListRec', localizer_dir_listing(6).name, '^T\w*.ep2d_bold', 1:84);
loc7_files = spm_select('ExtFPListRec', localizer_dir_listing(7).name, '^T\w*.ep2d_bold', 1:133);

% Slice timing correction
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,1} = run1_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,2} = run2_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,3} = run3_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,4} = run4_files;

ind_matlabbatch{1,1}.spm.temporal.st.scans{1,5} = loc1_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,6} = loc2_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,7} = loc3_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,8} = loc4_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,9} = loc5_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,10} = loc6_files;
ind_matlabbatch{1,1}.spm.temporal.st.scans{1,11} = loc7_files;

% Coregistration
anat_files = spm_select('ExtFPlist', nsub_dir, '^ToolRotation\w*.anat.nii');
ind_matlabbatch{1,3}.spm.spatial.coreg.estwrite.source{1,1} = anat_files;
fprintf('Anatomical file: %s \n', anat_files);

save(fullfile(nsub_dir, 'ind_spm_preprocess_all_sessions.mat'), 'ind_matlabbatch')

% Batch preprocessing
spm_jobman('run', ind_matlabbatch);

% Check alignment and corregistration
% Func and anat
% Func and T1 template



