function [all_labels, all_colors] = gen_ts_labels(all_ijk_coords, target_ijk_coords)
% Generate labels for target time series
% Binary
% To be updated

[~, target_idx] = ismember(target_ijk_coords, all_ijk_coords, 'rows');
all_labels = zeros(size(all_ijk_coords, 1), 1);
all_labels(target_idx) = 1;

all_labels = all_labels + 1;
% colorlist = {'r', 'g', 'b', 'y', 'm', 'c'};
colorlist = [1 0 0;
             0 1 0;
             0 0 1;
             1 1 0;
             1 0 1;
             0 1 1];
all_colors = zeros(length(all_labels), 3);
for ctr = 1:length(all_colors)
    all_colors(ctr, :) = colorlist(all_labels(ctr), :);
end

end