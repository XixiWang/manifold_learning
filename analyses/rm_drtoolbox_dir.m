function rm_drtoolbox_dir()
% Check platform version and add corresponding dimension reduction toolbox
% path

if ismac
    drtoolbox_dir = '~/Documents/Code/manifold_learning/toolboxes/drtoolbox';
    drtoolbox_dir = GetFullPath(drtoolbox_dir);
    rmpath(genpath(drtoolbox_dir));
    fprintf('Remove drtoolbox-mac path:%s\n', drtoolbox_dir);
elseif isunix
    drtoolbox_dir = '/raizadaData/Xixi_manifold_learning/toolboxes/drtoolbox';
    drtoolbox_dir = GetFullPath(drtoolbox_dir);
    rmpath(genpath(drtoolbox_dir));
    fprintf('Remove drtoolbox-linux path:%s\n', drtoolbox_dir);
end

end