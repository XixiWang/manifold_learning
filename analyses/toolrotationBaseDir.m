function [nsub_dir, subID, root_code_dir, root_data_dir, spm_dir] = toolrotationBaseDir()
% [nsub_dir, subID, root_code_dir, root_data_dir, spm_dir] = toolrotationBaseDir:
% run function to initialize tool rotation data analysis

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify script folder and data folder
if ismac
    script_dir = '~/Documents/Code/manifold_learning';
    root_data_dir = '~/Documents/Data/caos_toolrotation/Data_ToolRotation';
elseif isunix
    script_dir = '/raizadaData/Xixi_manifold_learning';
    root_data_dir = '/raizadaData/Tool_Rotation/Data_ToolRotation';
end
% If saving to any other locations:
if ~exist(script_dir, 'dir')
    warning('Script folder .../manifold_learning/ directory does not exist!!')
    warning('Please specify the script folder directory!!')
    script_dir = uigetdir;
end
if ~exist(root_data_dir, 'dir')
    warning('Data directory .../Tool_Rotation/Data_ToolRotation/ does not exist!!')
    warning('Please specify data directory!!')
    root_data_dir = uigetdir;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add utilities dir
utilities_dir = fullfile(script_dir, 'utilities');
addpath(genpath(utilities_dir));
fprintf('Adding utilities path: %s\n', GetFullPath(utilities_dir));
script_dir = GetFullPath(script_dir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add base data directory and code directory
root_code_dir = fullfile(script_dir, 'analyses');
root_data_dir = GetFullPath(root_data_dir);
addpath(genpath(root_code_dir));
addpath(root_data_dir);
fprintf('Adding analyses code path: %s\n', root_code_dir);
fprintf('Adding processed data path: %s\n', root_data_dir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add spm dir
toolbox_dir = fullfile(script_dir, 'toolboxes');
if verLessThan('matlab', '8.3');
    spm_dir = fullfile(toolbox_dir, 'spm8');
    fprintf('MATLAB version %s: using spm8 \n', version);
else
    spm_dir = fullfile(toolbox_dir, 'spm12');
    fprintf('MATLAB version %s: using spm12 \n', version);
end
addpath(spm_dir);
fprintf('Adding SPM path: %s\n\n', spm_dir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add specific subject dir
fprintf('***********************************************************************\n');
nsub_dir = uigetdir(root_data_dir);
cd(nsub_dir);
fprintf('Analyzing %s \n\n', nsub_dir);

[~, subID] = fileparts(nsub_dir);
end


% if ismac
%     if verLessThan('matlab', '8.3');
%         spm_dir = '~/Documents/Toolboxes/spm8/';
%         spm_dir = fullfile(root_code_dir, )
%         fprintf('MATLAB version %s: using spm8 \n', version);
%     else
%         spm_dir = '~/Documents/Toolboxes/spm12';
%         fprintf('MATLAB version %s: using spm12 \n', version);
%     end
%     % if using other macs
%     if ~exist(spm_dir, 'dir')
%         warning('SPM directory does not exist');
%         spm_dir = uigetdir;
%     end
% elseif isunix
%     spm_dir = '/usr/local/spm8';
% end
% addpath(spm_dir);
% fprintf('Adding SPM path: %s\n', spm_dir);