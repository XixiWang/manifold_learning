function [rot_ang_labels, rot_labels_color] = gen_rot_ang_labels(ini_time, ini_TR, TR, num_rot, rot_period, ang_interval)
% [rot_ang_labels, rot_labels_color] = gen_rot_ang_labels(ini_time, ini_TR, TR, num_rot, rot_period[, ang_interval]):
% generate labels for each brain pattern according to stimuli's rotation angles
%
% Input:
%   ini_time: first TR onset time (absolute value) in secs
%
%   ini_TR: first TR index (absolute value)
%
%   TR: TR value in secs
%
%   num_rot: number of rotations
%
%   rot_period: rotation period in secs
%
%   ang_interval: optional, angle interval for label generation in degrees.
%   Default is 0, so that all data points will have same color label.
%
% Output:
%   rot_ang_labels:
%       1) absolute TR index
%       2) absolute TR time (secs)
%       3) rotation angles for each TR (degrees)
%       4) labels
%
%   rot_labels_color: RGB values for each label
%
% ATTENTION:
%   Hard code initial angle as 0 degree, to be modified!
%
% Update 1/6/2016:
% - keep first 4 TRs after rotation (hard coded: TR_fix_offset)
% Update 1/15/2016: 
% - assign 360 degrees to fixation TRs after rotations

if nargin == 5
    ang_interval = 0;
end


%%%%%%%%%%%% Hard coded! %%%%%%%%%%%%%%%%%%%
ini_ang = 0;
TR_fix_offset = 4;
%%%%%%%%%%%% Hard coded! %%%%%%%%%%%%%%%%%%%
rot_TR_timing = ini_time: TR: ini_time + num_rot * rot_period;
rot_TR_idx = ini_TR: 1: ini_TR + length(rot_TR_timing) - 1;

ang_per_sec = 360 / rot_period;
ang_per_TR = ang_per_sec * TR;
TR_num = length(rot_TR_idx);
rot_TR_ang = ini_ang: ang_per_TR : (TR_num-1) * ang_per_TR;
rot_TR_ang_mod = mod(rot_TR_ang, 360);

% Add 4 fixtation period offset
rot_TR_idx = [rot_TR_idx rot_TR_idx(end) + 1: 1: rot_TR_idx(end) + TR_fix_offset];
rot_TR_timing = [rot_TR_timing rot_TR_timing(end) + TR: TR: rot_TR_timing(end) + TR_fix_offset * TR];
rot_TR_ang_mod = [rot_TR_ang_mod 360 * ones(1, TR_fix_offset)];

% Generate color labels
if ang_interval == 0
    rot_labels = ones(size(rot_TR_idx));
else
    label_angs = 0:ang_interval:360;
    label_num = length(label_angs) - 1;
    range_val = zeros(label_num, 2);
    for ctr = 1:label_num
        range_val(ctr, 1) = label_angs(ctr);
        range_val(ctr, 2) = label_angs(ctr + 1);
    end
    
    rot_labels = zeros(size(rot_TR_idx));
    for ctr = 1:label_num
        rot_labels(rot_TR_ang_mod >= range_val(ctr, 1) & rot_TR_ang_mod <= range_val(ctr, 2)) = ctr;
    end
    % rot_labels = [rot_labels rot_labels(end) * ones(1, TR_fix_offset)];
end

% Combine all variables
rot_ang_labels = [rot_TR_idx; rot_TR_timing; rot_TR_ang_mod; rot_labels];

% colorlist = {'r', 'g', 'b', 'y', 'm', 'c'};
colorlist = [1 0 0;
    0 1 0;
    0 0 1;
    1 1 0;
    1 0 1;
    0 1 1];
rot_labels_color = zeros(length(rot_labels), 3);
for ctr = 1:length(rot_labels_color)
    rot_labels_color(ctr, :) = colorlist(rot_labels(ctr), :);
end

end