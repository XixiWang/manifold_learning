function gen_scatter_text(icoords, jcoords, text_strs, cmap, dot_connection, color_vals,  text_offset)
% gen_scatter_text(icoords, jcoords[, text_strs, cmap, dot_connection, color_vals, text_offset]):
% generate scatter plot and label each data point
%
% Input:
%   icoords, jcoords
%
%   text_strs
%
%   cmap: colormap type, default is 'hot'
% 
%   dot_connection: default is 0
%
%   color_vals: default is 1:N
%
%   text_offset: default is 0.01
%
%  Update 1/19/2016:
%  - Change colormap type

% -------------------------------------------
default_sca_sz = 100;
default_font_sz = 10;
% -------------------------------------------

if nargin < 2
    error('Not enough input arguments!');
end

if nargin < 3
    scatter(icoords, jcoords, default_sca_sz);
    return;
end

if nargin < 4
    cmap = 'hot';
end

if nargin < 5
    dot_connection = 0;
end

if nargin < 6
    color_vals = 1:length(icoords);
end

if nargin < 7
    text_offset = 0.015;
end

colormap(cmap);
scatter(icoords, jcoords, default_sca_sz, color_vals, 'fill');
hold on;
text(icoords + text_offset, jcoords + text_offset + 0.01, text_strs, 'FontSize', default_font_sz);
if dot_connection == 1
    plot(icoords, jcoords, '-x');
end

end