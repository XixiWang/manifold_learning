function new_name = gen_ind_run_roi_name(name_with_star, new_char)
% Replace * in ROI name with run index 

new_name = strrep(name_with_star, '*', num2str(new_char));
end