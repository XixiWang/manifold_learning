function [ind_ts, padded_ts] = compare_timeseries(ts_file, ind_mni_coords)
% [ind_ts, padded_ts] = compare_timeseries(ts_file, ind_mni_coords):
% compare the detrened and high pass filtered time sereis with original
% time series
% 
% Input:
%   ts_file: time series mat file name (string), need to specify file path!
% 
%   ind_mni_coords: mni coordniates that you want to plot
% 
% Output:
%   ind_ts: zero-meaned and high-pass filtered individual time series
% 
%   padded_ts: zero-padded time series, for better visualization  
% 
% Update 12/23/2015:
% - Change input parameters so that this function can be used to check the
%   roi based time series

load(ts_file)
ts_file_mat_obj = matfile(ts_file);
ts_file_struct = whos(ts_file_mat_obj);

for ctr = 1:size(ts_file_struct, 1)
    temp_name = ts_file_struct(ctr).name;
    if strfind(temp_name, 'mni')
        all_mni_coords = eval(temp_name);
    elseif strfind(temp_name, 'zero_meaned_ts')
        all_zero_meaned_ts = eval(temp_name);
    end
end

% Read original time series
[Lia, idx] = ismember(ind_mni_coords, all_mni_coords, 'rows');
if Lia == 0
    error('Can not find this voxel, try another!');
end
% ind_xyz_coords = xyz_coords(idx, :);

% Plot time series
figure(1);
ind_ts = all_zero_meaned_ts(:, idx);
% Zero pad time series for better visualization
padded_ts = [zeros(178 - length(ind_ts), 1); ind_ts];
plot(padded_ts);
title(['Zero meaned and high pass filtered time series:' num2str(ind_mni_coords)]);
% grid on;
axh = gca;
set(axh, 'YGrid', 'on');

end