function combine_rois(roi1_dir, roi1_name, roi2_dir, roi2_name, output_roi_name, output_dir)
% combine_rois(mode, roi1_dir, roi1_name, roi2_dir, roi2_name, output_roi_name, output_dir):
% function to combine two rois and write out nii file
%
% Input:
%
%   roi1_dir, roi2_dir: path to the roi volumes, i.e. ~/Data/ROI_dir/
%
%   roi1_name, roi2_name: roi names, can be specified without extensions
%
%   output_roi_name: output roi name, can be specified without extensions,
%   default is 'roi1_name_roi2_name.nii'
%
%   output_dir: default is roi1_dir
%
% Update 1/19/2016: 
% - modify output roi name and directory
% - add check_var_ext function to check variable extension
% 
% Update 1/21/2016:
% - add GetFullPath function to return full path to the ROI directories

if nargin < 5
    roi1_name = check_var_ext(roi1_name, '.nii');
    roi2_name = check_var_ext(roi2_name, '.nii');
    output_roi_name = sprintf('%s_%s.nii', roi1_name(1:end - 4), roi2_name(1:end - 4));
end

if nargin < 6
    output_dir = roi1_dir;
end

% Get full path
roi1_dir = GetFullPath(roi1_dir);
roi2_dir = GetFullPath(roi2_dir);
output_dir = GetFullPath(output_dir);

fprintf('Loading ROI nifti files... \n\n');
% check extension
roi1_name = check_var_ext(roi1_name, '.nii');
roi2_name = check_var_ext(roi2_name, '.nii');
output_roi_name = check_var_ext(output_roi_name, '.nii');

% add_spm_dir;
roi_hdr1 = spm_vol(fullfile(roi1_dir, roi1_name));
roi_vol1 = spm_read_vols(roi_hdr1);

roi_hdr2 = spm_vol(fullfile(roi2_dir, roi2_name));
roi_vol2 = spm_read_vols(roi_hdr2);

if ~isequal(size(roi_vol1), size(roi_vol2))
    error('ROI dimensions do not match!');
end

output_roi_vol = roi_vol1 + roi_vol2;
output_roi_hdr = roi_hdr1;
output_roi_hdr.fname = fullfile(output_dir, output_roi_name);

if exist(fullfile(output_dir, output_roi_name), 'file')
    [~, disp_dir] = fileparts(output_dir);
    fprintf('%s/%s already exists! \n\n', disp_dir, output_roi_name);
    return;
else
    spm_write_vol(output_roi_hdr, output_roi_vol);
    [~, disp_dir] = fileparts(output_dir);
    fprintf('Saving combined ROI - %s/%s\n\n', disp_dir, output_roi_name);
end
end