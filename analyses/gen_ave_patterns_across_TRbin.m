function [ave_pattern_mtrx, bin_TR_idx] = gen_ave_patterns_across_TRbin(ts_mtrx, ang_bin_labels)
% ave_pattern_mtrx = gen_ave_patterns(ts_mtrx, ang_bin_labels):
% function to calculate the averaged brain pattern according to bin labels
% for each TR
%  
% Input:
%   ts_mtrx: time series matrix, #TR-by-#voxel dimensional
% 
%   ang_bin_labels: 1-by-#TR vector, each TR is labeled with a specific bin
%   number
% 
% Output:
%   ave_pattern_mtrx: averaged brain pattern matrix, #bins-by-#voxel
%   dimensional
% 
%   bin_TR_idx: a #bin-by-1 cell matrix, each cell contains corresponding 
%   TR index for each bin

if ~isequal(size(ts_mtrx, 1), size(ang_bin_labels, 2))
    warning('Remove TR after rotation')
    rm_nTRs = size(ts_mtrx, 1) - size(ang_bin_labels, 2);
    fprintf('Remove %d TRs \n\n', rm_nTRs);
    ts_mtrx = ts_mtrx(1:size(ang_bin_labels,2), :);
end

fprintf('Time series matrix has %d TRs, %d voxels \n\n', size(ts_mtrx, 1), size(ts_mtrx, 2));

bin_TR_idx = cell(max(ang_bin_labels), 1);
ave_pattern_mtrx = zeros(max(ang_bin_labels), size(ts_mtrx, 2));
for ctr = 1:max(ang_bin_labels)   
    temp_pattern_mtrx = ts_mtrx(ang_bin_labels == ctr, :);
    temp_pattern_mtrx_ave = mean(temp_pattern_mtrx, 1);
    ave_pattern_mtrx(ctr, :) = temp_pattern_mtrx_ave;
    
    bin_TR_idx{ctr, 1} = find(ang_bin_labels == ctr);
end

end