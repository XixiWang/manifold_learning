% Script to run dimension reduction using selected Harvard-Oxford rois
%
% Refer to ToolRotationID/todo_rois_dim_red.txt for detailed ROI
% information!!! Remember to update .txt file if new ROIs have been
% generated!!!
%
% Output files are:
% .../Data_ToolRotation/Figures_dim_red_cmap_hot/ToolRotationID/roi_names/***.png
% .../ToolRotationID/dim_red_log***
%
% TODO: check FREE PARAMETERS


clear;
clc;
close all;

% Select subject
[nsub_dir, subID, ~, root_data_dir] = toolrotationBaseDir;
base_dir = fileparts(nsub_dir);
bold_dir_listing = dir('ToolRotationRun*')';

% -------------------------------------------------------------
mode = 'pattern';
dr_method = 'lle';
nnei = 3; % # of neighbor points
max_dim = 2; % # of max dimensionality
nTR_bins = 33; % # of total TR bins
cmap = 'hsv'; % colormap type: 'hsv', 'hot'...
% -------------------------------------------------------------
fig_dir_name = sprintf('Figures_dim_red_%s', cmap);
subset_TR_labels = num2str([1: nTR_bins]');
scrsz = get(groot, 'ScreenSize');

% Generate rotation angles
rot_ang_labels = gen_rot_ang_labels(16, 5, 2.2, 6, 60);
deg_per_TR = rot_ang_labels(3, :);
ang_bin_labels = gen_bin_labels(nTR_bins, deg_per_TR);

% Generate diary file
log_fname = sprintf('dim_red_log_%s_%s_nnei%d_dim%d_bins%d_%s', ...
    mode, dr_method, nnei, max_dim, nTR_bins, cmap);
diary on;
diary(log_fname);

% Loop through each roi
[bold_type_all, roi_names] = read_roi_txt_info(fullfile(root_data_dir, 'todo_rois_dim_red.txt'));
for roi_ctr = 1:length(bold_type_all)
    bold_type = bold_type_all{roi_ctr, 1};
    ts_roi_name = roi_names{roi_ctr, 1};
    fprintf('Analyzing ROI: %s\n\n', ts_roi_name);
    
    fig_dir = fullfile(base_dir, fig_dir_name, subID, ts_roi_name);
    if ~exist(fig_dir, 'dir')
        mkdir(fig_dir);
        fprintf('mkdir: %s\n\n', fig_dir);
    end
    
    % Batch process each functional run
    for runIdx = 1:length(bold_dir_listing)
        bold_run_name = bold_dir_listing(runIdx).name;
        bold_dir = fullfile(nsub_dir, bold_run_name);
        cd(bold_dir);
        
        % Replace * with runIdx and load corresponding time series matrix
        ind_ts_roi_name = gen_ind_run_roi_name(ts_roi_name, runIdx);
        ts_mtrx_dir = fullfile(bold_dir, 'roi_mtrx');
        ts_mtrx_name = [bold_type '_' bold_run_name '_' ind_ts_roi_name];
        
        if ~exist(fullfile(ts_mtrx_dir, check_var_ext(ts_mtrx_name, '.mat')), 'file')
            warning('%s/%s%s does not exist!!!', 'roi_mtrx', ts_mtrx_name, '.mat');
            continue;
        end
        
        fig_name = sprintf('pca_%s_run%d_nnei%d_mdim%d_nTR%d.png', dr_method, runIdx, nnei, max_dim, nTR_bins);
        if exist(fullfile(fig_dir, fig_name), 'file')
            fprintf('%s already exists!\n\n', fig_name);
            continue;
        end
        
        % Load time series matrix
        [ts_mtrx, ts_ijk_coords, ts_mni_coords] = load_ts_mtrx(ts_mtrx_dir, ts_mtrx_name);
        % Average brain patterns for each angle bin
        [ave_pattern_mtrx, bin_TR_idx] = gen_ave_patterns_across_TRbin(ts_mtrx, ang_bin_labels);
        
        tot_nvox = size(ave_pattern_mtrx, 2);
        h_fig = figure;
        % Customize figure size
        set(h_fig, 'Position', [1 scrsz(4)/2 scrsz(3)*0.5 scrsz(4)*0.5]);
        set(gcf, 'Color', [1 1 1]);
        
        % Dimension reduction
        % PCA
        pca_embed_mtrx = pca(ave_pattern_mtrx', 'NumComponents', max_dim);
        subplot(1, 2, 1);
        gen_scatter_text(pca_embed_mtrx(:, 1), pca_embed_mtrx(:,2), subset_TR_labels, cmap);
        subfig1 = sprintf('%d voxels: nnei%d mdim %d - pca: %s', tot_nvox, nnei, max_dim, bold_run_name);
        title(subfig1, 'FontSize', 10);
        axis square;
        
        if strcmp(dr_method, 'lle')
            % lle
            lle_embed_mtrx = run_lle_roweis(ave_pattern_mtrx, nnei, max_dim, mode);
            % Generate figures
            subplot(1, 2, 2);
            gen_scatter_text(lle_embed_mtrx(:, 1), lle_embed_mtrx(:, 2), subset_TR_labels, cmap);
            subfig2 = sprintf('lle: %s', bold_run_name);
            title(subfig2, 'FontSize', 10);
            axis square;
        elseif strcmp(dr_method, 'iso')
            % Isomap
            [embed_mtrx, residuals] = run_iso_tenen(ave_pattern_mtrx, nnei, max_dim, mode);
            iso_embed_mtrx = embed_mtrx.coords{2}';
            
            %%%%%%%%%%%%% Check the largest connected component %%%%%%%%%
            con_len = size(iso_embed_mtrx, 1);
            if con_len < nTR_bins
                warning('Largest connected component has %d points only!', con_len);
                warning('Skip %s', bold_run_name);
                continue
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Generate figures
            subplot(1, 2, 2);
            gen_scatter_text(iso_embed_mtrx(:, 1), iso_embed_mtrx(:, 2), subset_TR_labels, cmap);
            subfig2 = sprintf('isomap: %s', bold_run_name);
            title(subfig2, 'FontSize', 10);
            axis square;
        else
            error('Unknown dimension reduction approach!');
        end
        
        % Save figures
        export_fig(h_fig, fullfile(fig_dir, fig_name), '-m1.25');
        close(h_fig);
        fprintf('-------------------------------------------------------\n\n');
        
    end
    
    fprintf('ROI: %s is done!!\n\n', ts_roi_name);
    fprintf('***************************************************************\n\n');
    % clc;
end

cd(nsub_dir);
diary off;

% Update 1/15/2016:
% - add batch processing related functions
%
% Update 1/20/2016:
% - change output figure directory
% - return warning messages if roi_ts.mat file doesn't exist
% - add dr_method variable to select different dimension reduction method
% - add dim_red_log diary file
%
% Update 1/21/2016:
% - check the largest connected component length when performing isomap and
% return warning messages if length of connected component < number of TR
% bins