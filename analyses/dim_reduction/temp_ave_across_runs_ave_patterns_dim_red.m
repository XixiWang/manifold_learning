% Dimension reduction wrapper script
%
% Calculate the averaged brain patterns
%
% Update 1/18/2016: add batch processing related functions
% add function to run isomap toolbox
% 
% NEEDS TO BE MODIFIED!!!!!!

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses/');
addpath('~/Documents/Code/manifold_learning/analyses/dim_reduction/');
addpath('~/Documents/utilities/export_fig/');

% ToolRotation09
nsub_dir = toolrotationBaseDir;
bold_dir_listing = dir('ToolRotationRun*')';

% -------------------------------------------------------------
mode = 'pattern';
nnei = 3; % nnei = 10;
max_dim = 2; % max_dim = 10;
nTR_bins = 33; % # of data points that are going to be plotted
% -------------------------------------------------------------
subset_TR_labels = num2str([1: nTR_bins]');
scrsz = get(groot, 'ScreenSize');

% Generate rotation angles
rot_ang_labels = gen_rot_ang_labels(16, 5, 2.2, 6, 60);
deg_per_TR = rot_ang_labels(3, :);
ang_bin_labels = gen_bin_labels(nTR_bins, deg_per_TR);

% Loop through each roi
[bold_type_all, roi_name] = read_roi_txt_info('todo_rois_dim_red.txt');
for roi_ctr = 1:length(bold_type_all)
    
    bold_type = bold_type_all{roi_ctr, 1};
    ts_roi_name = roi_name{roi_ctr, 1};
    fprintf('Analyzing ROI: %s\n\n', ts_roi_name);
    
    fig_dir = fullfile(nsub_dir, 'Figures_dim_red', ts_roi_name);
    % ---------------------------------------------------
    fig_name = sprintf('ave_pca_lle_nnei%d_mdim%d_nTR%d.png', nnei, max_dim, nTR_bins);
    % ---------------------------------------------------
    if exist(fullfile(fig_dir, fig_name), 'file')
        fprintf('%s already exists!\n\n', fig_name);
        fprintf('=======================================================\n\n');
        continue;
    end
    h_fig = figure;
    % Customize figure size
    set(h_fig, 'Position', [1 scrsz(4)/2 scrsz(3)*0.5 scrsz(4)*0.5]);
    set(gcf, 'Color', [1 1 1]);
    
    % Average brain patterns across multiple runs
    [ave_brain_pattern, ~, pattern_ijk, pattern_mni] = gen_ave_pattern_across_runs(nsub_dir, ts_roi_name);
    % Average brain patterns across multiple TR bins
    ave_brain_pattern_TRbin = gen_ave_patterns_across_TRbin(ave_brain_pattern, ang_bin_labels);
    
    % Dimension reduction
    % PCA
    pca_embed_mtrx = pca(ave_brain_pattern_TRbin', 'NumComponents', max_dim);
    subplot(1, 2, 1);
    gen_scatter_text(pca_embed_mtrx(:, 1), pca_embed_mtrx(:,2), subset_TR_labels);
    subfig1 = sprintf('nnei%d mdim %d - pca', nnei, max_dim);
    title(subfig1, 'FontSize', 10);
    axis square;
    
    % lle
    lle_embed_mtrx = run_lle_roweis(ave_brain_pattern_TRbin, nnei, max_dim, mode);
    % Generate figures
    subplot(1, 2, 2);
    gen_scatter_text(lle_embed_mtrx(:, 1), lle_embed_mtrx(:, 2), subset_TR_labels);
    subfig2 = sprintf('lle');
    title(subfig2, 'FontSize', 10);
    axis square;
    
    % Isomap
    %     [embed_mtrx, residuals] = run_iso_tenen(ave_brain_pattern_TRbin, nnei, max_dim, mode);
    %     iso_embed_mtrx = embed_mtrx.coords{2}';
    %     % Generate figures
    %     subplot(1, 2, 2);
    %     gen_scatter_text(iso_embed_mtrx(:, 1), iso_embed_mtrx(:, 2), subset_TR_labels);
    %     subfig2 = sprintf('lle');
    %     title(subfig2, 'FontSize', 10);
    %     axis square;
    
    % Save figures
    export_fig(h_fig, fullfile(fig_dir, fig_name), '-m1.25');
    close(h_fig);
        
    fprintf('ROI: %s is done!!\n\n', ts_roi_name);
    fprintf('=======================================================\n\n');

end





% Generate color labels
% ang_bin_labels = gen_bin_labels(num_bins, rot_ang_labels(3, :));