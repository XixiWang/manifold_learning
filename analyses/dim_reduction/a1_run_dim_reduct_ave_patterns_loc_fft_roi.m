% Script to run dimension reduction using Fourier based localizer rois
%
% Output files are:
% .../Data_ToolRotation/Figures_dim_red_cmap_hot/ToolRotationID/Localizer_Fourier/***.png
% .../ToolRotationID/loc_fft_dim_red_log***

% Update 1/20/2016:
% - Update roi names
% - Update output figure directory
% - return warning messages if roi_ts.mat file doesn't exist
% - add dr_method variable to select different dimension reduction method
% - add dim_red_log diary file

clear;
clc;
close all;

[nsub_dir, subID] = toolrotationBaseDir;
base_dir = fileparts(nsub_dir);
bold_dir_listing = dir('ToolRotationRun*')';

% -------------------------------------------------------------
mode = 'pattern';
dr_method = 'lle';
nnei = 3; % # of neighbor points
max_dim = 2; % # of max dimensionality
nTR_bins = 33; % # of total TR bins
bold_type = 'unsmoothed';
cmap = 'hsv';
% -------------------------------------------------------------
fig_dir = fullfile(base_dir, sprintf('Figures_dim_red_%s', cmap), subID, 'Localizer_Fourier');
% fig_dir = fullfile(base_dir, 'Figures_dim_red_cmap_hot', subID, 'Localizer_Fourier');
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

subset_TR_labels = num2str([1: nTR_bins]');
scrsz = get(groot, 'ScreenSize');

% Generate rotation angles
rot_ang_labels = gen_rot_ang_labels(16, 5, 2.2, 6, 60);
deg_per_TR = rot_ang_labels(3, :);
ang_bin_labels = gen_bin_labels(nTR_bins, deg_per_TR);

log_fname = sprintf('loc_fft_dim_red_log_%s_%s_nnei%d_dim%d_bins%d_%s', ...
    mode, dr_method, nnei, max_dim, nTR_bins, cmap);
if ~exist(fullfile(nsub_dir, log_fname), 'file')
    diary on;
    diary(log_fname);
end

for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    ts_mtrx_dir = fullfile(bold_dir, 'roi_mtrx');
    fft_loc_tsmtrx_name = sprintf('*run%d*twopeaks*loc*.mat', runIdx);
    fft_loc_tsmtrx_name = dir(fullfile(ts_mtrx_dir, fft_loc_tsmtrx_name));
    
    % Loop through each Fourier based localizer ROI
    for ctr = 1:length(fft_loc_tsmtrx_name)
        ind_fft_loc_tsmtrx_name = fft_loc_tsmtrx_name(ctr).name;
        [~, temp_name] = fileparts(ind_fft_loc_tsmtrx_name);
        
        if ~exist(fullfile(ts_mtrx_dir, check_var_ext(temp_name, '.mat')), 'file')
            warning('%s/%s%s does not exist!!!', 'roi_mtrx', temp_name, '.mat');
            continue;
        end
        
        loc_idx = temp_name(end - 3: end);
        fig_name = sprintf('%s_pca_%s_run%d_nnei%d_mdim%d_nTR%d.png', loc_idx, dr_method, runIdx, nnei, max_dim, nTR_bins);
        if exist(fullfile(fig_dir, fig_name), 'file')
            fprintf('%s already exists!\n\n', fig_name);
            continue;
        end
        
        % Load time series matrix
        [ts_mtrx, ts_ijk_coords, ts_mni_coords] = load_ts_mtrx(ts_mtrx_dir, ind_fft_loc_tsmtrx_name);
        % Average brain patterns for each angle bin
        [ave_pattern_mtrx, bin_TR_idx] = gen_ave_patterns_across_TRbin(ts_mtrx, ang_bin_labels);
        
        tot_nvox = size(ave_pattern_mtrx, 2);
        h_fig = figure;
        % Customize figure size
        set(h_fig, 'Position', [1 scrsz(4)/2 scrsz(3)*0.5 scrsz(4)*0.5]);
        set(gcf, 'Color', [1 1 1]);
        
        % Dimension reduction
        % PCA
        pca_embed_mtrx = pca(ave_pattern_mtrx', 'NumComponents', max_dim);
        subplot(1, 2, 1);
        gen_scatter_text(pca_embed_mtrx(:, 1), pca_embed_mtrx(:,2), subset_TR_labels, cmap);
        subfig1 = sprintf('%d voxels: nnei%d mdim %d - pca: %s', tot_nvox, nnei, max_dim, bold_run_name);
        title(subfig1, 'FontSize', 10);
        axis square;
        
        if strcmp(dr_method, 'lle')
            % lle
            lle_embed_mtrx = run_lle_roweis(ave_pattern_mtrx, nnei, max_dim, mode);
            % Generate figures
            subplot(1, 2, 2);
            gen_scatter_text(lle_embed_mtrx(:, 1), lle_embed_mtrx(:, 2), subset_TR_labels, cmap);
            subfig2 = sprintf('lle: %s', bold_run_name);
            title(subfig2, 'FontSize', 10);
            axis square;
        elseif strcmp(dr_method, 'iso')
            % Isomap
            [embed_mtrx, residuals] = run_iso_tenen(ave_pattern_mtrx, nnei, max_dim, mode);
            iso_embed_mtrx = embed_mtrx.coords{2}';
            % Generate figures
            subplot(1, 2, 2);
            gen_scatter_text(iso_embed_mtrx(:, 1), iso_embed_mtrx(:, 2), subset_TR_labels, cmap);
            subfig2 = sprintf('isomap: %s', bold_run_name);
            title(subfig2, 'FontSize', 10);
            axis square;
        else
            error('Unknown dimension reduction approach!');
        end
        
        % Save figures
        export_fig(h_fig, fullfile(fig_dir, fig_name), '-m1.25');
        close(h_fig);
        fprintf('***************************************************************\n\n');
    end
    
end

cd(nsub_dir);
if exist(fullfile(nsub_dir, log_fname), 'file')
    diary off;
end
cd(nsub_dir);
