function embed_mtrx = run_lle_roweis(ts_mtrx, nnei, max_dim, mode)
% run Local Linear Embedding (LLE) dimension reduction using Roweis'
% toolbox
% 
% Detailed information for LLE toolbox be found: 
% https://www.cs.nyu.edu/~roweis/lle/algorithm.html
% 
% https://www.cs.nyu.edu/~roweis/lle/code.html
% 
% Input: 
%   ts_mtrx: time series matrix, #TR-by-#voxels dimensional (i.e. 174 x 3000
%   dimensional)
% 
%   nnei: number of neighbors, free paramter
% 
%   max_dim: max embedding dimensionality
% 
%   mode: 'pattern' for brain pattern analysis; 'ts' for time series
%   analysis
% 
% Output:
%   embed_mtrx: output embedding matrix, #var-by-max_dim dimensional (i.e.
%   174*2 for brain pattern analysis, 3000*2 for time series analysis)

addpath('~/Documents/Toolboxes/LLE/');

if strcmp(mode, 'pattern')
    fprintf('Analyzing brain patterns \n\n');
    input_mtrx = ts_mtrx';
elseif strcmp(mode, 'ts')
    fprintf('Analyzing time series \n\n');
    input_mtrx = ts_mtrx;
end

fprintf('%d variables in total \n\n', size(input_mtrx, 2));
embed_mtrx = lle_roweis(input_mtrx, nnei, max_dim);

embed_mtrx = embed_mtrx';
end