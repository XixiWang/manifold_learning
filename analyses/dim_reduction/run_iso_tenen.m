function [embed_coords, residuals] = run_iso_tenen(ts_mtrx, nnei, max_dim, mode)
% run isomap dimension reduction using Tenenbaum's toolbox
% 
% Detailed information for isomap toolbox be found: 
% http://isomap.stanford.edu/
% 
% Input: 
%   ts_mtrx: time series matrix, #TR-by-#voxels dimensional (i.e. 174 x 3000
%   dimensional)
% 
%   nnei: number of neighbors, free paramter
% 
%   max_dim: max embedding dimensionality
% 
%   mode: 'pattern' for brain pattern analysis; 'ts' for time series
%   analysis
% 
% Output:
%   embed_coords: embed_coords.coords is a cell array
% 
%   residuals: residual variances for embeddings in Y
% 
%   edge_mtrx: edge matrix for neighborhood graph
% 

addpath('~/Documents/Toolboxes/IsomapR1/');

if strcmp(mode, 'pattern')
    fprintf('Analyzing brain patterns \n\n');
    input_mtrx = ts_mtrx';
elseif strcmp(mode, 'ts')
    fprintf('Analyzing time series \n\n');
    input_mtrx = ts_mtrx;
end

fprintf('%d variables in total \n\n', size(input_mtrx, 2));

Dis_mtrx = L2_distance(input_mtrx, input_mtrx);

options.dims = 1:max_dim;
options.display = 0;
options.overlay = 0;
options.verbose = 0;

% Call isomap function from Tenenbaum's toolbox
[embed_coords, residuals] = Isomap_jbt(Dis_mtrx, 'k', nnei, options);

end