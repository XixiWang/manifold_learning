% Script to generate Fourier based localizer rois and then extract 
% corresponding time matrix
% 
% Output files are:
% ToolRotationID/ToolRotationRun*/roi_mtrx/...run*stimuli*loc.mat

clear;
clc;
close all;
addpath('~/Documents/Code/manifold_learning/analyses/');
nsub_dir = toolrotationBaseDir;

% ------------------------------------------------
loc_roi_dir = fullfile(nsub_dir, 'ROI_Localizer');
fourier_roi_dir = fullfile(nsub_dir, 'ROI_Fourier');
roi_bold_type = 'unsmoothed';
% ------------------------------------------------
overlap_roi_dir = fullfile(nsub_dir, 'ROI_Overlap');

bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % Find overlap rois
    %     loc_roi_name = 'sel_overall_roi_loc2';    
    %     fourier_roi_name = sprintf('fourier_ROI_run%d*twopeaks.nii', runIdx);
    %     fprintf('Combining %s and %s \n\n', loc_roi_name, fourier_roi_name);
    %     combine_rois()
    
    overlap_roi_name = sprintf('run%d*twopeaks*loc3.nii', runIdx);
    overlap_roi_name = dir(fullfile(overlap_roi_dir, overlap_roi_name));
    overlap_roi_name = overlap_roi_name.name;
    
    % Extract corresponding time series
    % ------------------------------------------------
    ts_output_dir = fullfile(bold_dir, 'roi_mtrx');
    [~, temp_name] = fileparts(overlap_roi_name);
    ts_output_name = sprintf('%s_%s_%s', roi_bold_type, bold_run_name, temp_name);
    % ------------------------------------------------

    extract_masked_timeseries(bold_dir, roi_bold_type, overlap_roi_dir, ...
        overlap_roi_name, ts_output_dir, ts_output_name) 
    
    fprintf('***************************************************************\n\n');
end
cd(nsub_dir);