% Script to generate combined ho-rois or sub combined ho-rois and then
% extracted corresponding time matrix
% 
% Output files are combined_roi.nii & combined_roi_ts.mat

clear;
clc;
close all;
addpath('~/Documents/Code/manifold_learning/analyses/');
nsub_dir = toolrotationBaseDir;

% ------------------------------------------------
roi_dir = fullfile(nsub_dir, 'ROI_harv_oxf');
sel_ho_roi1 = 'sub_LingualGyrus';
sel_ho_roi2 = 'sub_OccipitalPole';
% sel_ho_roi1 = 'LingualGyrus';
% sel_ho_roi2 = 'OccipitalPole';
roi_bold_type = 'unsmoothed';
% ------------------------------------------------

bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % Combine two rois
    sel_ho_roi1_ind_run = sprintf('%s_run%d', sel_ho_roi1, runIdx);
    sel_ho_roi2_ind_run = sprintf('%s_run%d', sel_ho_roi2, runIdx);
    combine_rois(roi_dir, sel_ho_roi1_ind_run, roi_dir, sel_ho_roi2_ind_run);
    
    % Extract corresponding time series
    % ------------------------------------------------
    combined_roi_name = sprintf('%s_%s.nii', sel_ho_roi1_ind_run, sel_ho_roi2_ind_run);
    ts_output_dir = fullfile(bold_dir, 'roi_mtrx');
    ts_output_name = sprintf('%s_%s_%s_%s', ...
        roi_bold_type, bold_run_name, sel_ho_roi1_ind_run, sel_ho_roi2_ind_run);
    % ------------------------------------------------
    extract_masked_timeseries(bold_dir, roi_bold_type, roi_dir, combined_roi_name, ...
        ts_output_dir, ts_output_name);
    
    fprintf('====================================================\n\n');

end