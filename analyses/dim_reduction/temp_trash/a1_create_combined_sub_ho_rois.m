% Script to extract time series for ho-rois
% (Can also combine two rois and generate a sub roi)
%
% Update 01/18/2016: add ROI_info.csv file for batch ROI time series
% extraction
% 
% Output files are: .../ROI_harv_oxf/combined_ho_roi.nii,
% .../roi_mtrx/sub_ho_roi_ts.mat
% 

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses');
nsub_dir = toolrotationBaseDir;

% --------------------------------------------------------
roi_dir = fullfile(nsub_dir, 'ROI_harv_oxf');
sel_hos = {'FrontalPole', 'LateralOccipitalCortexsuperiordivision', ...
        'LingualGyrus', 'OccipitalPole', 'TemporalOccipitalFusiformCortex'};
roi_bold_type = 'unsmoothed';
% --------------------------------------------------------

bold_dir_listing = dir('ToolRotationRun*')';
% Loop through each sub ho-roi
for roi_ctr = 1:length(sel_hos)
    roi_name = sel_hos{roi_ctr};
    
    % Loop through each functional runs
    for runIdx = 1:length(bold_dir_listing)
        bold_run_name = bold_dir_listing(runIdx).name;
        bold_dir = fullfile(nsub_dir, bold_run_name);
        cd(bold_dir);
        
        mask_name = sprintf('sub_%s_run%d.nii', roi_name, runIdx);
        [~, temp_dir] = fileparts(roi_dir);
        fprintf('Applying ROI mask: %s/%s\n\n', temp_dir, mask_name);
        % Output dir
        % ------------------------------------------------
        output_dir = fullfile(bold_dir, 'roi_mtrx');
        output_name = [roi_bold_type '_' bold_run_name '_' mask_name(1:end-4)];
        % ------------------------------------------------
        
        % Extract and save masked time series
        extract_masked_timeseries(bold_dir, roi_bold_type, roi_dir, mask_name, output_dir, output_name);
        fprintf('-----------------------------------------\n\n');
    end
    fprintf('====================================================\n\n');
    
end
