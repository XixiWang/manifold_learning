% TODO: 
% Update localizer names
% 
clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses/');
addpath('~/Documents/Code/manifold_learning/analyses/dim_reduction/');
addpath('~/Documents/utilities/export_fig/');

% ToolRotation09
nsub_dir = toolrotationBaseDir;
bold_dir_listing = dir('ToolRotationRun*')';

% -------------------------------------------------------------
mode = 'pattern';
nnei = 3; % nnei = 10;
max_dim = 10; % max_dim = 10;
nTR_bins = 33; % # of data points that are going to be plotted
bold_type = 'unsmoothed';
fig_dir = fullfile(nsub_dir, 'Figures_dim_red_cmap_hot', 'Localizer_Fourier');
% -------------------------------------------------------------
if ~exist(fig_dir, 'dir')
    mkdir(fig_dir);
end

subset_TR_labels = num2str([1: nTR_bins]');
scrsz = get(groot, 'ScreenSize');

% Generate rotation angles
rot_ang_labels = gen_rot_ang_labels(16, 5, 2.2, 6, 60);
deg_per_TR = rot_ang_labels(3, :);
ang_bin_labels = gen_bin_labels(nTR_bins, deg_per_TR);

for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    roi_mtrx_dir = fullfile(bold_dir, 'roi_mtrx');
    fft_loc_tsmtrx_name = sprintf('*run%d*twopeaks*loc3.mat', runIdx);
    fft_loc_tsmtrx_name = dir(fullfile(roi_mtrx_dir, fft_loc_tsmtrx_name));
    fft_loc_tsmtrx_name = fft_loc_tsmtrx_name.name;
    % ---------------------------------------------------
    fig_name = sprintf('pca_iso_run%d_nnei%d_mdim%d_nTR%d.png', runIdx, nnei, max_dim, nTR_bins);
    % ---------------------------------------------------
    if exist(fullfile(fig_dir, fig_name), 'file')
        fprintf('%s already exists!\n\n', fig_name);
        continue;
    end
    
    ts_mtrx_dir = fullfile(bold_dir, 'roi_mtrx');
    
    % Load time series matrix
    [ts_mtrx, ts_ijk_coords, ts_mni_coords] = load_ts_mtrx(ts_mtrx_dir, fft_loc_tsmtrx_name);
    % Average brain patterns for each angle bin
    [ave_pattern_mtrx, bin_TR_idx] = gen_ave_patterns_across_TRbin(ts_mtrx, ang_bin_labels);
    
    tot_nvox = size(ave_pattern_mtrx, 2);
    h_fig = figure;
    % Customize figure size
    set(h_fig, 'Position', [1 scrsz(4)/2 scrsz(3)*0.5 scrsz(4)*0.5]);
    set(gcf, 'Color', [1 1 1]);
    
    % Dimension reduction
    % PCA
    pca_embed_mtrx = pca(ave_pattern_mtrx', 'NumComponents', max_dim);
    subplot(1, 2, 1);
    gen_scatter_text(pca_embed_mtrx(:, 1), pca_embed_mtrx(:,2), subset_TR_labels);
    subfig1 = sprintf('%d voxels: nnei%d mdim %d - pca: %s', tot_nvox, nnei, max_dim, bold_run_name);
    title(subfig1, 'FontSize', 10);
    axis square;
    
    %     % lle
    %     lle_embed_mtrx = run_lle_roweis(ave_pattern_mtrx, nnei, max_dim, mode);
    %     % Generate figures
    %     subplot(1, 2, 2);
    %     gen_scatter_text(lle_embed_mtrx(:, 1), lle_embed_mtrx(:, 2), subset_TR_labels);
    %     subfig2 = sprintf('lle: %s', bold_run_name);
    %     title(subfig2, 'FontSize', 10);
    %     axis square;
    
    % Isomap
    [embed_mtrx, residuals] = run_iso_tenen(ave_pattern_mtrx, nnei, max_dim, mode);
    iso_embed_mtrx = embed_mtrx.coords{2}';
    % Generate figures
    subplot(1, 2, 2);
    gen_scatter_text(iso_embed_mtrx(:, 1), iso_embed_mtrx(:, 2), subset_TR_labels);
    subfig2 = sprintf('isomap: %s', bold_run_name);
    title(subfig2, 'FontSize', 10);
    axis square;
    
    % Save figures
    export_fig(h_fig, fullfile(fig_dir, fig_name), '-m1.25');
    close(h_fig);
    fprintf('=======================================================\n\n');
end

cd(nsub_dir);
