% Read in masks and extract corresponding time series
% 
% 
% Update 01/14/2016: functionalized extract_masked_timeseries
% 
% Output files are: .../roi_mtrx/roi_ts.mat

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses');
nsub_dir = toolrotationBaseDir;

% ROI director
% -------------------------------
mask_dir = fullfile(nsub_dir, 'ROI_harv_oxf'); 
bold_type = 'unsmoothed';
% -------------------------------
[mask_vol, mask_name, ~] = read_roi_vol(mask_dir);
% roi_ijk_coords = get_roi_coords(roi_vol);

% Batch process each functional run
bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % Use func-loc hybrid rois
    % ------------------------------
    roi_ts_name = [bold_type '_' bold_run_name '_' mask_name(1: end - 4)];
    roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
    % -------------------------------
    extract_masked_timeseries(bold_dir, bold_type, mask_dir, mask_name, roi_ts_dir, roi_ts_name);
       
end

% Compare time series
% compare_timeseries(ts_file, ind_mni_coords);