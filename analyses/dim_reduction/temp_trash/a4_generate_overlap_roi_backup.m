% Generate Fourier functional & localizer overlapped ROIs
% Script to generate Fourier based localzier rois and then extract
% corresponding time matrix

clear;
clc;
close all;

addpath('~/Documents/Code/manifold_learning/analyses/')
addpath('~/Documents/Code/manifold_learning/analyses/func_fourier/');

nsub_dir = toolrotationBaseDir;

fourier_roi_dir = fullfile(nsub_dir, 'ROI_Fourier');
localizer_roi_dir = fullfile(nsub_dir, 'ROI_Localizer');
% ---------------------------------------
fourier_roi_listing = dir(fullfile(fourier_roi_dir, 'fourier*twopeaks.nii'));
loc_roi_listing = dir(fullfile(localizer_roi_dir, 'sel*loc*.nii'));
% ---------------------------------------

overlap_roi_dir = fullfile(nsub_dir, 'ROI_Overlap');
if ~exist(overlap_roi_dir, 'dir')
    mkdir(overlap_roi_dir);
end

% ---------------------------------------
fileID = fopen(fullfile(overlap_roi_dir, 'overlap_rois_twopeaks.txt'), 'a+');
% ---------------------------------------
for ctri = 1:length(fourier_roi_listing)
    for ctrj = 1:length(loc_roi_listing)
        [~, tempname1, ~] = fileparts(fourier_roi_listing(ctri).name);
        [~, tempname2, ~] = fileparts(loc_roi_listing(ctrj).name);
        
        overlap_roi_name = strcat(tempname1(13:end), '_', tempname2(17:end));
        overlap_roi_name_full = fullfile(overlap_roi_dir, overlap_roi_name);
        
        if exist(fullfile(overlap_roi_dir, [overlap_roi_name '.nii']), 'file')
            fprintf('Overlap ROI %s already exist! \n', overlap_roi_name);
            continue;
        else
            
            overlap_roi = find_overlap_vol(...
                fullfile(fourier_roi_dir, [tempname1 '.nii']), ...
                fullfile(localizer_roi_dir, [tempname2 '.nii']), ...
                1, 'on', ...
                overlap_roi_name_full);
            
            fprintf(fileID, '%s: %d voxels in total \n', overlap_roi_name, sum(overlap_roi(:)));
        end
        
    end
end
fclose(fileID);