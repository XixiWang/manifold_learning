% Script to load fourier spectrum for time series of the whole brain and
% perform "Fourier functional ROI" selection
% 
% Update 12/23/2015: 
% - Read FFT_nofixation.mat
% - Change xyz_coords to ijk_coords to avoid confusion
% - Add tool stimuli information
% 
% Update 1/7/2015:
% - Modify select_fourier_ROI function

clear;
clc;
% close all;

nsub_dir = toolrotationBaseDir;

% Load stimuli info
load('~/Documents/Data/caos_toolrotation/Data_ToolRotation/StimuliOrder.mat', 'StimuliNames');
[~, b] = fileparts(nsub_dir);
subid = str2double(b(end-1:end)) - 1; % Start from ToolRotation02 - ..10!
ind_stimuli_info = StimuliNames(subid, :);

% Check Fourier roi directory
fourier_roi_dir = fullfile(nsub_dir, 'ROI_Fourier');
if ~exist(fourier_roi_dir, 'dir')
    mkdir(fourier_roi_dir);
end

% Loop through each functional run
Func_dir_listing = dir('ToolRotationRun*');
type = 'unsmoothed';
for runIdx = 1:length(Func_dir_listing)
    bold_run_name = Func_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    temp_sti = ind_stimuli_info{runIdx};
    
    roi_name1 = ['fourier_ROI_run' num2str(runIdx) '_' temp_sti '_onepeak.nii'];
    roi_name2 = ['fourier_ROI_run' num2str(runIdx) '_' temp_sti '_twopeaks.nii'];

    if exist(fullfile(fourier_roi_dir, roi_name2), 'file')
        fprintf('Fourier ROI already exists for %s \n', bold_run_name);
        continue;
    else
        fprintf('Analyzing %s \n', bold_run_name);
        FFT_filename = [type '_' bold_run_name '_FFT_nofixation.mat'];
        load(FFT_filename, 'T');
        
        % Generate Fourier functional ROI
        % Use default cut-off frequency 0.0151 - 0.0186 Hz
        sel_T1 = select_fourier_ROI(T);
        % Strict filtering
        sel_T2 = select_fourier_ROI(T, 0.0151, 0.0186, 2);
        
        % Read in bold file and write nifti file
        mean_bold = dir(fullfile(nsub_dir, 'wmeana*bold*.nii'));
        mean_bold = mean_bold.name;
        mean_hdr = spm_vol(fullfile(nsub_dir, mean_bold));
        
        vol_size = mean_hdr.dim;
        vec_coords1 = sel_T1.ijk_coords;
        fourier_ROI_vol1 = vec2vol(vol_size, vec_coords1);
        
        vec_coords2 = sel_T2.ijk_coords;
        fourier_ROI_vol2 = vec2vol(vol_size, vec_coords2);
        
        % Write nifti file
        fourier_ROI_hdr1 = mean_hdr;
        fourier_ROI_hdr1.fname = fullfile(fullfile(fourier_roi_dir, roi_name1));
        spm_write_vol(fourier_ROI_hdr1, fourier_ROI_vol1);
        
        fourier_ROI_hdr2 = mean_hdr;
        fourier_ROI_hdr2.fname = fullfile(fullfile(fourier_roi_dir, roi_name2));
        spm_write_vol(fourier_ROI_hdr2, fourier_ROI_vol2);
        
    end
      
end