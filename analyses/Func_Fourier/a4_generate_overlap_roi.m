% Script to generate Fourier based localzier rois and then extract
% corresponding time matrix
%
% Output files are:
% ToolRotationID/ROI_Overlap/overlapped_roi.nii
% ToolRotationID/ROI_Overlap/overlap_rois_*peaks.txt
% ToolRotationID/ToolRotationRun*/roi_mtrx/...run*stimuli*loc.mat

clear;
clc;
close all;

nsub_dir = toolrotationBaseDir;
overlap_roi_dir = fullfile(nsub_dir, 'ROI_Overlap');
if ~exist(overlap_roi_dir, 'dir')
    mkdir(overlap_roi_dir);
end

fourier_roi_dir = fullfile(nsub_dir, 'ROI_Fourier');
localizer_roi_dir = fullfile(nsub_dir, 'ROI_Localizer');
% ---------------------------------------
fourier_roi_listing = dir(fullfile(fourier_roi_dir, 'fourier*twopeaks.nii'));
loc_roi_listing = dir(fullfile(localizer_roi_dir, 'sel*loc*.nii'));
fileID = fopen(fullfile(overlap_roi_dir, 'overlap_rois_twopeaks.txt'), 'a+');
roi_bold_type = 'unsmoothed';
% ---------------------------------------

for ctri = 1:length(fourier_roi_listing)
    for ctrj = 1:length(loc_roi_listing)
        [~, temp_vol1_name, ~] = fileparts(fourier_roi_listing(ctri).name);
        [~, temp_vol2_name, ~] = fileparts(loc_roi_listing(ctrj).name);
        
        overlap_roi_name = strcat(temp_vol1_name(13:end), '_', temp_vol2_name(17:end));
        overlap_roi_name = check_var_ext(overlap_roi_name, '.nii');
        
        % Generate overlapped .nii file
        if exist(fullfile(overlap_roi_dir, overlap_roi_name), 'file')
            [~, temp_dir] = fileparts(overlap_roi_dir);
            fprintf('%s/%s already exist! \n\n', temp_dir, overlap_roi_name);
            continue;
        else
            overlap_roi = find_overlap_vol(...
                fullfile(fourier_roi_dir, temp_vol1_name), ...
                fullfile(localizer_roi_dir, temp_vol2_name), ...
                1, 'on', ...
                overlap_roi_name);
            fprintf(fileID, '%s: %d voxels in total \n\n', overlap_roi_name, sum(overlap_roi(:)));
        end
        
    end
end
fclose(fileID);

fprintf('***************************************************************\n\n');

% Extract corresponding time series
bold_dir_listing = dir('ToolRotationRun*')';
for runIdx = 1:length(bold_dir_listing)
    bold_run_name = bold_dir_listing(runIdx).name;
    bold_dir = fullfile(nsub_dir, bold_run_name);
    cd(bold_dir);
    
    % ---------------------------------------
    ts_output_dir = fullfile(bold_dir, 'roi_mtrx');
    overlap_roi_name = sprintf('run%d*twopeaks*loc*.nii', runIdx);
    % ---------------------------------------
    overlap_roi_name = dir(fullfile(overlap_roi_dir, overlap_roi_name));
    
    for ctr = 1:length(overlap_roi_name)
        ind_overlap_roi_name = overlap_roi_name(ctr).name;
        [~, temp_name] = fileparts(ind_overlap_roi_name);
        fprintf('Extracting time series for %s \n\n', temp_name);
        % ---------------------------------------
        ts_output_name = sprintf('%s_%s_%s', roi_bold_type, bold_run_name, temp_name);
        % ---------------------------------------
        extract_masked_timeseries(bold_dir, roi_bold_type, overlap_roi_dir, ...
            ind_overlap_roi_name, ts_output_dir, ts_output_name)
    end
    
    fprintf('----------------------------------------------------------\n');
end
fprintf('***************************************************************\n\n');

cd(nsub_dir);