function [Y_ts, pk_vals, pk_freqs] = compute_ts_fft(time_series, TR, fig_mode, NFFT)
% [Y_ts, pk_vals, pk_freqs] = calc_ts_fft(time_series, TR[, fig_mode, NFFT]):
% Compute FFT of time series and return first five peak values and
% corresponding frequencies based on the amplitude spectrum
%
% Input:
%   time_series: time series vector
%
%   TR: in secs
%
% Optional input:
%   fig_mode: set the display as 'on' or 'off', default is 'on'
%
%   FFT transformation length, default is 512
%
% Output:
%   Y_ts: single-sided amplitude spectrum of time series
%
%   pk_vals: time series's amplitude spectrum's top 5 peak values (in
%   descending order
% 
%   pk_freqs: corresponding peak frequencies

% Default FFT transformation length
if nargin < 3
    fig_mode = 'on';
end

% Default figure mode
if nargin < 4
    % NFFT = 2^nextpow2(length(time_series));
    NFFT = 2^nextpow2(length(time_series)) * 2;
end

% Sampling freqency in Hz
Fs = 1/TR;
num_vol = length(time_series);
% Generate time vector
time_vec = (0:num_vol - 1) * TR;

% Two-sided frequency spectrum
Y2_ts = fft(time_series, NFFT)/num_vol;
f = Fs/2 * linspace(0, 1, NFFT/2 + 1);
% Generate single-sided power spectrum
Y_ts = 2 * abs(Y2_ts(1: NFFT/2 + 1));

% Find peaks of the amplitude spectrum
[pk_vals, locs] = findpeaks(Y_ts, 'SORTSTR', 'descend', 'NPEAKS', 5);
pk_freqs = f(locs);

if strcmp(fig_mode, 'on')
    scrsz = get(0, 'ScreenSize');
    hfig = figure(1);
    set(hfig, 'Position', [1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]);
    % Plot original time series
    subplot(2, 1, 1);
    plot(time_vec, time_series);
    xlim([min(time_vec) max(time_vec)]);
    axh1 = gca;
    set(axh1, 'YGrid', 'on');
    title('Original time series');
    xlabel('Time (secs)');
    ylabel('Amplitude');
    
    % Plot single-sided amplitude spectrum
    subplot(2, 1, 2);
    plot(f, Y_ts);
    hold on;
    % Plot peaks
    plot(pk_freqs, pk_vals, 'or');
    hold off;
    axh2 = gca;
    set(axh2, 'XGrid', 'on');
    set(axh2, 'XTick', min(f): 0.009: max(f));
    xlim([min(f) max(f)]);
    title('Single-Sided Amplitude Spectrum of time series')
    xlabel('f (Hz)')
    ylabel('|Y(f)|')
end

end