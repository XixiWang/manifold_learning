function outvol = vec2vol(vol_size, vec_coords, vec_vals)
% OutVol = vec2vol(vol_size, vec_coords[, vec_vals]):
% Function to convert 1D vector to 3D volume
% 
% Input:
%   vol_size: template volume size, [sizex, sizey, sizez]
% 
%   vec_coords: vector coordinates, n-by-3 matrix
% 
%   vec_vals: vector values, n-by-1 matrix. vec_vals can be empty if mask
%   volume is generated

% Default: generate mask volume;
if nargin < 3
    vec_vals = ones(size(vec_coords, 1), 1);
end

outvol = zeros(vol_size);
for ctr = 1:length(vec_coords)
    outvol(vec_coords(ctr, 1), vec_coords(ctr, 2), vec_coords(ctr, 3)) = vec_vals(ctr);
end

end