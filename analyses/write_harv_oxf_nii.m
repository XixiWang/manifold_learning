function write_harv_oxf_nii(roi_ijk_coords, ho_roi_name, output_path, harv_oxf_template)
% write_harv_oxf_mask(roi_ijk_coords[, ho_roi_name, output_path, harv_oxf_template])
% function to output nifti file for harvard-oxford roi, this is to check 
% whether the generated ho-roi is right
% 
% Input:
%   roi_ijk_coords: N-by-3 ijk coordinates
% 
%   ho_roi_name: default is 'harvard_oxford_roi.nii'
%  
%   output_path: default is '.../Data_Toolrotation/harvard_oxford'
% 
%   harv_oxf_templates: default is harvard_oxford_toolrotation.nii
% 
% Update 01/21/2016:
% - modify output path for multi platforms use

if ismac,
    data_basedir = '~/Documents/Data/caos_toolrotation/Data_ToolRotation';
    data_basedir = GetFullPath(data_basedir);
elseif isunix
    data_basedir = '/raizadaData/Tool_Rotation/Data_ToolRotation';
end

if ~exist(data_basedir, 'dir')
    warning('%s does not exist, please specify where it is!', ...
        '.../Tool_Rotation/Data_ToolRotation');
    data_basedir = uigetdir;
end

if nargin < 1
    error('roi coordinates must be specified');
end

if nargin < 2
    ho_roi_name = 'harvard_oxford_roi.nii';
end

if nargin < 3
    output_path = fullfile(data_basedir, 'harvard_oxford');
end

if nargin < 4
    harv_oxf_template = ...
        fullfile(data_basedir, 'harvard_oxford', 'harvard_oxford_toolrotation.nii');
end

ho_roi_name = check_var_ext(ho_roi_name, '.nii');

ho_temp_hdr = spm_vol(harv_oxf_template);
ho_temp_vol = spm_read_vols(ho_temp_hdr);

if ~exist(output_path, 'dir')
    mkdir(output_path);
    fprintf('Create path: %s\n\n', output_path);
end

if exist(fullfile(output_path, ho_roi_name), 'file')
    [~, temp_roi_path, ~] = fileparts(output_path);
    fprintf('%s already exists!\n\n', fullfile(temp_roi_path, ho_roi_name));
    return;
end

ho_roi_vol = zeros(size(ho_temp_vol));
for ctr = 1:size(roi_ijk_coords)
    ho_roi_vol(roi_ijk_coords(ctr, 1), roi_ijk_coords(ctr, 2), roi_ijk_coords(ctr, 3)) = 1;
end

ho_roi_hdr = ho_temp_hdr;
ho_roi_hdr.fname = fullfile(output_path, ho_roi_name);

spm_write_vol(ho_roi_hdr, ho_roi_vol);
end