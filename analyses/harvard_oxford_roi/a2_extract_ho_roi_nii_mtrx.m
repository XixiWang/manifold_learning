% Script to extract selected harvard-oxford ROIs for each subject
%
% Output files are:
% ToolRotationID/ROI_harv_oxf/ho_roi.nii
% ToolRotationID/ToolRotationRun*/roi_mtrx/ho_roi_ts.mat

clear;
clc;
close all;

[nsub_dir, ~, ~, base_data_dir] = toolrotationBaseDir;
harv_oxf_template_dir = fullfile(base_data_dir, 'harvard_oxford');
load(fullfile(harv_oxf_template_dir, 'harvard_oxford_toolrotation_all_rois.mat'));
bold_dir_listing = dir('ToolRotationRun*')';

% -------------------------------
% Extract specific ho roi
all_roi_idx = [1, 22, 23, 36, 39, 40, 48];
type = 'unsmoothed';
% -------------------------------

% Batch process each ho-roi
for nroi = 1:length(all_roi_idx)
    roi_idx = all_roi_idx(nroi);
    roi_vol = roi_masks_struct(roi_idx).both;
    roi_name = roi_masks_struct(roi_idx).name;
    roi_name = regexprep(roi_name,'[^\w'']','');
    fprintf('Generating ROI for %s: %d voxels in total\n\nVolume size is %d %d %d \n\n', ...
        roi_name, sum(roi_vol(:)), size(roi_vol));
    roi_ijk_coords = get_roi_coords(roi_vol);
    
    % Batch process each functional run
    for runIdx = 1:length(bold_dir_listing)
        bold_run_name = bold_dir_listing(runIdx).name;
        bold_dir = fullfile(nsub_dir, bold_run_name);
        cd(bold_dir);
        
        roi_ts_dir = fullfile(bold_dir, 'roi_mtrx');
        roi_ts_name = [type '_' bold_run_name '_' roi_name '.mat'];
        
        if exist(fullfile(roi_ts_dir, roi_ts_name), 'file')
            fprintf('%s already exists! \n\n', roi_ts_name);
            continue;
        end
        
        % Use time series without fixation
        if strcmp(type, 'smoothed')
            bold_file = dir(fullfile(bold_dir, 'smoothed*_timeseries_nofixation.mat'));
            bold_file = bold_file.name;
        elseif strcmp(type, 'unsmoothed')
            bold_file = dir(fullfile(bold_dir, 'unsmoothed*_timeseries_nofixation.mat'));
            bold_file = bold_file.name;
        end
        load(fullfile(bold_dir, bold_file));
        fprintf('Analyzing %s \n', bold_file);
        
        [sela, selb] = ismember(roi_ijk_coords, ijk_coords, 'rows');
        no_zeros = length(find(sela == 0));
        fprintf('Remove %d voxels \n', no_zeros);
        
        selb(selb == 0) = [];
        roi_zero_meaned_ts = zero_meaned_ts(:, selb);
        roi_ijk_coords = ijk_coords(selb, :);
        roi_mni_coords = mni_coords(selb, :);
        
        % Save time series matrix
        save(fullfile(roi_ts_dir, roi_ts_name), 'roi_zero_meaned_ts', ...
            'roi_ijk_coords', 'roi_mni_coords');
        
        % Save nifti files
        roi_output_dir = fullfile(nsub_dir, 'ROI_harv_oxf');
        write_harv_oxf_nii(roi_ijk_coords, [roi_name '_run' num2str(runIdx)], roi_output_dir);
        fprintf('-----------------------------------------\n\n');
    end
    
    fprintf('***************************************************************\n\n');
end
cd(nsub_dir);