% Script to compute (and to display) fourier spectrum for specific
% harvard-oxford rois, and then filter power spectrum according to top
% two highest peaks in the power spectrum. The script then generates
% Fourier-based sub ho-rois.
%
% Output files are:
% ToolRotationID/ToolRotationRun*/roi_fft/ho_roi_fft.mat
% ToolRotationID/ROI_harv_oxf/sub_ho_roi.nii
% ToolRotationID/ROI_harv_oxf/ho_roi.txt
%
% Update 1/19/2015:
% - modify roi name

clear;
clc;
close all;

[nsub_dir, ~, ~, base_data_dir] = toolrotationBaseDir;
harv_oxf_template_dir = fullfile(base_data_dir, 'harvard_oxford');
load(fullfile(harv_oxf_template_dir, 'harvard_oxford_toolrotation_all_rois.mat'));
bold_dir_listing = dir('ToolRotationRun*');

% --------------------------------------
bold_type = 'unsmoothed';
TR = 2.2;
all_roi_idx = [1, 22, 23, 36, 39, 40, 48];
% --------------------------------------

% Batch process each ho-roi
for nroi = 1:length(all_roi_idx)
    roi_idx = all_roi_idx(nroi);
    roi_vol = roi_masks_struct(roi_idx).both;
    roi_name = roi_masks_struct(roi_idx).name;
    roi_name = regexprep(roi_name,'[^\w'']','');
    roi_fft_txt = fopen(fullfile(nsub_dir, 'ROI_harv_oxf', [roi_name '.txt']), 'a+');
    fprintf('Analyzing ROI: %s \n\n', roi_name);
    
    % Batch process each functional run
    for runIdx = 1:length(bold_dir_listing)
        bold_run_name = bold_dir_listing(runIdx).name;
        bold_dir = fullfile(nsub_dir, bold_run_name);
        cd(bold_dir);
        
        roi_dir = fullfile(bold_dir, 'roi_mtrx');
        roi_fft_dir = fullfile(bold_dir, 'roi_fft');
        if ~exist(roi_fft_dir, 'dir')
            mkdir(roi_fft_dir);
        end
        
        roi_fft = [bold_type '_' bold_run_name '_FFT_' roi_name '.mat'];
        if exist(fullfile(roi_fft_dir, roi_fft), 'file')
            fprintf('FFT file for %s already exists! \n\n', bold_run_name);
            continue;
        end
        
        % Load time series mat file
        roi_ts_mtrx = [bold_type '_' bold_run_name '_' roi_name '.mat'];
        fprintf('Compute FFT for %s \n\n', roi_ts_mtrx);
        load(fullfile(roi_dir, roi_ts_mtrx));
        
        tot_voxel = size(roi_zero_meaned_ts, 2);
        tot_volume = size(roi_zero_meaned_ts, 1);
        all_voxel_fft = cell(1, tot_voxel);
        all_voxel_pk_freqs = zeros(tot_voxel, 5);
        all_voxel_pk_vals = zeros(tot_voxel, 5);
        
        % Generate Fourier power spectrum
        % Loop through every voxel
        for ind_vox_idx = 1:tot_voxel
            temp_ts = roi_zero_meaned_ts(:, ind_vox_idx);
            % Compute FFT
            % Turn figure window off
            [Y_ts, pk_vals, pk_freqs] = compute_ts_fft(temp_ts, TR, 'off');
            % [Y_ts, pk_vals, pk_freqs] = compute_ts_fft(temp_ts, TR, 'on');
            
            all_voxel_fft{1, ind_vox_idx} = Y_ts;
            all_voxel_pk_freqs(ind_vox_idx, :) = pk_freqs;
            all_voxel_pk_vals(ind_vox_idx, :) = pk_vals;
        end
        
        % Save FFT results and peak frequency values for each voxel
        voxel_idx = (1:tot_voxel)';
        T = table(voxel_idx, all_voxel_pk_freqs, roi_mni_coords, roi_ijk_coords);
        
        % Filter Fourier power spectrum
        sel_T = select_fourier_ROI(T, 0.0151, 0.0186, 2);
        save(fullfile(roi_fft_dir, roi_fft), 'all_voxel_fft', 'all_voxel_pk_vals', 'T', 'sel_T');
        
        fprintf(roi_fft_txt, '%s - %s: %d voxels in total, FFT filtered: %d voxels in total\n', ...
            bold_run_name, roi_name, size(T, 1), size(sel_T, 1));
        
        % Write functional selected ROI
        sub_horoi_output_dir = fullfile(nsub_dir, 'ROI_harv_oxf');
        write_harv_oxf_nii(sel_T.roi_ijk_coords, ['sub_' roi_name '_run' num2str(runIdx) '.nii'], sub_horoi_output_dir);
        
        fprintf('-----------------------------------------\n\n');
    end
    
    fclose('all');
    fprintf('***************************************************************\n\n');
end
cd(nsub_dir);