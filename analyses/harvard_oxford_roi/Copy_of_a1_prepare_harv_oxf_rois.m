% Prepare Harvard-Oxford ROIs for toolrotation dataset
%
% Output files are:
% .../Data_ToolRotation/harvard_oxford/ho_rois.nii

clear;
clc;
close all;

[root_code_dir, root_data_dir] = add_mani_learn_dir;

ho_roi_template = fullfile(root_code_dir, 'harvard_oxford_roi');
harvard_oxford_names = read_harvard_oxford_names;

% ho-roi template
cd(ho_roi_template);
ho_fn = fullfile(pwd, 'HarvardOxford-cort-maxprob-thr0-2mm.nii');

% Input func data
toolrotationBaseDir;
target_fn = fullfile(pwd, '/ToolRotationRun01/glm_func/unsmoothed/beta_0001.img');

% Output data
cd(root_data_dir);
output_fn = fullfile(pwd, 'harvard_oxford', 'harvard_oxford_toolrotation.nii');

% Realign
if ~exist(output_fn, 'file')
    put_into_same_voxel_space_spm5(ho_fn, target_fn, output_fn);
else
    disp('harvard_oxford_toolrotation.nii already exists!')
end

% Generate ROI images/struct
cd(root_data_dir);
harv_oxf_dir = fullfile(pwd, 'harvard_oxford');

if ~exist(fullfile(harv_oxf_dir, 'harvard_oxford_toolrotation_all_rois.mat'), 'file')
    [~, harv_oxf_realigned_fn] = fileparts(output_fn);
    roi_masks_struct = make_harvard_oxford_roi_masks(harv_oxf_dir, [harv_oxf_realigned_fn '.nii']);
else
    disp('harvard oxford rois for toolrotation dataset already exist!')
end
