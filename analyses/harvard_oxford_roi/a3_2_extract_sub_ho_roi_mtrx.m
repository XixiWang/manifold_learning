% Script to read sub ho-roi nii files and then extract time series for 
% sub ho-rois
% 
% Output files are: 
% ToolRotationID/ToolRotationRun*/roi_mtrx/sub_ho_roi_ts.mat
% 

clear;
clc;
close all;

[nsub_dir, ~, ~, base_data_dir] = toolrotationBaseDir;
roi_dir = fullfile(nsub_dir, 'ROI_harv_oxf');
harv_oxf_template_dir = fullfile(base_data_dir, 'harvard_oxford');
load(fullfile(harv_oxf_template_dir, 'harvard_oxford_toolrotation_all_rois.mat'));

% --------------------------------------------------------
bold_type = 'unsmoothed';
all_roi_idx = [1, 22, 23, 36, 39, 40, 48];
% --------------------------------------------------------

bold_dir_listing = dir('ToolRotationRun*')';
% Loop through each sub ho-roi
for nroi = 1:length(all_roi_idx)
    roi_idx = all_roi_idx(nroi);
    roi_vol = roi_masks_struct(roi_idx).both;
    roi_name = roi_masks_struct(roi_idx).name;
    roi_name = regexprep(roi_name,'[^\w'']','');
    
    % Loop through each functional runs
    for runIdx = 1:length(bold_dir_listing)
        bold_run_name = bold_dir_listing(runIdx).name;
        bold_dir = fullfile(nsub_dir, bold_run_name);
        cd(bold_dir);
        
        mask_name = sprintf('sub_%s_run%d.nii', roi_name, runIdx);
        [~, temp_dir] = fileparts(roi_dir);
        fprintf('Applying ROI mask: %s/%s\n\n', temp_dir, mask_name);
        
        % Output dir
        % ------------------------------------------------
        output_dir = fullfile(bold_dir, 'roi_mtrx');
        output_name = [bold_type '_' bold_run_name '_' mask_name(1:end-4)];
        % ------------------------------------------------
        
        % Extract and save masked time series
        extract_masked_timeseries(bold_dir, bold_type, roi_dir, mask_name, output_dir, output_name);
        fprintf('-----------------------------------------\n\n');
    end
    fprintf('***************************************************************\n\n');

end

cd(nsub_dir);