% Script to generate combined (sub)ho-rois or then
% extract corresponding time series matrix
%
% Output files are:
% ToolRotationID/ROI_harv_oxf/combined_roi.nii
% ToolRotationID/ToolRotationRun*/roi_mtrx/combined_roi_ts.mat

clear;
clc;
close all;

nsub_dir = toolrotationBaseDir;
bold_dir_listing = dir('ToolRotationRun*')';

% ------------------------------------------------
roi_dir = fullfile(nsub_dir, 'ROI_harv_oxf');
roi_bold_type = 'unsmoothed';
combined_rois = {'LingualGyrus', 'OccipitalPole'; ...
                'sub_LingualGyrus', 'sub_OccipitalPole'};
% ------------------------------------------------

for ncombine = 1:size(combined_rois, 1)
    sel_ho_roi1 = combined_rois{ncombine, 1};
    sel_ho_roi2 = combined_rois{ncombine, 2};
    
    for runIdx = 1:length(bold_dir_listing)
        bold_run_name = bold_dir_listing(runIdx).name;
        bold_dir = fullfile(nsub_dir, bold_run_name);
        cd(bold_dir);
        
        fprintf('Combining %s and %s for run%d\n\n', sel_ho_roi1, sel_ho_roi2, runIdx);
        
        % Combine two rois
        sel_ho_roi1_ind_run = sprintf('%s_run%d', sel_ho_roi1, runIdx);
        sel_ho_roi2_ind_run = sprintf('%s_run%d', sel_ho_roi2, runIdx);
        combine_rois(roi_dir, sel_ho_roi1_ind_run, roi_dir, sel_ho_roi2_ind_run);
        
        % Extract corresponding time series
        % ------------------------------------------------
        combined_roi_name = sprintf('%s_%s.nii', sel_ho_roi1_ind_run, sel_ho_roi2_ind_run);
        ts_output_dir = fullfile(bold_dir, 'roi_mtrx');
        ts_output_name = sprintf('%s_%s_%s_%s', ...
            roi_bold_type, bold_run_name, sel_ho_roi1_ind_run, sel_ho_roi2_ind_run);
        % ------------------------------------------------
        extract_masked_timeseries(bold_dir, roi_bold_type, roi_dir, combined_roi_name, ...
            ts_output_dir, ts_output_name);
        fprintf('-------------------------------------------------------\n\n');
        
    end
    
    fprintf('***************************************************************\n\n');
end
cd(nsub_dir);