function [ts_mtrx, ts_ijk_coords, ts_mni_coords, roi_ts_mtrx_name] = load_ts_mtrx(ts_mtrx_dir, ts_mtrx_name)
% function to load time series matrix for whole brain or specific rois
% 
% To be updated: right now the time series mat file should contain
% variables: (roi_)zero_meaned_ts, (roi_)ijk_coords, (roi_)mni)coords

ts_mtrx_name = check_var_ext(ts_mtrx_name, '.mat');

if ~exist(fullfile(ts_mtrx_dir, ts_mtrx_name), 'file')
    error('Time series matrix does not exist!');
end
[~, roi_ts_mtrx_name, ~] = fileparts(ts_mtrx_name);

load(fullfile(ts_mtrx_dir, ts_mtrx_name));
if exist('zero_meaned_ts', 'var') && exist('ijk_coords', 'var') && exist('mni_coords', 'var')
    frpintf('Loading time series matrix for whole brain \n\n');
    ts_mtrx = zero_meaned_ts;
    ts_ijk_coords = ijk_coords;
    ts_mni_coords = mni_coords;
elseif exist('roi_zero_meaned_ts', 'var') && exist('roi_ijk_coords', 'var') && exist('roi_mni_coords', 'var')
    fprintf('Loading time series for ROI: %s \n\n', roi_ts_mtrx_name);
    ts_mtrx = roi_zero_meaned_ts;
    ts_ijk_coords = roi_ijk_coords;
    ts_mni_coords = roi_mni_coords;
else
    error('Can not find required variables: (roi_)zero_meaned_ts, (roi_)ijk_coords, (roi_)mni)coords!!!');
end 



end