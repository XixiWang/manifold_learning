% Test script to run fsl command from matlab
setenv('FSLDIR','/Applications/fsl');
setenv('FSLOUTPUTTYPE', 'NIFTI_GZ'); 

nifti_file_name = 'subj1_actmaps_cond1_FeatSel_mc_ss.nii';
setenv('FileName', nifti_file_name);
!/Applications/fsl/bin/fslinfo $FileName

% cmd = '/Applications/fsl/bin/fslinfo subj1_actmaps_cond1_VT_mc_ss.nii'
% system(cmd)