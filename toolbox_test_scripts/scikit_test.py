# Script to test run scikit toolbox

print(__doc__)

from time import time

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import NullFormatter

from sklearn import datasets
from sklearn import manifold

# Silence pyflakes
Axes3D

# Generate swissroll dataset
n_points = 1000
n_neighbors = 10
n_components = 2
X, color = datasets.samples_generator.make_s_curve(n_points, random_state=0)

# Set up figure window
fig1 = plt.figure(figsize=(15, 8))
plt.suptitle("Manifold learning with %i points, %i neighbors" % (n_neighbors, n_neighbors))

# Plot original data
try:
    # compatibility matplotlib < 1.0
    ax = fig1.add_subplot(141, projection='3d')
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
    # ax.view_init(4, -72)
except:
    ax = fig1.add_subplot(141, projection='3d')
    plt.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)

# Standard LLE
t0 = time()
Y1 = manifold.LocallyLinearEmbedding(n_neighbors, n_components, eigen_solver='auto', method='standard').fit_transform(X)
t1 = time()
print("%s, %.2g secs" % ('standard LLE', t1 - t0))
ax = fig1.add_subplot(142)
plt.scatter(Y1[:, 0], Y1[:, 1], c=color, cmap=plt.cm.Spectral)
plt.title("Standard LLE")

# Modified LLE
t0 = time()
Y3 = manifold.LocallyLinearEmbedding(n_neighbors, n_components, eigen_solver='auto', method='modified').fit_transform(X)
t3 = time()
print("%s, %.2g secs" % ('modified LLE', t3 - t0))
ax = fig1.add_subplot(143)
plt.scatter(Y3[:, 0], Y3[:, 1], c=color, cmap=plt.cm.Spectral)
plt.title("Modified LLE")

# Isomap
t0 = time()
Y2 = manifold.Isomap(n_neighbors, n_components).fit_transform(X)
t2 = time()
print("%s, %.2g secs" % ('standard LLE', t2 - t0))
ax = fig1.add_subplot(144)
plt.scatter(Y2[:, 0], Y2[:, 1], c=color, cmap=plt.cm.Spectral)
plt.title("Isomap")

# t-SNE

# Keep the figure window
plt.show()
