% Test script to get familiar with the drtoolbox
clear;
% clc;
close all;

drtoolbox_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/drtoolbox/';
addpath(genpath(drtoolbox_dir));

cd(drtoolbox_dir);
% Test drtoolbox, could be skipped after setting up the toolbox
% test_toolbox;

%% Generates data
% Generates artifical swissroll dataset
[art_data, labels, real_coords] = generate_data('swiss', 5000, 0.05);

%% Estimates dimensionality
est_dim1 = round(intrinsic_dim(art_data, 'MLE'));
est_dim2 = round(intrinsic_dim(art_data, 'EigValue'));
fprintf('MLE-dim is %d, EigValue_dim is %d \n', est_dim1, est_dim2);

%% Computes mapping
% Testify LLE and isomap approaches
% Default dim is 2, mappedA is the low-dim representation of the data,
% mapping returns the information struct
%
% For isomap, default k = 12
[mappedA, mappingA] = compute_mapping(art_data, 'Isomap', est_dim1);
% For LLE, default k = 12
[mappedB, mappingB] = compute_mapping(art_data, 'LLE', est_dim1);

%% Display original data and mapped points
figure;
scatter3(art_data(:,1), art_data(:,2), art_data(:,3), 5, labels);
title('Original');

figure;
scatter(mappedA(:,1), mappedA(:,2), 5, labels);
title('Projected Isomap');

figure;
scatter(mappedB(:,1), mappedB(:,2), 5, labels);
title('Projected LLE');

%% drtoolbox test code 
[X, labels] = generate_data('helix', 2000);
figure, scatter3(X(:,1), X(:,2), X(:,3), 5, labels); title('Original dataset'), drawnow
no_dims = round(intrinsic_dim(X, 'MLE'));
disp(['MLE estimate of intrinsic dimensionality: ' num2str(no_dims)]);
[mappedX, mapping_dr] = compute_mapping(X, 'PCA', no_dims);
figure, scatter(mappedX(:,1), mappedX(:,2), 5, labels); title('Result of PCA');
[mappedX, mapping_dr] = compute_mapping(X, 'Laplacian', no_dims, 7);
figure, scatter(mappedX(:,1), mappedX(:,2), 5, labels(mapping_dr.conn_comp)); title('Result of Laplacian Eigenmaps'); drawnow

%% Perform classification 
% Generate test dataset
[test_data, test_labels, test_coords] = generate_data('swiss', 1000, 0.05);
test_points = out_of_sample(test_data, mapping);

% test_points2 = out_of_sample_est()