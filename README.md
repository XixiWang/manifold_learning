# README #

Manifold learning directory

### analyses ###
Scripts to analyze caos_toolrotation dataset

### documentation ###
Experiment designs and other documentation files

### experiment_design ###
Stimuli
Scripts to generate two categories of stimuli:

* Discrete stimuli: object is rotated every 5 degrees, so for TR = 2s, a complete rotation includes 72 frames

* Continuous stimuli: object is rotated ever .25 degrees, so for TR = 2s, a complete rotation includes 1440 frames

### toolbox_test_scripts ###
Scripts to test run dimension reduction toolboxes (i.e. drtoolbox, scikit toolbox)

### trash_files ###
Temporary trash files